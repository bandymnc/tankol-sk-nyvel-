package gamf.tankolaskonyvelo.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;
import gamf.tankolaskonyvelo.entities.ItemEntity;

@Dao
public interface ItemDao {
    @Insert
    long insert(ItemEntity itemEntity);
    @Insert
    void insert(List<ItemEntity> itemEntities);

    @Update
    void update(ItemEntity itemEntity);

    @Delete
    void delete(ItemEntity itemEntity);

    @Query("SELECT * FROM items ORDER BY item_name DESC")
    LiveData<List<ItemEntity>> getAllItemOrderByName();

    @Query("DELETE FROM items")
    void deleteAll();

    @Query("SELECT * FROM items WHERE item_name = :itemName")
    LiveData<ItemEntity> getItemByName(String itemName);
}
