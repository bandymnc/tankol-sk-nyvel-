package gamf.tankolaskonyvelo.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.dataobjects.PurchasedItemWithVehicle;

@Dao
public interface PurchasedItemDao {
    @Insert
    void insert(PurchasedItemEntity purchasedItemEntity);

    @Insert
    void insert(List<PurchasedItemEntity> purchasedItemEntities);

    @Update
    void update(PurchasedItemEntity purchasedItemEntity);

    @Delete
    void delete(PurchasedItemEntity purchasedItemEntity);

    @Query("SELECT * FROM purchased_items ORDER BY purchase_date DESC")
    LiveData<List<PurchasedItemEntity>> getAllPurchasedItemsOrderByPurschasedDateDesc();

    @Query("DELETE FROM purchased_items")
    void deleteAll();

    @Query("SELECT purchase_date as purchaseDate,piece,current_price as currentPrice ,items.item_name as itemName,is_important as isImportant" +
            " FROM purchased_items INNER JOIN vehicles " +
            "ON vehicle_id=vehicles.id INNER JOIN items " +
            "ON items.id=item_id" +
            " WHERE vehicles.license_plate = :licensePlateNumber" +
            " ORDER BY purchase_date DESC")
    LiveData<List<PurchasedItemWithVehicle>> getAllPurchasedItemByVehicle(String licensePlateNumber);

    @Query("SELECT purchase_date as purchaseDate,piece,current_price as currentPrice ,items.item_name as itemName,is_important as isImportant" +
            " FROM purchased_items INNER JOIN vehicles " +
            "ON vehicle_id=vehicles.id INNER JOIN items " +
            "ON items.id=item_id" +
            " WHERE vehicles.license_plate = :licensePlateNumber" +
            " AND is_important='1'" +
            "group by items.item_name" +
            " ORDER BY purchase_date DESC")
    LiveData<List<PurchasedItemWithVehicle>> getAllImportantPurchasedItemByVehicle(String licensePlateNumber);

    @Query("SELECT * FROM purchased_items" +
            " WHERE purchase_date = :date")
    LiveData<List<PurchasedItemEntity>> getPurchasedItemsByDate(String date);

    @Query("SELECT * FROM purchased_items" +
            " INNER JOIN vehicles" +
            " WHERE license_plate=:licensePlateNumber")
    LiveData<List<PurchasedItemEntity>> getAllPurchasedItemsByLicensePlateNumber(String licensePlateNumber);

    @Query("SELECT * FROM purchased_items")
    LiveData<List<PurchasedItemEntity>> getAllPurchasedItems();

}
