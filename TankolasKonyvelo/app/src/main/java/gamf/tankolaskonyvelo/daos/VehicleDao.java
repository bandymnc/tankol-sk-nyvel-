package gamf.tankolaskonyvelo.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import gamf.tankolaskonyvelo.entities.VehicleEntity;

@Dao
public interface VehicleDao {
    @Insert
    long insert(VehicleEntity vehicleEntity);

    @Insert
    void insert(List<VehicleEntity> vehicleEntities);

    @Update
    void update(VehicleEntity vehicleEntity);

    @Delete
    void delete(VehicleEntity vehicleEntity);

    @Query("SELECT * FROM vehicles")
    LiveData<List<VehicleEntity>> getAllVechicle();

    @Query("DELETE FROM vehicles")
    void deleteAll();

    @Query("SELECT * FROM vehicles WHERE default_vehicle = 1")
    LiveData<VehicleEntity> getDefaultVehicle();

    @Query("SELECT COUNT(*) FROM vehicles")
    LiveData<Integer> getVehicleAmount();

    @Query("Select fuelTypes.unit from fuelTypes inner join vehicles on vehicles.fuel_type_id=fuelTypes.id where vehicles.license_plate=:plateNumber")
    LiveData<String> getDefaultFuelUnit(String plateNumber);

}
