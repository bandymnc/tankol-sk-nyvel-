package gamf.tankolaskonyvelo.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelTypeEntity;

@Dao
public interface FuelTypeDao {
    @Insert
    void insert(FuelTypeEntity fuelTypeEntity);
    @Insert
    void insert(List<FuelTypeEntity> fuelTypeEntities);

    @Update
    void update(FuelTypeEntity fuelTypeEntity);

    @Delete
    void delete(FuelTypeEntity fuelTypeEntity);

    @Query("Select * from fuelTypes order by name Desc")
    LiveData<List<FuelTypeEntity>> getAllFuelTypeOrderByName();

    @Query("DELETE FROM fuelTypes")
    void deleteAll();

    @Query("SELECT * FROM fuelTypes INNER JOIN vehicles ON fuelTypes.id = vehicles.fuel_type_id " +
            "WHERE vehicles.license_plate = :licensePlate")
    LiveData<FuelTypeEntity> getFuelTypeByLicensePlate(String licensePlate);
}
