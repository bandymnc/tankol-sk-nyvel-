package gamf.tankolaskonyvelo.daos;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.dataobjects.FuelingWithFuelType;
import gamf.tankolaskonyvelo.entities.dataobjects.FuelingWithPurchasedItems;

@Dao
public interface FuelingDao {
    @Insert
    long insert(FuelingEntity fuelingEntity);
    @Insert
    void insert(List<FuelingEntity> fuelingEntities);

    @Update
    void update(FuelingEntity fuelingEntity);

    @Delete
    void delete(FuelingEntity fuelingEntity);

    @Query("SELECT * FROM fuelings ORDER BY fueling_date DESC")
    LiveData<List<FuelingEntity>> getAllFuelingsOrderByFuelingDate();

    @Query("DELETE FROM fuelings")
    void deleteAll();

    @Query("SELECT fuelings.fueling_date AS fuelingDate,km_meter_state as kmMeterState, fuelings.name_of_gas_station AS nameOfGasStation," +
                " ft.name AS fuelName, fuelings.unit_price_of_fuel AS unitPriceOfFuel, fuelings.fuel_quantity AS fuelQuantity," +
                " fuelings.fuel_meter_state AS fuelMeterState" +
            " FROM fuelings, fuelTypes AS ft" +
            " INNER JOIN vehicles ON fuelings.vehicle_id=vehicles.id" +
            " INNER JOIN fuelTypes ON fuelings.fuel_type_id=ft.id" +
            " WHERE vehicles.license_plate = :licensePlateNumber" +
            " ORDER BY fuelings.fueling_date DESC LIMIT 1")
    LiveData<FuelingWithFuelType> getLastFuelingByVehicleLicensePlateNumber(String licensePlateNumber);

    @Query("SELECT fuelings.fueling_date AS fuelingDate,km_meter_state as kmMeterState, fuelings.name_of_gas_station AS nameOfGasStation," +
            " ft.name AS fuelName, fuelings.unit_price_of_fuel AS unitPriceOfFuel, fuelings.fuel_quantity AS fuelQuantity," +
            " fuelings.fuel_meter_state AS fuelMeterState" +
            " FROM fuelings, fuelTypes AS ft" +
            " INNER JOIN fuelTypes ON fuelings.fuel_type_id=ft.id" +
            " ORDER BY fuelings.fueling_date DESC")
    LiveData<List<FuelingWithFuelType>> getAllFuelingsWithFuelTypesOrderByFuelingDate();

    @Query("SELECT * FROM fuelings INNER JOIN vehicles ON fuelings.vehicle_id = vehicles.id " +
            "WHERE vehicles.license_plate = :licensePlateNumber " +
            "ORDER BY fuelings.fueling_date DESC LIMIT 1")
    LiveData<FuelingEntity> getLastFueling(String licensePlateNumber);

    @Query("SELECT * FROM fuelings" +
            " INNER JOIN vehicles ON fuelings.vehicle_id=vehicles.id" +
            " WHERE vehicles.license_plate = :licensePlateNumber")
    LiveData<List<FuelingEntity>> getAllFuelingsByLicensePlateNumber(String licensePlateNumber);


}
