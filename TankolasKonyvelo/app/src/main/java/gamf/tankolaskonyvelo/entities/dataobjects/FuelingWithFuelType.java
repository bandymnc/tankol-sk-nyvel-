package gamf.tankolaskonyvelo.entities.dataobjects;

public class FuelingWithFuelType {
    private String fuelingDate;
    private String nameOfGasStation;
    private String fuelName;
    private double unitPriceOfFuel;
    private double fuelQuantity;
    private double fuelMeterState;
    private double kmMeterState;

    public double getKmMeterState() {
        return kmMeterState;
    }

    public void setKmMeterState(double kmMeterState) {
        this.kmMeterState = kmMeterState;
    }

    public String getFuelingDate() {
        return fuelingDate;
    }

    public void setFuelingDate(String fuelingDate) {
        this.fuelingDate = fuelingDate;
    }

    public String getNameOfGasStation() {
        return nameOfGasStation;
    }

    public void setNameOfGasStation(String nameOfGasStation) {
        this.nameOfGasStation = nameOfGasStation;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public double getUnitPriceOfFuel() {
        return unitPriceOfFuel;
    }

    public void setUnitPriceOfFuel(double unitPriceOfFuel) {
        this.unitPriceOfFuel = unitPriceOfFuel;
    }

    public double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public Double getFuelMeterState() {
        return fuelMeterState;
    }

    public void setFuelMeterState(double fuelMeterState) {
        this.fuelMeterState = fuelMeterState;
    }
}
