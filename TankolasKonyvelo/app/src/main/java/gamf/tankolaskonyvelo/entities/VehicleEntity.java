package gamf.tankolaskonyvelo.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "vehicles",
        indices = {@Index(value = {"license_plate"}, unique = true)},
        foreignKeys = {
                @ForeignKey(entity = FuelTypeEntity.class,
                        parentColumns = "id",
                        childColumns = "fuel_type_id",
                        onDelete = CASCADE,
                        onUpdate = CASCADE)})
public class VehicleEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "default_vehicle")
    private boolean defaultVehicle;

    @ColumnInfo(name = "vehicle_category")
    private String vehicleCategory;

    @ColumnInfo(name = "license_plate")
    private String licensePlateNumber;

    @ColumnInfo(name = "fuel_type_id")
    private int defaultFuel;

    public VehicleEntity(boolean defaultVehicle, String vehicleCategory, String licensePlateNumber, int defaultFuel) {
        this.defaultVehicle = defaultVehicle;
        this.vehicleCategory = vehicleCategory;
        this.licensePlateNumber = licensePlateNumber;
        this.defaultFuel = defaultFuel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getDefaultVehicle() {
        return defaultVehicle;
    }

    public void setDefaultVehicle(boolean defaultVehicle) {
        this.defaultVehicle = defaultVehicle;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public int getDefaultFuel() {
        return defaultFuel;
    }

    public void setDefaultFuel(int defaultFuel) {
        this.defaultFuel = defaultFuel;
    }
}

