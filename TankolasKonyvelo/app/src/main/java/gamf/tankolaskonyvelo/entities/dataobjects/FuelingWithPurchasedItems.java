package gamf.tankolaskonyvelo.entities.dataobjects;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;

public class FuelingWithPurchasedItems {
    private FuelingEntity fuelingEntity;
    private List<PurchasedItemEntity> purchasedItems;

    public FuelingWithPurchasedItems(FuelingEntity fuelingEntity, List<PurchasedItemEntity> purchasedItems) {
        this.fuelingEntity = fuelingEntity;
        this.purchasedItems = purchasedItems;
    }

    public FuelingEntity getFuelingEntity() {
        return fuelingEntity;
    }

    public void setFuelingEntity(FuelingEntity fuelingEntity) {
        this.fuelingEntity = fuelingEntity;
    }

    public List<PurchasedItemEntity> getPurchasedItems() {
        return purchasedItems;
    }

    public void setPurchasedItems(List<PurchasedItemEntity> purchasedItems) {
        this.purchasedItems = purchasedItems;
    }
}
