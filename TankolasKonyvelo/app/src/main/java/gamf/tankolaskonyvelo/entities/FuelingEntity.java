package gamf.tankolaskonyvelo.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "fuelings",
        //indices = {@Index(value = {"fueling_date"}, unique = true)},
        foreignKeys = {
                @ForeignKey(entity = VehicleEntity.class,
                        parentColumns = "id",
                        childColumns = "vehicle_id",
                        onDelete = CASCADE,
                        onUpdate = CASCADE),
                @ForeignKey(entity = FuelTypeEntity.class,
                        parentColumns = "id",
                        childColumns = "fuel_type_id",
                        onDelete = CASCADE,
                        onUpdate = CASCADE)})
public class FuelingEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "fueling_date")
    private String fuelingDate;

    @ColumnInfo(name = "name_of_gas_station")
    private String nameOfGasStation;

    @ColumnInfo(name = "unit_price_of_fuel")
    private double unitPriceOfFuel;

    @ColumnInfo(name = "fuel_quantity")
    private double fuelQuantity;

    @ColumnInfo(name = "fuel_meter_state")
    private double fuelMeterState;

    @ColumnInfo(name = "km_meter_state")
    private int kmMeterState;

    @ColumnInfo(name = "fuel_type_id")
    private int fuelTypeId;

    @ColumnInfo(name = "vehicle_id")
    private int vehicleId;

    public FuelingEntity(String fuelingDate, String nameOfGasStation, double unitPriceOfFuel,
                         double fuelQuantity, double fuelMeterState, int kmMeterState,
                         int fuelTypeId, int vehicleId) {
        this.fuelingDate = fuelingDate;
        this.nameOfGasStation = nameOfGasStation;
        this.unitPriceOfFuel = unitPriceOfFuel;
        this.fuelQuantity = fuelQuantity;
        this.fuelMeterState = fuelMeterState;
        this.kmMeterState = kmMeterState;
        this.fuelTypeId = fuelTypeId;
        this.vehicleId = vehicleId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFuelingDate() {
        return fuelingDate;
    }

    public void setFuelingDate(String fuelingDate) {
        this.fuelingDate = fuelingDate;
    }

    public String getNameOfGasStation() {
        return nameOfGasStation;
    }

    public void setNameOfGasStation(String nameOfGasStation) {
        this.nameOfGasStation = nameOfGasStation;
    }

    public double getUnitPriceOfFuel() {
        return unitPriceOfFuel;
    }

    public void setUnitPriceOfFuel(double unitPriceOfFuel) {
        this.unitPriceOfFuel = unitPriceOfFuel;
    }

    public double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public double getFuelMeterState() {
        return fuelMeterState;
    }

    public void setFuelMeterState(double fuelMeterState) {
        this.fuelMeterState = fuelMeterState;
    }

    public int getKmMeterState() {
        return kmMeterState;
    }

    public void setKmMeterState(int kmMeterState) {
        this.kmMeterState = kmMeterState;
    }

    public int getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(int fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }
}
