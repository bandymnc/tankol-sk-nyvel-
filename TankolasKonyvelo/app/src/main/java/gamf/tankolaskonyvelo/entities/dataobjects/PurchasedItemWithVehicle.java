package gamf.tankolaskonyvelo.entities.dataobjects;

public class PurchasedItemWithVehicle {
    private int piece;
    private String purchaseDate;
    private int currentPrice;
    private String itemName;
    private boolean isImportant;

    public PurchasedItemWithVehicle(int piece, String purchaseDate, int currentPrice, String itemName, boolean isImportant) {
        this.piece = piece;
        this.purchaseDate = purchaseDate;
        this.currentPrice = currentPrice;
        this.itemName = itemName;
        this.isImportant = isImportant;
    }

    public int getPiece() {
        return piece;
    }

    public void setPiece(int piece) {
        this.piece = piece;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public int getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(int currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }
}
