package gamf.tankolaskonyvelo.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "purchased_items",
        foreignKeys = {
                @ForeignKey(entity = VehicleEntity.class,
                        parentColumns = "id",
                        childColumns = "vehicle_id",
                        onDelete = CASCADE,
                        onUpdate = CASCADE),
                @ForeignKey(entity = ItemEntity.class,
                        parentColumns = "id",
                        childColumns = "item_id",
                        onDelete = CASCADE,
                        onUpdate = CASCADE)
        })
public class PurchasedItemEntity {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "piece")
    private int piece;

    @ColumnInfo(name = "purchase_date")
    private String purchaseDate;

    @ColumnInfo(name = "current_price")
    private int currentPrice;

    @ColumnInfo(name = "vehicle_id")
    private int vehicleId;

    @ColumnInfo(name = "item_id")
    private int itemId;

    public PurchasedItemEntity(int piece, String purchaseDate, int currentPrice, int vehicleId, int itemId) {
        this.piece = piece;
        this.purchaseDate = purchaseDate;
        this.currentPrice = currentPrice;
        this.vehicleId = vehicleId;
        this.itemId = itemId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPiece() {
        return piece;
    }

    public void setPiece(int piece) {
        this.piece = piece;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public int getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(int currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}
