package gamf.tankolaskonyvelo.ui.dashboard;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import java.util.List;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.repositories.FuelingRepository;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.repositories.FuelTypeRepository;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;

public class FuelingViewModel extends AndroidViewModel {
    VehicleRepository vehicleRepository;
    FuelTypeRepository fuelTypeRepository;
    FuelingRepository fuelingRepository;;
    int selectedVehicleId;

    public FuelingViewModel(@NonNull Application application) {
        super(application);

        vehicleRepository = new VehicleRepository(application, new AppExecutors());
        fuelTypeRepository = new FuelTypeRepository(application, new AppExecutors());
        fuelingRepository = new FuelingRepository(application, new AppExecutors());
    }

    public LiveData<List<VehicleEntity>> getAllVehicle() {
        return vehicleRepository.getAllVehicle();
    }

    public LiveData<List<FuelTypeEntity>> getAllFuelType() {
        return fuelTypeRepository.getAllFuelTypesOrderByName();
    }

    public void insertFueling(FuelingEntity fuelingEntity, InsertionListener listener) {
        fuelingRepository.insert(fuelingEntity, listener);
    }

    public LiveData<FuelingEntity> getLastFueling(String licensePlate) {
        return fuelingRepository.getLastFueling(licensePlate);
    }

    public int getSelectedVehicleId() {
        return selectedVehicleId;
    }

    public void setSelectedVehicleId(int selectedVehicleId) {
        this.selectedVehicleId = selectedVehicleId;
    }

    public LiveData<VehicleEntity> getDefaultVehicle(){
        return vehicleRepository.getDefaultVehicle();
    }

    public LiveData<Integer> getVehicleAmount() {
        return vehicleRepository.getVehicleAmount();
    }

    public LiveData<FuelTypeEntity> getFuelTypeByLicensePlate(String licensePlate) {
        return fuelTypeRepository.getFuelTypeByLicensePlate(licensePlate);
    }
}
