package gamf.tankolaskonyvelo.ui.about;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

public class AboutViewModel extends AndroidViewModel {
    public AboutViewModel(@NonNull Application application) {
        super(application);
    }
}
