package gamf.tankolaskonyvelo.ui.recyclerviewcontroller;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import gamf.tankolaskonyvelo.R;

public class RecyclerViewController implements ListController {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<RecyclerViewItem> recyclerViewItems;
    public RecyclerViewController(Context context, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(context.getApplicationContext());
    }

    public void show() {
        if (recyclerViewItems == null) {
            return;
        }

        adapter = new RecyclerViewAdapter(recyclerViewItems);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public void add(RecyclerViewItem item) {
        if (recyclerViewItems == null) {
            return;
        }

        recyclerViewItems.add(item);
        adapter.notifyDataSetChanged();
    }

    public void remove(RecyclerViewItem item) {
        if (recyclerViewItems != null) {
            recyclerViewItems.remove(item);
            adapter.notifyDataSetChanged();
        }
    }

    public void clear() {
        if (recyclerViewItems != null) {
            recyclerViewItems.clear();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public List<RecyclerViewItem> getRecyclerViewItems() {
        return recyclerViewItems;
    }

    public void setRecyclerViewItems(List<RecyclerViewItem> recyclerViewItems) {
        this.recyclerViewItems = recyclerViewItems;
    }
}
