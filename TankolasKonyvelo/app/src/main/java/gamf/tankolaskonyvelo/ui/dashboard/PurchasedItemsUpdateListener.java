package gamf.tankolaskonyvelo.ui.dashboard;

import java.util.List;

import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;

public interface PurchasedItemsUpdateListener {
    void onUpdate(List<PurchasedItemEntity> purchasedItems);
}
