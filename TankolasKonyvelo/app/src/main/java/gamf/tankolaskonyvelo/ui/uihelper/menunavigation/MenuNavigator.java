package gamf.tankolaskonyvelo.ui.uihelper.menunavigation;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.ui.main.MainActivity;

public class MenuNavigator {

    private Context context;

    public MenuNavigator(Context context) {
        this.context = context;
    }

    public void replaceFragment(Fragment fragment, String tag) {
        if (fragment != null) {
            doFragmentTransaction(fragment, tag);
        }
    }

    public void replaceFragment(Fragment fragment, String tag, Bundle argBundle) {
        fragment.setArguments(argBundle);
        replaceFragment(fragment, tag);
    }

    private void doFragmentTransaction(Fragment fragment, String tag) {
        FragmentManager fragmentManager = ((MainActivity)context).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        doBackStackPush(fragmentTransaction, tag);

        fragmentTransaction.setCustomAnimations(R.anim.enter_right_anim, R.anim.exit_left_anim,
                R.anim.enter_left_anim, R.anim.exit_right_anim);
        fragmentTransaction.replace(R.id.mainLayout, fragment, tag);
        fragmentTransaction.commit();
    }

    private void doBackStackPush(FragmentTransaction fragmentTransaction, String tag) {
        int entryCount = ((MainActivity)context).getSupportFragmentManager().getBackStackEntryCount();
        if (entryCount > 0) {
            FragmentManager.BackStackEntry backEntry = ((MainActivity)context).getSupportFragmentManager()
                    .getBackStackEntryAt(entryCount - 1);

            if (!backEntry.getName().equals(tag)) {
                fragmentTransaction.addToBackStack(tag);
            }
        }
        else {
            fragmentTransaction.addToBackStack(tag);
        }
    }
}
