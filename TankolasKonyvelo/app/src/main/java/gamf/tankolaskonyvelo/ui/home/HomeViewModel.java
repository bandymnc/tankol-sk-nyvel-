package gamf.tankolaskonyvelo.ui.home;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.entities.dataobjects.FuelingWithFuelType;
import gamf.tankolaskonyvelo.repositories.FuelingRepository;
import gamf.tankolaskonyvelo.repositories.PurchasedItemRepository;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelTypeConstants;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.AllUsedFuelQuantitySinceAllTime;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.AverageConsumptionSinceAllTime;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.CountOfFuelingsSinceAllTime;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.LengthOfWaySinceAllTime;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.OneFuelingAvgTraveledRouteSinceAllTime;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.SumOfAllCostSinceAllTime;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.SumOfFuelCostSinceAllTime;
import gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics.TravelledRoadSinceLastFueling;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class HomeViewModel extends AndroidViewModel {
    FuelingRepository fuelingRepository;
    VehicleRepository vehicleRepository;
    PurchasedItemRepository purchasedItemRepository;

    Application application;

    public HomeViewModel(@NonNull Application application) {
        super(application);

        this.application = application;

        fuelingRepository = new FuelingRepository(application, new AppExecutors());
        vehicleRepository = new VehicleRepository(application, new AppExecutors());
        purchasedItemRepository = new PurchasedItemRepository(application, new AppExecutors());
    }

    public void InitializeFuelTypeConstants(LifecycleOwner lifecycleOwner) {
        FuelTypeConstants fuelTypeConstants = FuelTypeConstants.getInstance();
        fuelTypeConstants.Initialize(lifecycleOwner, application, new AppExecutors());
    }

    public LiveData<FuelingWithFuelType> getLastFuelingByVehicleLicensePlateNumber(String licensePlateNumber) {
        return fuelingRepository.getLastFuelingByVehicleLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<FuelingEntity>> getAllFuelingsByLicensePlateNumber(String licensePlateNumber) {
        return fuelingRepository.getAllFuelingsByLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<PurchasedItemEntity>> getAllPurchasedItemsByLicensePlateNumber(String licensePlateNumber) {
        return purchasedItemRepository.getAllPurchasedItemsByLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<VehicleEntity>> getAllVehicle() {
        return vehicleRepository.getAllVehicle();
    }

    // STATISTICS
    public Double getLengthOfWaySinceAllTime(List<FuelingEntity> fuelingEntities) {
        StatisticsCalculator statisticsCalculation = new LengthOfWaySinceAllTime(fuelingEntities);
        StatisticsResult length = statisticsCalculation.calculateStatisics();

        return length.getSingleStatisticsValue();
    }

    public Double getAllUsedFuelQuantity(List<FuelingEntity> fuelingEntities) {
        StatisticsCalculator statisticsCalculation = new AllUsedFuelQuantitySinceAllTime(fuelingEntities);
        StatisticsResult quantity = statisticsCalculation.calculateStatisics();

        return quantity.getSingleStatisticsValue();
    }

    public Double getTravelledRoadSinceLastFuelling(List<FuelingEntity> fuelingEntities) {
        StatisticsCalculator statisticsCalculation = new TravelledRoadSinceLastFueling(fuelingEntities);
        StatisticsResult travelledRoad = statisticsCalculation.calculateStatisics();

        return travelledRoad.getSingleStatisticsValue();
    }

    public Double getCountOfFuelings(List<FuelingEntity> fuelingEntities) {
        StatisticsCalculator statisticsCalculation = new CountOfFuelingsSinceAllTime(fuelingEntities);
        StatisticsResult travelledRoad = statisticsCalculation.calculateStatisics();

        return travelledRoad.getSingleStatisticsValue();
    }

    public Double getSumOfFuelCost(List<FuelingEntity> fuelingEntities) {
        StatisticsCalculator statisticsCalculation = new SumOfFuelCostSinceAllTime(fuelingEntities);
        StatisticsResult sumOfFuelCostSinceAllTime = statisticsCalculation.calculateStatisics();

        return sumOfFuelCostSinceAllTime.getSingleStatisticsValue();
    }

    public Double getSumOfAllCost(List<FuelingEntity> fuelings, List<PurchasedItemEntity> purchasedItems) {
        StatisticsCalculator statisticsCalculation = new SumOfAllCostSinceAllTime(fuelings, purchasedItems);
        StatisticsResult sumOfAllCostSinceAllTime = statisticsCalculation.calculateStatisics();

        return sumOfAllCostSinceAllTime.getSingleStatisticsValue();
    }

    public Double getOneFuelingAvgTraveledRoute(List<FuelingEntity> fuelings) {
        StatisticsCalculator statisticsCalculation = new OneFuelingAvgTraveledRouteSinceAllTime(fuelings);
        StatisticsResult oneFuelingAvgTraveledRoute = statisticsCalculation.calculateStatisics();

        return oneFuelingAvgTraveledRoute.getSingleStatisticsValue();
    }

    public Double getAverageConsumption(List<FuelingEntity> fuelings) {
        StatisticsCalculator statisticsCalculation = new AverageConsumptionSinceAllTime(fuelings);
        StatisticsResult averageConsumption = statisticsCalculation.calculateStatisics();

        return averageConsumption.getSingleStatisticsValue();
    }
}
