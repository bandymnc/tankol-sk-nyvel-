package gamf.tankolaskonyvelo.ui.statistics;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;

public class Statistic {
    private BarChart barChart;
    private LineChart lineChart;
    private PieChart pieChart;
    private StatisticsViewModel statisticsViewModel;
    private ValueFormatter customValueFormatter;
    private TextView statisticsYAxesTextView;
    private TextView statisticsXAxesTextView;

    public Statistic(BarChart barChart, LineChart lineChart, PieChart pieChart,TextView statisticsYAxesTextView,TextView statisticsXAxesTextView) {
        this.barChart = barChart;
        this.lineChart = lineChart;
        this.pieChart = pieChart;
        this.statisticsYAxesTextView=statisticsYAxesTextView;
        this.statisticsXAxesTextView=statisticsXAxesTextView;
    }

    public void barChart(AxesStatisticsResult axesStatisticsResult) {

        barChart.fitScreen();
        barChart.clear();
        barChart.getDescription().setEnabled(false);
        barChart.setDrawBarShadow(false);
        barChart.setDoubleTapToZoomEnabled(false);
        barChart.animateXY(1000, 1000);
        barChart.setDrawValueAboveBar(true);
        barChart.zoom(1.5f, 0, 0, 1.5f);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(true);
        barChart.setExtraRightOffset(10f);

        ArrayList<BarEntry> barEntries = new ArrayList<>();

        barEntries = getBarEntryDataList(axesStatisticsResult);

        BarDataSet barDataSet = new BarDataSet(barEntries, "");
        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        BarData data = new BarData(barDataSet);
        data.setValueTextSize(10f);
        barChart.setData(data);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setTextSize(10f);
        xAxis.setGranularityEnabled(true);// egységnyi beosztás
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(axesStatisticsResult.getStatisticsNameAsTmb()));
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setLabelRotationAngle(45);

        statisticsYAxesTextView.setText(axesStatisticsResult.getyAxesName());
        statisticsXAxesTextView.setText(axesStatisticsResult.getxAxesName());
    }

    public void lineChart(AxesStatisticsResult axesStatisticsResult) {

        lineChart.clear();
        lineChart.fitScreen();
        lineChart.getDescription().setEnabled(false);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.setDoubleTapToZoomEnabled(false);
        lineChart.setExtraRightOffset(20f);
        lineChart.animateXY(1000, 1000);

        if(axesStatisticsResult.getLowerExtremeValue()!=0) {
            LimitLine upper_limit = new LimitLine(axesStatisticsResult.getUpperExtremeFloatValue(), "Kiugróan magas");
            upper_limit.setLineWidth(4f);
            upper_limit.enableDashedLine(10f, 10f, 0f);
            upper_limit.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            upper_limit.setTextSize(10f);

            LimitLine lower_limit = new LimitLine(axesStatisticsResult.getLowerExtremeFloatValue(), "Kiugróan alacsony");
            lower_limit.setLineWidth(4f);
            lower_limit.enableDashedLine(10f, 10f, 0f);
            lower_limit.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            lower_limit.setTextSize(10f);

            YAxis leftAxis = lineChart.getAxisLeft();
            leftAxis.removeAllLimitLines();
            leftAxis.addLimitLine(upper_limit);
            leftAxis.addLimitLine(lower_limit);
            leftAxis.enableGridDashedLine(10f, 10f, 0);
            leftAxis.setDrawLimitLinesBehindData(true);
        }
        lineChart.getAxisRight().setEnabled(false);

        ArrayList<Entry> yValues = new ArrayList<>();
        yValues = getLineChartDataList(axesStatisticsResult);

        LineDataSet lineDataSet = new LineDataSet(yValues, "");
        lineDataSet.setFillAlpha(110);
        lineDataSet.setColor(Color.BLUE);
        lineDataSet.setLineWidth(3f);
        lineDataSet.setValueTextSize(15f);
        lineDataSet.setValueTextColor(Color.DKGRAY);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);

        LineData data = new LineData(dataSets);

        lineChart.setData(data);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(1f);
        xAxis.setTextSize(10);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(axesStatisticsResult.getStatisticsNameAsTmb()));
        xAxis.setPosition(XAxis.XAxisPosition.TOP);
        xAxis.setDrawGridLines(true);
        xAxis.setLabelCount(25);
        xAxis.setLabelRotationAngle(45);

        statisticsYAxesTextView.setText(axesStatisticsResult.getyAxesName());
        statisticsXAxesTextView.setText(axesStatisticsResult.getxAxesName());
    }

    public void pieChart(AxesStatisticsResult axesStatisticsResult,boolean usePercentValue,String unit) {

        pieChart.clear();
        statisticsYAxesTextView.setText("");
        statisticsXAxesTextView.setText("");
        pieChart.getDescription().setEnabled(false);
        pieChart.setUsePercentValues(usePercentValue);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
        pieChart.setDragDecelerationFrictionCoef(0.99f);
        pieChart.setDrawHoleEnabled(false);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleRadius(61f);
        pieChart.animateY(1000, Easing.EaseInCubic);

        ArrayList<PieEntry> yValues = new ArrayList<>();

        yValues = getPieEntryDataList(axesStatisticsResult,unit);

        PieDataSet dataSet = new PieDataSet(yValues, " ");
        dataSet.setSliceSpace(1f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData data = new PieData(dataSet);
        data.setValueTextSize(20f);
        data.setValueTextColor(Color.DKGRAY);

        pieChart.setData(data);
    }

    private ArrayList<Entry> getLineChartDataList(AxesStatisticsResult axesStatisticsResult) {
        ArrayList<Entry> entryArrayList = new ArrayList<>();

        for (int i = 0; i < axesStatisticsResult.size(); i++) {
            entryArrayList.add(new Entry(i, axesStatisticsResult.get(i).getSingleStatisticsFloatValue()));
        }
        return entryArrayList;
    }

    private ArrayList<PieEntry> getPieEntryDataList(AxesStatisticsResult axesStatisticsResult,String unit) {
        ArrayList<PieEntry> pieEntryArrayList = new ArrayList<>();
        ArrayList<PieEntry> yValues = new ArrayList<>();
        for (int i = 0; i < axesStatisticsResult.size(); i++) {
            pieEntryArrayList.add(new PieEntry(axesStatisticsResult.get(i).getSingleStatisticsFloatValue(), axesStatisticsResult.get(i).getSingleStatisticsName()+" "+unit));
        }
        return pieEntryArrayList;
    }

    private ArrayList<BarEntry> getBarEntryDataList(AxesStatisticsResult axesStatisticsResult) {
        ArrayList<BarEntry> barEntryArrayList = new ArrayList<>();

        for (int i = 0; i < axesStatisticsResult.size(); i++) {
            barEntryArrayList.add(new BarEntry(i, axesStatisticsResult.get(i).getSingleStatisticsFloatValue()));
        }
        return barEntryArrayList;
    }
}
