package gamf.tankolaskonyvelo.ui.recyclerviewcontroller;

public class RecyclerViewItem {

    private String serviceName;
    private String servicePrice;
    private String serviceAmount;
    private String sumOfCosts;
    private String date;

    public RecyclerViewItem(String serviceName, String servicePrice, String serviceAmount, String sumOfCosts, String date) {
        this.serviceName = serviceName;
        this.servicePrice = servicePrice;
        this.serviceAmount = serviceAmount;
        this.sumOfCosts = sumOfCosts;
        this.date = date;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public String getServiceAmount() {
        return serviceAmount;
    }

    public String getDate() {
        return date;
    }

    public String getSumOfCosts() {
        return sumOfCosts;
    }

    public void setSumOfCosts(String sumOfCosts) {
        this.sumOfCosts = sumOfCosts;
    }
}