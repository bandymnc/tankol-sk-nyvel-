package gamf.tankolaskonyvelo.ui.dialog;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private Fragment containerFragment;

    public static DatePickerFragment getNewInstance(DialogArgs dialogArgs, Fragment fragment) {
        DatePickerFragment datePickerFragment;
        Bundle argsBundle = new Bundle();

        argsBundle.putString(dialogArgs.layoutTagKey, dialogArgs.layoutTag);
        argsBundle.putInt(dialogArgs.resIdKey, dialogArgs.resId);

        datePickerFragment = new DatePickerFragment();
        datePickerFragment.setArguments(argsBundle);
        datePickerFragment.containerFragment = fragment;

        return datePickerFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        final Calendar c = Calendar.getInstance();
        int yearCheck = c.get(Calendar.YEAR);
        int monthCheck = c.get(Calendar.MONTH);
        int dayCheck = c.get(Calendar.DAY_OF_MONTH);

        String yearString;
        String monthString;
        String dayString;

        if (year > yearCheck || month > monthCheck || day > dayCheck) {
            yearString = DateTimeHelper.correctTwoDigitValue(String.valueOf(yearCheck));
            monthString = DateTimeHelper.correctTwoDigitValue(String.valueOf(monthCheck + 1));
            dayString = DateTimeHelper.correctTwoDigitValue(String.valueOf(dayCheck));
            new Snackbars().errorSnackbar(containerFragment.getView(), "Jövőbeli dátum megadása nem támogatott.");
        } else {
            yearString = DateTimeHelper.correctTwoDigitValue(String.valueOf(year));
            monthString = DateTimeHelper.correctTwoDigitValue(String.valueOf(month + 1));
            dayString = DateTimeHelper.correctTwoDigitValue(String.valueOf(day));
        }
        ((EditText) DialogFragmentManager.removeDialogFragment(this))
                .setText(yearString + "-" + monthString  + "-" + dayString);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        DialogFragmentManager.removeDialogFragment(this);
    }

}