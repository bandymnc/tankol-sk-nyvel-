package gamf.tankolaskonyvelo.ui.settings;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;

public class SettingsViewModel extends AndroidViewModel {
    private VehicleRepository vehicleRepository;

    public SettingsViewModel(@NonNull Application application) {

        super(application);
        vehicleRepository = new VehicleRepository(application, new AppExecutors());
    }

    public LiveData<List<VehicleEntity>> getAllVehicle(){
        return vehicleRepository.getAllVehicle();
    }
    public void update(VehicleEntity vehicleEntity){
        vehicleRepository.update(vehicleEntity);
    }

    public void insert(VehicleEntity vehicleEntity){
         vehicleRepository.insert(vehicleEntity);
    }
}
