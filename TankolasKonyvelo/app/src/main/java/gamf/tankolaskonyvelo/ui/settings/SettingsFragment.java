package gamf.tankolaskonyvelo.ui.settings;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.database.operations.DatabaseImportExport;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.localization.Localization;
import gamf.tankolaskonyvelo.localization.Localizer;
import gamf.tankolaskonyvelo.ui.main.MainActivity;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;

public class SettingsFragment extends Fragment {

    private final int IMPORT_REQUEST_CODE = 123;

    private Spinner settingsSpinner;
    private SettingsViewModel settingsViewModel;
    private Snackbars snackbars;
    private VehicleEntity defaultVehicle;
    private VehicleEntity newDefaultVehicle;
    private List<VehicleEntity> vehicleEntityList = new ArrayList<>();
    private Button databaseImportButton;
    private Button databaseExportButton;
    private DatabaseImportExport databaseImportExport;
    private int readPermission;
    private int writePermission;
    private String path;

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        settingsViewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);

        return inflater.inflate(R.layout.fragment_settings, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        AppDatabase appDatabase = AppDatabase.getInstance(getContext(), new AppExecutors());
        databaseImportExport = new DatabaseImportExport(getContext(), appDatabase, getActivity());
        settingsSpinner = getView().findViewById(R.id.defaultVehicleSpinner);
        databaseExportButton = getView().findViewById(R.id.databaseExportButton);
        databaseImportButton = getView().findViewById(R.id.databaseImportButton);

        getActivity().setTitle(getString(R.string.nav_settings));
        addItemToSpinner();
        setLanguageChangeListeners();
        settingsSpinnerListener();
        databaseExportButtonListener();
        databaseImportButtonListener();
    }

    private void databaseExportButtonListener() {
        databaseExportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getPermission()) {
                    String result = databaseImportExport.databaseExport();
                    informationSnackbar(result);
                }
            }
        });
    }

    private void databaseImportButtonListener() {
        databaseImportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getPermission()) {

                    String[] extraMimeTypes = {"*/*"};
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("application/x-sqlite3");
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
                            .putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(Intent.createChooser(intent, "Válassza ki az adatbázist"), IMPORT_REQUEST_CODE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMPORT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            if (data.getData().toString().endsWith("app_database")) {
                String result = databaseImportExport.databaseImport(data.getData());
                informationSnackbar(result);
            } else {
                errorSnackbar(getString(R.string.no_database_sourche_file));
            }
        }
    }

    private boolean getPermission() {
        readPermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        writePermission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (readPermission == PackageManager.PERMISSION_GRANTED) {
            if (writePermission == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                Snackbar.make(getView(), getString(R.string.permission_write), Snackbar.LENGTH_LONG)
                        .setAction(R.string.allow_permission, view1 -> ActivityCompat.requestPermissions(getActivity(), new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        }, 1)).show();
            }
        } else {
            Snackbar.make(getView(), getString(R.string.permission_read), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.allow_permission), view1 -> ActivityCompat.requestPermissions(getActivity(), new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    }, 0)).show();
        }
        return false;
    }

    private void settingsSpinnerListener() {
        settingsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                newDefaultVehicle = searchChosenVehicle(settingsSpinner.getSelectedItem().toString());
                newDefaultVehicle.setDefaultVehicle(true);

                if (newDefaultVehicle != defaultVehicle) {
                    setUpNewDefaultVehicle(defaultVehicle, newDefaultVehicle);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setLanguageChangeListeners() {
        setLanguageButtonListener(getView().findViewById(R.id.hungaryButton));
        setLanguageButtonListener(getView().findViewById(R.id.englishButton));
    }

    private void setLanguageButtonListener(Button button) {
        button.setOnClickListener(view -> {
            Localization localization = new Localizer((MainActivity) getContext(), MainActivity.class);

            switch (button.getId()) {
                case R.id.hungaryButton:
                    localization.saveDefaultLocale("hu");
                    localization.setLocale("hu");
                    break;
                case R.id.englishButton:
                    localization.saveDefaultLocale("en");
                    localization.setLocale("en");
                    break;
                default:
                    localization.setLocale("hu");
                    break;
            }
            localization.apply();
        });
    }

    public void addItemToSpinner() {
        settingsViewModel.getAllVehicle().observe(this, vehicles -> {

            if (vehicles.size() != 0) {
                List<String> vehicleLicensePlateNumbers = new ArrayList<String>();

                int defaultVehicleIndex = 0;

                for (int i = 0; i < vehicles.size(); i++) {
                    vehicleEntityList.add(vehicles.get(i));
                    vehicleLicensePlateNumbers.add(vehicles.get(i).getLicensePlateNumber());

                    if (vehicleEntityList.get(i).getDefaultVehicle()) {
                        defaultVehicleIndex = i;
                        defaultVehicle = vehicleEntityList.get(i);
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, vehicleLicensePlateNumbers);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                settingsSpinner.setAdapter(adapter);
                settingsSpinner.setSelection(defaultVehicleIndex);
            } else {
                errorSnackbar(getString(R.string.save_vehicle_at_first));
            }
        });
    }

    private void setUpNewDefaultVehicle(VehicleEntity defaultVehicle, VehicleEntity newDefaultVehicle) {
        defaultVehicle.setDefaultVehicle(false);
        settingsViewModel.update(defaultVehicle);
        settingsViewModel.update(newDefaultVehicle);
        informationSnackbar(getString(R.string.new_default_license_plate_num) + newDefaultVehicle.getLicensePlateNumber());
    }

    private VehicleEntity searchChosenVehicle(String licensePlateNumber) {

        for (int i = 0; i < vehicleEntityList.size(); i++) {
            if (vehicleEntityList.get(i).getLicensePlateNumber().equals(licensePlateNumber)) {
                return vehicleEntityList.get(i);
            }
        }
        return null;
    }

    private void errorSnackbar(String message) {
        Snackbars snackbars = new Snackbars();
        snackbars.errorSnackbar(getView(), message);
    }

    private void informationSnackbar(String message) {
        Snackbars snackbars = new Snackbars();
        snackbars.informationSnackbar(getView(), message);
    }
}