package gamf.tankolaskonyvelo.ui.uihelper.customsnackbar;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

public class Snackbars {

    public void errorSnackbar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        TextView snackBarTextView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);

        snackbarView.setBackgroundColor(Color.RED);
        snackBarTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        snackBarTextView.setTextSize(20);

        snackbar.show();
    }

    public void informationSnackbar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        TextView snackBarTextView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);

        snackBarView.setBackgroundColor(Color.BLUE);
        snackBarTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        snackBarTextView.setTextSize(20);

        snackbar.show();
    }

    public void warningSnackbar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        TextView snackBarTextView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);

        snackBarView.setBackgroundColor(Color.rgb(236,213, 64));
        snackBarTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        snackBarTextView.setTextSize(20);

        snackbar.show();
    }
}
