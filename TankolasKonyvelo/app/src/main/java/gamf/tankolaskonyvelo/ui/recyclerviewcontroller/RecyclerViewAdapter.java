package gamf.tankolaskonyvelo.ui.recyclerviewcontroller;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gamf.tankolaskonyvelo.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ListViewHolder> {

    private List<RecyclerViewItem> recyclerViewItems;

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.other_services_list_item, parent, false);

        ListViewHolder holder = new ListViewHolder(view);
        return holder;
    }

    public RecyclerViewAdapter(List<RecyclerViewItem> recyclerViewItems) {
        this.recyclerViewItems = recyclerViewItems;
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        RecyclerViewItem item = recyclerViewItems.get(position);
        holder.mNameTextView.setText(item.getServiceName());
        holder.mPriceTextView.setText(item.getServicePrice());
        holder.mAmountTextView.setText(item.getServiceAmount());
        holder.mAllPriceTextView.setText(item.getSumOfCosts());
        holder.mDateTextView.setText(item.getDate());
    }

    @Override
    public int getItemCount() {
        return recyclerViewItems.size();
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder {

        public TextView mNameTextView;
        public TextView mPriceTextView;
        public TextView mAmountTextView;
        public TextView mAllPriceTextView;
        public TextView mDateTextView;

        public ListViewHolder(View itemView) {
            super(itemView);
            mNameTextView = itemView.findViewById(R.id.listItemNameTextView);
            mPriceTextView = itemView.findViewById(R.id.listItemPriceTextView);
            mAmountTextView = itemView.findViewById(R.id.listItemPieceTextView);
            mAllPriceTextView = itemView.findViewById(R.id.listItemAllPriceTextView);
            mDateTextView = itemView.findViewById(R.id.listItemDateTextView);
        }
    }
}
