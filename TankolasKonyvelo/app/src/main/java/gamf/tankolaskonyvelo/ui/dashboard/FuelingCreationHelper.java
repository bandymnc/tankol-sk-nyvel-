package gamf.tankolaskonyvelo.ui.dashboard;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;

public class FuelingCreationHelper {

    private Fragment context;
    private String selectedLicensePlate;
    private String selectedFuelType;

    private int vehicleId;
    private int fuelTypeId;
    private List<FuelingCreationHelperListener> listeners;

    public FuelingCreationHelper(Fragment context, FuelingViewModel fuelingViewModel,
                                 String selectedLicensePlate, String selectedFuelType) {
        this.context = context;
        this.listeners = new ArrayList<>();
        this.selectedLicensePlate = selectedLicensePlate;
        this.selectedFuelType = selectedFuelType;

        selectLicensePlate(fuelingViewModel);
    }

    public void addListener(FuelingCreationHelperListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    private void selectLicensePlate(FuelingViewModel fuelingViewModel) {
        fuelingViewModel.getAllVehicle().observe(context, vehicles -> {
            for (VehicleEntity vehicle : vehicles) {
                if (vehicle.getLicensePlateNumber().equals(selectedLicensePlate)) {
                    vehicleId = vehicle.getId();

                    // defining the fuel type
                    fuelingViewModel.getAllFuelType().observe(context, fuelTypes -> {
                        for (FuelTypeEntity fuelType : fuelTypes) {
                            if (fuelType.getName().equals(selectedFuelType)) {
                                fuelTypeId = fuelType.getId();

                                for (FuelingCreationHelperListener listener : listeners) {
                                    if (listener != null) {
                                        listener.onFuelHelperTerminated(fuelTypeId, vehicleId);
                                    }
                                }
                                return;
                            }
                        }
                    });
                    return;
                }
            }
        });
    }
}
