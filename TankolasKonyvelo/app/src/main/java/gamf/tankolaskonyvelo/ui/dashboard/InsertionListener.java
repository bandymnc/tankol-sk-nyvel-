package gamf.tankolaskonyvelo.ui.dashboard;

public interface InsertionListener {
    void onRecordInserted(long idOfInserted);
}
