package gamf.tankolaskonyvelo.ui.dashboard;

interface FuelingCreationHelperListener {

    void onFuelHelperTerminated(int fuelTypeId, int vehicleId);
}
