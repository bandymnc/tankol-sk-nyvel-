package gamf.tankolaskonyvelo.ui.dashboard;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.repositories.ItemRepository;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.repositories.PurchasedItemRepository;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;

public class RequiredServicesViewModel extends AndroidViewModel {
    ItemRepository itemRepository;
    VehicleRepository vehicleRepository;
    PurchasedItemRepository purchasedItemRepository;

    Bundle argBundle;
    boolean isItemListed;
    VehicleEntity selectedVehicle;

    public RequiredServicesViewModel(@NonNull Application application) {
        super(application);
        this.itemRepository = new ItemRepository(application, new AppExecutors());
        this.purchasedItemRepository = new PurchasedItemRepository(application, new AppExecutors());
        this.vehicleRepository = new VehicleRepository(application, new AppExecutors());
    }

    public LiveData<List<VehicleEntity>> getAllVehicle() {
        return vehicleRepository.getAllVehicle();
    }

    public LiveData<ItemEntity> getItemByName(String itemName) {
        return itemRepository.getItemByName(itemName);
    }

    public LiveData<VehicleEntity> getDefaultVehicle() {
        return vehicleRepository.getDefaultVehicle();
    }

    public LiveData<List<ItemEntity>> getAllItems() {
        return itemRepository.getAllFuelingsOrderByFuelingDate();
    }

    public void insertItem(ItemEntity itemEntity, InsertionListener listener) {
        itemRepository.insert(itemEntity, listener);
    }

    public void insertPurchasedItems(List<PurchasedItemEntity> purchasedItemEntities) {
        for (PurchasedItemEntity purchasedItemEntity : purchasedItemEntities) {
            purchasedItemEntity.setVehicleId(selectedVehicle.getId());
            purchasedItemRepository.insert(purchasedItemEntity);
        }
    }

    public void updatedPurchasedItems(Fragment context, String itemName, boolean isImportant,
                                      List<PurchasedItemEntity> purchasedItems,
                                      PurchasedItemEntity purchasedItem,
                                      PurchasedItemsUpdateListener listener) {
        isItemListed = false;

        getItemByName(itemName).observe(context, item -> {
            if (item == null && !isItemListed) {
                item = new ItemEntity(itemName, isImportant);
                insertItem(item, idOfInserted -> {
                    purchasedItem.setItemId((int) idOfInserted);
                    purchasedItems.add(purchasedItem);
                });

                isItemListed = true;
            } else if (!isItemListed) {
                purchasedItem.setItemId(item.getId());
                purchasedItems.add(purchasedItem);
                listener.onUpdate(purchasedItems);

                isItemListed = true;
            }
        });
    }

    public void initVehicleSpinnerListener(Fragment context, Spinner vehicleSpinner) {
        vehicleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedLicense = vehicleSpinner.getSelectedItem().toString();

                getAllVehicle().observe(context, vehicles -> {
                    for (VehicleEntity aVehicle : vehicles) {
                        if (aVehicle.getLicensePlateNumber().equals(selectedLicense)) {
                            selectedVehicle = aVehicle;
                            break;
                        }
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public String getDateByArgument() {
        if (argBundle != null) {
            return argBundle.getString("date");
        }
        return "";
    }

    public String getTimeByArgument() {
        if (argBundle != null) {
            return argBundle.getString("time");
        }
        return "";
    }

    public int getDefaultVehicleIdByArgument() {
        if (argBundle != null) {
            return argBundle.getInt("selectedVehicleId");
        }
        return 0;
    }

    public void setArgBundle(Bundle argBundle) {
        this.argBundle = argBundle;
    }

    public VehicleEntity getSelectedVehicle() {
        return selectedVehicle;
    }
}
