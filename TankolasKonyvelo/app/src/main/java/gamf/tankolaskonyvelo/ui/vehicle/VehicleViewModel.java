package gamf.tankolaskonyvelo.ui.vehicle;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.repositories.FuelTypeRepository;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;

public class VehicleViewModel extends AndroidViewModel {
    FuelTypeRepository fuelTypeRepository;
    VehicleRepository vehicleRepository;

    public VehicleViewModel(@NonNull Application application) {
        super(application);

        fuelTypeRepository = new FuelTypeRepository(application, new AppExecutors());
        vehicleRepository = new VehicleRepository(application, new AppExecutors());
    }

    public LiveData<List<FuelTypeEntity>> getAllFuelTypesOrderByName() {
        return fuelTypeRepository.getAllFuelTypesOrderByName();
    }

    public void insert(VehicleEntity vehicleEntity) {
        vehicleRepository.insert(vehicleEntity);
    }

    public  LiveData<List<VehicleEntity>> getAllVehicle(){
       return vehicleRepository.getAllVehicle();
    }
}
