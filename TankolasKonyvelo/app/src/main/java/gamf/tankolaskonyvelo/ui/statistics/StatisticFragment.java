package gamf.tankolaskonyvelo.ui.statistics;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;

public class StatisticFragment extends Fragment {
    private BarChart barChart;
    private PieChart pieChart;
    private LineChart lineChart;
    private Spinner diagramSpinner;
    private List<String> typeOfStatisticsList;
    private Statistic statistic;
    private List<String> statisticsType = new ArrayList<>();
    private List<FuelingEntity> fuelingEntities = new ArrayList<>();
    private Spinner choosenCarSpinner;
    private boolean isVehicle = false;
    private TextView statisticsYAxesTextView;
    private TextView statisticsXAxesTextView;

    private AxesStatisticsResult travelledDistanceBetweenFueling;
    private AxesStatisticsResult sumFuelCostPerMonth;
    private AxesStatisticsResult fuelTypesRatioAllVehicle;
    private AxesStatisticsResult sumOfConsumedFuelPerMonth;
    private AxesStatisticsResult sumOfFuelCostWithPurchasedItemsPerMonth;
    private AxesStatisticsResult sumOfFuelingsByMonth;
    private AxesStatisticsResult sumOfPassedKmPerMonth;
    private AxesStatisticsResult sumOfPurchasedItemsCostPerMonth;
    private AxesStatisticsResult topPurchasedItems;
    private List<VehicleEntity> vehicleEntityList=new ArrayList<>();


    private StatisticsViewModel statisticsViewModel;

    public StatisticFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        statisticsViewModel = ViewModelProviders.of(this).get(StatisticsViewModel.class);
        return inflater.inflate(R.layout.fragment_statistic, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.nav_stats));

        LifecycleOwner lifecycleOwner = this;
        choosenCarSpinner = getView().findViewById(R.id.choosenVehicleSpinner);
        diagramSpinner = getView().findViewById(R.id.statisticsTypeSpinner);
        barChart = getView().findViewById(R.id.barChart);
        pieChart = getView().findViewById(R.id.pieChart);
        lineChart = getView().findViewById(R.id.lineChart);

        setUpStatisticsTypeTmb();

        fillSpinnerWithVehiclePlateNumbers(lifecycleOwner);
    }

    private void addItemToSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, statisticsType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        diagramSpinner.setAdapter(adapter);
    }

    private void setUpStatisticsTypeTmb() {
        statisticsType.add(getString(R.string.distance_between_fuelings));
        statisticsType.add(getString(R.string.sum_fuel_cost_per_month));
        statisticsType.add(getString(R.string.fuel_types_ratio_all_vehicle));
        statisticsType.add(getString(R.string.sum_of_consumed_fuel_per_month));
        statisticsType.add(getString(R.string.sum_of_fuel_cost_with_purchased_items_per_month));
        statisticsType.add(getString(R.string.sum_of_fuelings_by_month));
        statisticsType.add(getString(R.string.sum_of_passed_km_per_month));
        statisticsType.add(getString(R.string.sum_of_purchased_items_cost_per_month));
        statisticsType.add(getString(R.string.top_purchased_items));
    }

    private void fillSpinnerWithVehiclePlateNumbers(LifecycleOwner lifecycleOwner) {
        statisticsViewModel.getAllVehicle().observe(this, vehicles -> {
            List<String> vehicleLicensePlateNumbers = new ArrayList<>();

            if (!vehicles.isEmpty()) {
                choosenCarSpinnerListener(lifecycleOwner);
                isVehicle = true;

                int defaultVehicleIndex = 0;

                for (int i = 0; i < vehicles.size(); i++) {
                    VehicleEntity aVehicle = vehicles.get(i);
                    vehicleLicensePlateNumbers.add(aVehicle.getLicensePlateNumber());
                    vehicleEntityList.add(vehicles.get(i));

                    if (aVehicle.getDefaultVehicle()) {
                        defaultVehicleIndex = i;
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, vehicleLicensePlateNumbers);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                choosenCarSpinner.setAdapter(adapter);
                choosenCarSpinner.setSelection(defaultVehicleIndex);
            } else {
                showErrorSnackBar(getString(R.string.no_saved_vehicle));
            }
        });
    }

    private void choosenCarSpinnerListener(LifecycleOwner lifecycleOwner) {
        choosenCarSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                statisticsYAxesTextView = getView().findViewById(R.id.statisticsYAxesTextView);
                statisticsXAxesTextView = getView().findViewById(R.id.statisticsXAxesTextView);
                statistic = new Statistic(barChart, lineChart, pieChart, statisticsYAxesTextView, statisticsXAxesTextView);

                String selectedPlateNumber=choosenCarSpinner.getSelectedItem().toString();

                statisticsViewModel.getDefaultFuelUnit(selectedPlateNumber).observe(lifecycleOwner,(fuelUnit)->{
                    fv(lifecycleOwner,selectedPlateNumber,fuelUnit.toString());
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public void fv(LifecycleOwner lifecycleOwner,String selectedPlateNumber,String fuelUnit){
        statisticsViewModel.getAllFuelingsByLicensePlateNumber(selectedPlateNumber).observe(lifecycleOwner, (fuelings) -> {
            addItemToSpinner();
            setNull();
            if (fuelings.size() > 1) {
                travelledDistanceBetweenFueling = statisticsViewModel.getTravelledDistanceBetweenFuelings(fuelings);
                sumFuelCostPerMonth = statisticsViewModel.getSumOfFuelCostPerMonth(fuelings);
                sumOfConsumedFuelPerMonth = statisticsViewModel.getSumOfConsumedFuelPerMonth(fuelings,fuelUnit);
                sumOfFuelingsByMonth = statisticsViewModel.getSumOfFuelingsByMonth(fuelings);

                AxesStatisticsResult axesStatisticsResult= statisticsViewModel.getSumOfPassedKmPerMonth(fuelings);
                sumOfPassedKmPerMonth = statisticsViewModel.getSumOfPassedKmPerMonth(fuelings);
                sumOfPassedKmPerMonth.clear();

                for(int j=0;j<axesStatisticsResult.size();j++){
                    if(axesStatisticsResult.get(j).getSingleStatisticsFloatValue()!=0.0f)
                        sumOfPassedKmPerMonth.add(axesStatisticsResult.get(j));
                }
                if(sumOfPassedKmPerMonth.size()==0)
                    sumOfPassedKmPerMonth=null;
            }

            statisticsViewModel.getAllPurchasedItemsByLicensePlateNumber(selectedPlateNumber).observe(lifecycleOwner, (purchasedItems) -> {
                if (fuelings.size() > 1 && purchasedItems.size() > 1) {
                    sumOfFuelCostWithPurchasedItemsPerMonth = statisticsViewModel.getSumOfFuelCostWithPurchasedItemsPerMonth(fuelings, purchasedItems);
                    sumOfPurchasedItemsCostPerMonth = statisticsViewModel.getSumOfPurchasedItemsCostPerMonth(purchasedItems);
                }

                statisticsViewModel.getAllFuelingsWithFuelTypesOrderByFuelingDate().observe(lifecycleOwner, (fuelTypes) -> {

                    if (fuelTypes.size() > 1) {
                        fuelTypesRatioAllVehicle = statisticsViewModel.getFuelTypesRatioToAllVehicle(fuelTypes);
                    }

                    statisticsViewModel.getAllItemsOrderByName().observe(lifecycleOwner, (items) -> {
                        if (purchasedItems.size() > 1 && items.size() > 0)
                            topPurchasedItems = statisticsViewModel.getTopFivePurchasedItems(purchasedItems, items);
                    });
                });
            });
            diagramSpinnerListener();
        });
    }

    private void diagramSpinnerListener() {
        diagramSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String selected = diagramSpinner.getSelectedItem().toString();

                String diagramOption1 = getString(R.string.distance_between_fuelings);
                String diagramOption3 = getString(R.string.sum_fuel_cost_per_month);
                String diagramOption4 = getString(R.string.sum_of_consumed_fuel_per_month);
                String diagramOption5 = getString(R.string.sum_of_fuel_cost_with_purchased_items_per_month);
                String diagramOption6 = getString(R.string.sum_of_fuelings_by_month);
                String diagramOption7 = getString(R.string.sum_of_passed_km_per_month);
                String diagramOption8 = getString(R.string.sum_of_purchased_items_cost_per_month);
                String diagramOption9 = getString(R.string.top_purchased_items);
                String diagramOption10 = getString(R.string.fuel_types_ratio_all_vehicle);

                if (selected.equals(diagramOption1) && travelledDistanceBetweenFueling != null) {
                    setVisibleGone();
                    barChart.setVisibility(View.VISIBLE);
                    statistic.barChart(travelledDistanceBetweenFueling);
                } else if (selected.equals(diagramOption3) && sumFuelCostPerMonth != null) {
                    setVisibleGone();
                    lineChart.setVisibility(View.VISIBLE);
                    statistic.lineChart(sumFuelCostPerMonth);

                } else if (selected.equals(diagramOption4) && sumOfConsumedFuelPerMonth != null) {
                    setVisibleGone();
                    barChart.setVisibility(View.VISIBLE);
                    statistic.barChart(sumOfConsumedFuelPerMonth);

                } else if (selected.equals(diagramOption5) && sumOfFuelCostWithPurchasedItemsPerMonth != null) {
                    setVisibleGone();
                    barChart.setVisibility(View.VISIBLE);
                    statistic.barChart(sumOfFuelCostWithPurchasedItemsPerMonth);

                } else if (selected.equals(diagramOption6) && sumOfFuelingsByMonth != null) {
                    setVisibleGone();
                    lineChart.setVisibility(View.VISIBLE);
                    statistic.lineChart(sumOfFuelingsByMonth);

                } else if (selected.equals(diagramOption7) && sumOfPassedKmPerMonth != null) {
                    setVisibleGone();
                    lineChart.setVisibility(View.VISIBLE);
                    statistic.lineChart(sumOfPassedKmPerMonth);

                } else if (selected.equals(diagramOption8) && sumOfPurchasedItemsCostPerMonth != null) {
                    setVisibleGone();
                    barChart.setVisibility(View.VISIBLE);
                    statistic.barChart(sumOfPurchasedItemsCostPerMonth);

                } else if (selected.equals(diagramOption9) && topPurchasedItems != null) {
                    setVisibleGone();
                    pieChart.setVisibility(View.VISIBLE);
                    statistic.pieChart(topPurchasedItems, false, "Db");

                } else if (selected.equals(diagramOption10) && fuelTypesRatioAllVehicle != null) {
                    setVisibleGone();
                    pieChart.setVisibility(View.VISIBLE);
                    statistic.pieChart(fuelTypesRatioAllVehicle, true, "%");
                } else {
                    statisticsYAxesTextView.setText("");
                    statisticsXAxesTextView.setText("");
                    setVisibleGone();
                    showErrorSnackBar(getString(R.string.no_enough_diagram_data));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setVisibleGone() {
        barChart.setVisibility(View.GONE);
        pieChart.setVisibility(View.GONE);
        lineChart.setVisibility(View.GONE);
    }

    private void showErrorSnackBar(String message) {
        Snackbars applicationSnackbars = new Snackbars();
        applicationSnackbars.errorSnackbar(getView(), message);
    }

    private void setNull() {
        travelledDistanceBetweenFueling = null;
        sumFuelCostPerMonth = null;
        fuelTypesRatioAllVehicle = null;
        sumOfConsumedFuelPerMonth = null;
        sumOfFuelCostWithPurchasedItemsPerMonth = null;
        sumOfFuelingsByMonth = null;
        sumOfPassedKmPerMonth = null;
        sumOfPurchasedItemsCostPerMonth = null;
        topPurchasedItems = null;
    }
}
