package gamf.tankolaskonyvelo.ui.dialog;

import gamf.tankolaskonyvelo.R;

public enum DialogArgs {

    FUELING_FRAGMENT_DATE("layoutTag", String.valueOf(R.layout.fragment_fueling),
            "resId", R.id.dateEditText),

    FUELING_FRAGMENT_TIME("layoutTag", String.valueOf(R.layout.fragment_fueling),
            "resId", R.id.timeEditText),

    REQUIRED_SERVICES_FRAGMENT_DATE("layoutTag", String.valueOf(R.layout.fragment_required_services),
            "resId", R.id.servicesDateEditText),

    REQUIRED_SERVICES_FRAGMENT_TIME("layoutTag", String.valueOf(R.layout.fragment_required_services),
            "resId", R.id.servicesTimeEditText);

    public final String layoutTag;
    public final String layoutTagKey;
    public final int resId;
    public final String resIdKey;

    DialogArgs(String layoutTagKey, String layoutTag, String resIdKey, int resId) {
        this.layoutTagKey = layoutTagKey;
        this.layoutTag = layoutTag;
        this.resIdKey = resIdKey;
        this.resId = resId;
    }
}
