package gamf.tankolaskonyvelo.ui.otherservices;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.entities.dataobjects.PurchasedItemWithVehicle;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.ui.dashboard.RequiredServicesFragment;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;
import gamf.tankolaskonyvelo.ui.uihelper.menunavigation.MenuNavigator;

public class OtherServicesFragment extends Fragment {

    private MenuNavigator menuNavigator;
    private OtherServicesViewModel otherServicesViewModel;
    private Spinner otherServiceSpinner;
    private RecyclerView otherServiceRecycleView;
    private CheckBox otherServiceCheckBox;

    public OtherServicesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        otherServicesViewModel = ViewModelProviders.of(this).get(OtherServicesViewModel.class);

        return inflater.inflate(R.layout.fragment_other_services, null);
    }

    private void defineFloatingActionButtonListener() {
        FloatingActionButton fab = getView().findViewById(R.id.floatingActionButton);

        fab.setOnClickListener(v -> {
            menuNavigator.replaceFragment(new RequiredServicesFragment(),
                    String.valueOf(R.layout.fragment_other_services));
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.nav_other_services));
        defineFloatingActionButtonListener();
        menuNavigator = new MenuNavigator(getContext());

        otherServiceRecycleView = getView().findViewById(R.id.otherServicesRecyclerView);
        otherServiceSpinner = getView().findViewById(R.id.otherServicesSpinner);
        otherServiceCheckBox = getView().findViewById(R.id.isImportantCheckBox);
        addItemToSpinner();

        otherServiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                otherServiceCheckBox.setChecked(false);
                allPurchasedItems(otherServiceSpinner.getSelectedItem().toString());
                otherServiceCheckBox.setOnCheckedChangeListener((compoundButton, checkState) -> {
                    if (checkState == true) {
                        importantPurchasedItems(otherServiceSpinner.getSelectedItem().toString());
                    } else {
                        allPurchasedItems(otherServiceSpinner.getSelectedItem().toString());
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void addItemToSpinner() {
        otherServicesViewModel.getAllVehicle().observe(this, vehicles -> {
            List<PurchasedItemEntity> purchasedItemEntityList = new ArrayList<>();

            List<String> vehicleLicensePlateNumbers = new ArrayList<String>();
            int defaultVehicleIndex = 0;

            for (int i = 0; i < vehicles.size(); i++) {
                VehicleEntity aVehicle = vehicles.get(i);
                vehicleLicensePlateNumbers.add(aVehicle.getLicensePlateNumber());

                if (aVehicle.getDefaultVehicle()) {
                    defaultVehicleIndex = i;
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, vehicleLicensePlateNumbers);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            otherServiceSpinner.setAdapter(adapter);
            otherServiceSpinner.setSelection(defaultVehicleIndex);
        });
    }

    public void importantPurchasedItems(String licensePlateNumber) {

        otherServicesViewModel.getAllImportantPurchasedItemByVehicle(licensePlateNumber).observe(this, items -> {
            List<PurchasedItemWithVehicle> purchasedItemWithVehicleList = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {
                purchasedItemWithVehicleList.add(items.get(i));
            }

            addItemToRecyclerView(purchasedItemWithVehicleList);
            if (items.size() == 0) {
                errorSnackbar(getString(R.string.no_displayable_data));
            }
        });
    }

    public void allPurchasedItems(String licensePlateNumber) {

        otherServicesViewModel.getAllPurchasedItemByVehicle(licensePlateNumber).observe(this, items -> {
            List<PurchasedItemWithVehicle> purchasedItemWithVehicleList = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {
                purchasedItemWithVehicleList.add(items.get(i));
            }

            addItemToRecyclerView(purchasedItemWithVehicleList);
            if (items.size() == 0) {
                errorSnackbar(getString(R.string.no_displayable_data));
            }
        });
    }

    public void addItemToRecyclerView(List<PurchasedItemWithVehicle> purchasedItemWithVehicleList) {
        OtherServicesAdapter adapter = new OtherServicesAdapter(purchasedItemWithVehicleList);
        otherServiceRecycleView.setAdapter(adapter);
        otherServiceRecycleView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void errorSnackbar(String message) {
        Snackbars snackbars = new Snackbars();
        snackbars.errorSnackbar(getView(), message);
    }

}
