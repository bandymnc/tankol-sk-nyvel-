package gamf.tankolaskonyvelo.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.localization.Localization;
import gamf.tankolaskonyvelo.localization.Localizer;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private MainActivityViewModel mainActivityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        Bundle bundle = getIntent().getExtras();
        Localization localization = new Localizer(this, this.getClass());
        if (bundle != null && bundle.containsKey(Localizer.LANGUAGE)) {
            localization.setLocale(bundle.getString(Localizer.LANGUAGE));
        } else {
            localization.loadDefaultLocale();
        }

        setContentView(R.layout.activity_main);
        defineDrawerLayout();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.nav_header_title));
        StringResourceGetter.init(getApplicationContext());
    }

    private void defineDrawerLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mainActivityViewModel.loadDefaultFragmentAtFirst(this);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().commit();

            NavigationView navView = findViewById(R.id.nav_view);
            navView.getMenu().getItem(mainActivityViewModel
                    .getIndexOfPreviousFragment(this))
                    .setChecked(true);
        } else {
            popExitDialog();
        }
    }

    private void popExitDialog() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.exit_header)
                .setMessage(R.string.exit_popup)
                .setNegativeButton(R.string.exit_no, null)
                .setPositiveButton(R.string.exit_yes, (arg0, arg1) -> MainActivity.this.finishAffinity()).create().show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        mainActivityViewModel.doNavigation(item, this);
        return true;
    }
}
