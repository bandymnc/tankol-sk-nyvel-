package gamf.tankolaskonyvelo.ui.recyclerviewcontroller;

import java.util.List;

public interface ListController {
    void show();
    void add(RecyclerViewItem item);
    void remove(RecyclerViewItem item);
    void clear();
    void setRecyclerViewItems(List<RecyclerViewItem> recyclerViewItems);
    List<RecyclerViewItem> getRecyclerViewItems();
}
