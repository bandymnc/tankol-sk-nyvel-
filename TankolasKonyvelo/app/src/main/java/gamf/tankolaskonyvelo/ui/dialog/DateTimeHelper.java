package gamf.tankolaskonyvelo.ui.dialog;

import java.util.Calendar;

public class DateTimeHelper {

    private static int HOURS_OF_DAY = 24;

    public static String correctTwoDigitValue(String value) {
        String correction = "0";
        if (value.length() == 1) {
            return correction + value;
        }
        return value;
    }

    public static String getValidHour(int hourOfDay) {
        boolean isHourInvalid = hourOfDay >= HOURS_OF_DAY;
        if (isHourInvalid) {
            return  "0";
        }

        return String.valueOf(hourOfDay);
    }

    public static String getActualTime() {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY) + 2;
        int minute = c.get(Calendar.MINUTE);
        String hourString = correctTwoDigitValue(getValidHour(hour));
        String minuteString = correctTwoDigitValue(String.valueOf(minute));
        String actualTime = hourString + ":" + minuteString;

        return actualTime;
    }

    public static String getActualDate() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        String yearString = DateTimeHelper.correctTwoDigitValue(String.valueOf(year));
        String monthString = DateTimeHelper.correctTwoDigitValue(String.valueOf(month));
        String dayString = DateTimeHelper.correctTwoDigitValue(String.valueOf(day));

        return yearString + "-" + monthString + "-" + dayString;
    }
}
