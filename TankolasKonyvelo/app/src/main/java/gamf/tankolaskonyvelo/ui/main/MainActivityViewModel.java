package gamf.tankolaskonyvelo.ui.main;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;
import gamf.tankolaskonyvelo.ui.about.AboutFragment;
import gamf.tankolaskonyvelo.ui.dashboard.DashboardFragment;
import gamf.tankolaskonyvelo.ui.home.HomeFragment;
import gamf.tankolaskonyvelo.ui.otherservices.OtherServicesFragment;
import gamf.tankolaskonyvelo.ui.settings.SettingsFragment;
import gamf.tankolaskonyvelo.ui.statistics.StatisticFragment;
import gamf.tankolaskonyvelo.ui.uihelper.menunavigation.MenuNavigator;
import gamf.tankolaskonyvelo.ui.vehicle.VehicleFragment;

public class MainActivityViewModel extends AndroidViewModel {

    VehicleRepository vehicleRepository;
    boolean isResumeCalled = false;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        vehicleRepository = new VehicleRepository(application, new AppExecutors());
    }

    public int getIndexOfPreviousFragment(MainActivity context){
        FragmentManager fragmentManager = context.getSupportFragmentManager();
        String tag = fragmentManager.getBackStackEntryAt(
                fragmentManager.getBackStackEntryCount() - 2).getName();

        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        Bundle bundle = fragment.getArguments();

        if (bundle == null) {
            return 0;
        } else {
            return bundle.getInt("index");
        }
    }

    public void doNavigation(MenuItem item, MainActivity context) {
        int id = item.getItemId();
        Bundle bundle = new Bundle();
        MenuNavigator menuNavigator = new MenuNavigator(context);

        switch (id) {
            case R.id.nav_home:
                bundle.putInt("index", 0);
                menuNavigator.replaceFragment(new HomeFragment(),
                        String.valueOf(R.layout.fragment_home), bundle);
                break;
            case R.id.nav_dashboard:
                bundle.putInt("index", 1);
                menuNavigator.replaceFragment(new DashboardFragment(),
                        String.valueOf(R.layout.fragment_dashboard), bundle);
                break;
            case R.id.nav_vehicle:
                bundle.putInt("index", 2);
                menuNavigator.replaceFragment(new VehicleFragment(),
                        String.valueOf(R.layout.fragment_vehicle), bundle);
                break;
            case R.id.nav_other_services:
                bundle.putInt("index", 3);
                menuNavigator.replaceFragment(new OtherServicesFragment(),
                        String.valueOf(R.layout.fragment_other_services), bundle);
                break;
            case R.id.nav_stats:
                bundle.putInt("index", 4);
                menuNavigator.replaceFragment(new StatisticFragment(),
                        String.valueOf(R.layout.fragment_statistic), bundle);
                break;
            case R.id.nav_settings:
                bundle.putInt("index", 5);
                menuNavigator.replaceFragment(new SettingsFragment(),
                        String.valueOf(R.layout.fragment_settings), bundle);
                break;
            case R.id.nav_about:
                bundle.putInt("index", 6);
                menuNavigator.replaceFragment(new AboutFragment(),
                        String.valueOf(R.id.nav_about));
                break;
            default:
                return;
        }
        DrawerLayout drawer = context.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public LiveData<Integer> getVehicleAmount() {
        return vehicleRepository.getVehicleAmount();
    }

    public void loadDefaultFragmentAtFirst(MainActivity mainActivity) {

        getVehicleAmount().observe(mainActivity, carAmount -> {
            if (!isResumeCalled) {
                boolean doesUserHaveCar = carAmount > 0;
                if (doesUserHaveCar) {
                    loadHomeFragment(mainActivity);
                } else {
                    loadVehicleFragment(mainActivity);
                }
                isResumeCalled = true;
            }
        });
    }

    private void loadHomeFragment(MainActivity mainActivity) {
        MenuNavigator menuNavigator = new MenuNavigator(mainActivity);

        menuNavigator.replaceFragment(new HomeFragment(),
                String.valueOf(R.layout.fragment_home));
    }

    private void loadVehicleFragment(MainActivity mainActivity) {
        MenuNavigator menuNavigator = new MenuNavigator(mainActivity);
        menuNavigator.replaceFragment(new VehicleFragment(),
                String.valueOf(R.layout.fragment_vehicle));
    }
}
