package gamf.tankolaskonyvelo.ui.statistics;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.entities.dataobjects.FuelingWithFuelType;
import gamf.tankolaskonyvelo.repositories.FuelingRepository;
import gamf.tankolaskonyvelo.repositories.ItemRepository;
import gamf.tankolaskonyvelo.repositories.PurchasedItemRepository;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.AverageTravelledDistanceByOneFueling;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.FuelTypesRatioToAllVehicle;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.SumOfConsumedFuelPerMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.SumOfFuelCostPerMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.SumOfFuelCostWithPurchasedItemsPerMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.SumOfFuelingsPerMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.SumOfPassedKmPerMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.SumOfPurchasedItemsCostPerMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.TopPurchasedItems;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.TravelledDistanceBetweenFuelings;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;

public class StatisticsViewModel extends AndroidViewModel {
    FuelingRepository fuelingRepository;
    VehicleRepository vehicleRepository;
    PurchasedItemRepository purchasedItemRepository;
    ItemRepository itemRepository;

    public StatisticsViewModel(@NonNull Application application) {
        super(application);

        fuelingRepository = new FuelingRepository(application, new AppExecutors());
        vehicleRepository = new VehicleRepository(application, new AppExecutors());
        purchasedItemRepository = new PurchasedItemRepository(application, new AppExecutors());
        itemRepository = new ItemRepository(application, new AppExecutors());
    }

    public LiveData<List<FuelingEntity>> getAllFuelingsByLicensePlateNumber(String licensePlateNumber) {
        return fuelingRepository.getAllFuelingsByLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<VehicleEntity>> getAllVehicle() {
        return vehicleRepository.getAllVehicle();
    }

    public LiveData<List<PurchasedItemEntity>> getAllPurchasedItemsByLicensePlateNumber(String licensePlateNumber) {
        return purchasedItemRepository.getAllPurchasedItemsByLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<ItemEntity>> getAllItemsOrderByName() {
        return itemRepository.getAllItemOrderByName();
    }

    public LiveData<List<FuelingWithFuelType>> getAllFuelingsWithFuelTypesOrderByFuelingDate() {
        return fuelingRepository.getAllFuelingsWithFuelTypesOrderByFuelingDate();
    }
    public LiveData<String> getDefaultFuelUnit(String plateNumber){
        return vehicleRepository.getDefaultFuelUnit(plateNumber);
    }

    // STATISTICS
    public AxesStatisticsResult getTravelledDistanceBetweenFuelings(List<FuelingEntity> fuelings) {
        StatisticsCalculator statisticsCalculation = new TravelledDistanceBetweenFuelings(fuelings);
        AxesStatisticsResult travelledDistancesBetweenFuelings = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return travelledDistancesBetweenFuelings;
    }

    public AxesStatisticsResult getAverageTravelledDistanceByOneFueling(List<FuelingEntity> fuelings) {
        StatisticsCalculator statisticsCalculation = new AverageTravelledDistanceByOneFueling(fuelings);
        AxesStatisticsResult travelledDistancesBetweenFuelings = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return travelledDistancesBetweenFuelings;
    }

    public AxesStatisticsResult getSumOfFuelCostPerMonth(List<FuelingEntity> fuelings) {
        StatisticsCalculator statisticsCalculation = new SumOfFuelCostPerMonth(fuelings);
        AxesStatisticsResult sumOfFuelCostPerMonth = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return sumOfFuelCostPerMonth;
    }

    public AxesStatisticsResult getSumOfFuelingsByMonth(List<FuelingEntity> fuelings) {
        StatisticsCalculator statisticsCalculation = new SumOfFuelingsPerMonth(fuelings);
        AxesStatisticsResult sumOfFuelingsByMonth = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return sumOfFuelingsByMonth;
    }

    public AxesStatisticsResult getSumOfFuelCostWithPurchasedItemsPerMonth(List<FuelingEntity> fuelings, List<PurchasedItemEntity> purchasedItems) {
        StatisticsCalculator statisticsCalculation = new SumOfFuelCostWithPurchasedItemsPerMonth(fuelings, purchasedItems);
        AxesStatisticsResult result = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return result;
    }

    public AxesStatisticsResult getSumOfPurchasedItemsCostPerMonth(List<PurchasedItemEntity> purchasedItems) {
        StatisticsCalculator statisticsCalculation = new SumOfPurchasedItemsCostPerMonth(purchasedItems);
        AxesStatisticsResult result = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return result;
    }

    public AxesStatisticsResult getTopFivePurchasedItems(List<PurchasedItemEntity> purchasedItems, List<ItemEntity> items) {
        StatisticsCalculator statisticsCalculation = new TopPurchasedItems(purchasedItems, items, 5);
        AxesStatisticsResult result = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return result;
    }

    public AxesStatisticsResult getSumOfPassedKmPerMonth(List<FuelingEntity> fuelings) {
        StatisticsCalculator statisticsCalculation = new SumOfPassedKmPerMonth(fuelings);
        AxesStatisticsResult result = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return result;
    }

    public AxesStatisticsResult getSumOfConsumedFuelPerMonth(List<FuelingEntity> fuelings,String fuelUnit) {
        StatisticsCalculator statisticsCalculation = new SumOfConsumedFuelPerMonth(fuelings,fuelUnit);
        AxesStatisticsResult result = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return result;
    }

    public AxesStatisticsResult getFuelTypesRatioToAllVehicle(List<FuelingWithFuelType> fuelingWithFuelTypes) {
        StatisticsCalculator statisticsCalculation = new FuelTypesRatioToAllVehicle(fuelingWithFuelTypes);
        AxesStatisticsResult result = (AxesStatisticsResult) statisticsCalculation.calculateStatisics();

        return result;
    }
}
