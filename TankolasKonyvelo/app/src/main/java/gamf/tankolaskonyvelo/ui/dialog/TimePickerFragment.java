package gamf.tankolaskonyvelo.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private static int HOUR_CORRIGATION = 2;
    private Fragment containerFragment;

    public static TimePickerFragment getNewInstance(DialogArgs dialogArgs, Fragment fragment) {
        TimePickerFragment timePickerFragment;
        Bundle argsBundle = new Bundle();

        argsBundle.putString(dialogArgs.layoutTagKey, dialogArgs.layoutTag);
        argsBundle.putInt(dialogArgs.resIdKey, dialogArgs.resId);

        timePickerFragment = new TimePickerFragment();
        timePickerFragment.setArguments(argsBundle);
        timePickerFragment.containerFragment = fragment;

        return timePickerFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        return new TimePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                this, hour + HOUR_CORRIGATION, minute, DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String hourString = DateTimeHelper.getValidHour(hourOfDay);
        String minuteString = String.valueOf(minute);

        final Calendar c = Calendar.getInstance();
        int hourCheck = c.get(Calendar.HOUR_OF_DAY) + HOUR_CORRIGATION;
        int minuteCheck = c.get(Calendar.MINUTE);

        if (hourOfDay > hourCheck || minute > minuteCheck) {
            hourString = DateTimeHelper.correctTwoDigitValue(String.valueOf(hourCheck));
            minuteString = DateTimeHelper.correctTwoDigitValue(String.valueOf(minuteCheck));
            new Snackbars().errorSnackbar(containerFragment.getView(), "Jövőbeli időpont megadása nem támogatott.");
        } else {
            hourString = DateTimeHelper.correctTwoDigitValue(hourString);
            minuteString = DateTimeHelper.correctTwoDigitValue(minuteString);

        }
        ((EditText) DialogFragmentManager.removeDialogFragment(this))
                .setText(hourString + ":" + minuteString);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        DialogFragmentManager.removeDialogFragment(this);
    }
}