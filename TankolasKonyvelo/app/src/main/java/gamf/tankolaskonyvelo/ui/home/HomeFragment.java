package gamf.tankolaskonyvelo.ui.home;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelTypeConstants;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;
import gamf.tankolaskonyvelo.ui.uihelper.menunavigation.MenuNavigator;
import gamf.tankolaskonyvelo.ui.vehicle.VehicleFragment;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    private Spinner changeVehicleSpinner;
    private EditText lastFuelingEditText;
    private EditText gasStationNameEditText;
    private EditText fuelTypeEditText;
    private EditText fuelPriceEditText;
    private EditText fuelQuantityEditText;
    private EditText totalFuelPriceEditText;
    private EditText fuelMeterStateEditText;
    private EditText lengthOfWaySinceAllTimeEditText;
    private EditText allUsedFuelQuantitySinceAllTimeEditText;
    private EditText travelledRoadSinceLastFuellingEditText;
    private EditText countOfFuelingEditText;
    private EditText allFuelCostEditText;
    private EditText sumOfAllCostEditText;
    private EditText oneFuelingAvgTraveledRouteEditText;
    private EditText avgConsumptionEditText;
    private String fuelUnit;

    private FuelTypeConstants fuelTypeConstants;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.InitializeFuelTypeConstants(this);

        return inflater.inflate(R.layout.fragment_home, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.nav_home));
        defineFloatingActionButtonListener();

        changeVehicleSpinner = getView().findViewById(R.id.changeVehicleSpinner);
        lastFuelingEditText = getView().findViewById(R.id.lastFuelingEditText);
        gasStationNameEditText = getView().findViewById(R.id.gasStationNameEditText);
        fuelTypeEditText = getView().findViewById(R.id.fuelTypeEditText);
        fuelPriceEditText = getView().findViewById(R.id.fuelPriceEditText);
        fuelQuantityEditText = getView().findViewById(R.id.fuelQuentityEditText);
        totalFuelPriceEditText = getView().findViewById(R.id.totalFuelPriceEditText);
        fuelMeterStateEditText = getView().findViewById(R.id.kmClockPositionEditText);
        lengthOfWaySinceAllTimeEditText = getView().findViewById(R.id.travelledRoadLengthEditText);
        allUsedFuelQuantitySinceAllTimeEditText = getView().findViewById(R.id.wornOutFuelEditText);
        travelledRoadSinceLastFuellingEditText = getView().findViewById(R.id.travelledRoadSinceLastFuellingEditText);
        countOfFuelingEditText = getView().findViewById(R.id.countOfFuelingEditText);
        allFuelCostEditText = getView().findViewById(R.id.allFuelCostEditText);
        sumOfAllCostEditText = getView().findViewById(R.id.allCostEditText);
        oneFuelingAvgTraveledRouteEditText = getView().findViewById(R.id.oneFuelingAvgTraveledRouteEditText);
        avgConsumptionEditText = getView().findViewById(R.id.avgConsumptionEditText);

        changeVehicleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                fillLastFuelingFields(changeVehicleSpinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getActivity(), R.string.choose_vehicle, Toast.LENGTH_LONG).show();
            }
        });

        fillViewWithData();
    }

    private void defineFloatingActionButtonListener() {
        FloatingActionButton fab = getView().findViewById(R.id.floatingActionButton);

        fab.setOnClickListener(v -> {
            MenuNavigator menuNavigator = new MenuNavigator(getContext());
            menuNavigator.replaceFragment(new VehicleFragment(), String.valueOf(R.layout.fragment_home));
        });
    }

    private void fillViewWithData() {
        fillSpinnerWithVehiclePlateNumbers();
    }

    private void fillSpinnerWithVehiclePlateNumbers() {
        homeViewModel.getAllVehicle().observe(this, vehicles -> {
            List<String> vehicleLicensePlateNumbers = new ArrayList<>();
            int defaultVehicleIndex = 0;

            for (int i = 0; i < vehicles.size(); i++) {
                VehicleEntity aVehicle = vehicles.get(i);
                vehicleLicensePlateNumbers.add(aVehicle.getLicensePlateNumber());

                if (aVehicle.getDefaultVehicle()) {
                    defaultVehicleIndex = i;
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, vehicleLicensePlateNumbers);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            changeVehicleSpinner.setAdapter(adapter);
            changeVehicleSpinner.setSelection(defaultVehicleIndex);
        });
    }

    private void fillLastFuelingFields(String licensePlateNumber) {
        homeViewModel.getLastFuelingByVehicleLicensePlateNumber(licensePlateNumber).observe(this, fuelingWithFuelType -> {
            boolean hasFuelingData = fuelingWithFuelType != null;
            fuelTypeConstants = FuelTypeConstants.getInstance();

            if (hasFuelingData) {

                if (fuelingWithFuelType.getFuelName().equals("Áram")) {
                    fuelUnit = "Kw";
                    fuelPriceEditText.setText(Double.toString(fuelingWithFuelType.getUnitPriceOfFuel()) + " Ft");
                } else {
                    fuelUnit = "L";
                    fuelPriceEditText.setText(Double.toString(fuelingWithFuelType.getUnitPriceOfFuel()) + " Ft/L");
                }

                lastFuelingEditText.setText(fuelingWithFuelType.getFuelingDate());
                gasStationNameEditText.setText(fuelingWithFuelType.getNameOfGasStation());
                fuelTypeEditText.setText(fuelingWithFuelType.getFuelName());

                fuelQuantityEditText.setText(Double.toString(fuelingWithFuelType.getFuelQuantity()) + " " + fuelUnit);

                int fuelTypeId = fuelTypeConstants.getFuelTypeIdByName(fuelingWithFuelType.getFuelName());
                FuelTypeEntity fuelTypeEntity = fuelTypeConstants.getFuelTypeEntityByFuelTypeId(fuelTypeId);
                if (fuelTypeEntity.getUnit().equalsIgnoreCase("kw")) {
                    totalFuelPriceEditText.setText(Double.toString(fuelingWithFuelType.getUnitPriceOfFuel()) + " Ft");
                } else {
                    totalFuelPriceEditText.setText(Double.toString(fuelingWithFuelType.getUnitPriceOfFuel() * fuelingWithFuelType.getFuelQuantity()) + " Ft");
                }

                fuelMeterStateEditText.setText(Double.toString(fuelingWithFuelType.getKmMeterState()) + " Km");

                fillLastFuelingStatisticsFields(changeVehicleSpinner.getSelectedItem().toString());
            } else {
                cleanEditTexts();
                showErrorSnackBar(getString(R.string.no_fueling_data));
            }
        });
    }

    private void fillLastFuelingStatisticsFields(String licensePlateNumber) {
        List<FuelingEntity> fuelingEntitiesOfSelectedVehicle = new ArrayList<>();

        homeViewModel.getAllFuelingsByLicensePlateNumber(licensePlateNumber).observe(this, fuelingEntities -> {
            fuelingEntitiesOfSelectedVehicle.addAll(fuelingEntities);

            if (fuelingEntities.size() < 2) {
                showWarningSnackBar(getString(R.string.not_enough_fueling_data));
                cleanStatisticsEditTexts();
            } else {
                Double allUsedFuelQuantity = homeViewModel.getAllUsedFuelQuantity(fuelingEntitiesOfSelectedVehicle);
                Double lengthOfWaySicneAllTime = homeViewModel.getLengthOfWaySinceAllTime(fuelingEntitiesOfSelectedVehicle);
                Double travelledRoadSinceLastFuelling = homeViewModel.getTravelledRoadSinceLastFuelling(fuelingEntitiesOfSelectedVehicle);
                Double oneFuelingAvgTraveledRouteSinceAllTime = homeViewModel.getOneFuelingAvgTraveledRoute(fuelingEntitiesOfSelectedVehicle);
                Double countOfFuelings = homeViewModel.getCountOfFuelings(fuelingEntitiesOfSelectedVehicle);
                Double sumOfFuelCost = homeViewModel.getSumOfFuelCost(fuelingEntitiesOfSelectedVehicle);
                Double averageConsumption = homeViewModel.getAverageConsumption(fuelingEntitiesOfSelectedVehicle);

                avgConsumptionEditText.setText(Double.toString(averageConsumption) + " " + fuelUnit + "/Km");
                allUsedFuelQuantitySinceAllTimeEditText.setText(Double.toString(allUsedFuelQuantity) + " " + fuelUnit);
                lengthOfWaySinceAllTimeEditText.setText(Integer.toString(lengthOfWaySicneAllTime.intValue()) + " Km");
                travelledRoadSinceLastFuellingEditText.setText(Integer.toString(travelledRoadSinceLastFuelling.intValue()) + " Km");
                oneFuelingAvgTraveledRouteEditText.setText(Double.toString(((long) (oneFuelingAvgTraveledRouteSinceAllTime * 1e2)) / 1e2) + " Km");
                countOfFuelingEditText.setText(Integer.toString(countOfFuelings.intValue()) + " Db");
                allFuelCostEditText.setText(Double.toString(sumOfFuelCost) + " Ft");

                fillAllCostOfVehicleField(licensePlateNumber);
            }
        });
    }

    private void fillAllCostOfVehicleField(String licensePlateNumber) {
        homeViewModel.getAllFuelingsByLicensePlateNumber(licensePlateNumber).observe(this, fuelings -> {
            homeViewModel.getAllPurchasedItemsByLicensePlateNumber(licensePlateNumber).observe(this, purchasedItems -> {
                Double sumOfAllCost = homeViewModel.getSumOfAllCost(fuelings, purchasedItems);
                sumOfAllCostEditText.setText(Double.toString(sumOfAllCost) + " Ft");
            });
        });
    }

    private void cleanEditTexts() {
        lastFuelingEditText.setText(R.string.no_data);
        gasStationNameEditText.setText(R.string.no_data);
        fuelTypeEditText.setText(R.string.no_data);
        fuelPriceEditText.setText(R.string.no_data);
        fuelQuantityEditText.setText(R.string.no_data);
        totalFuelPriceEditText.setText(R.string.no_data);
        fuelMeterStateEditText.setText(R.string.no_data);

        cleanStatisticsEditTexts();
    }

    private void cleanStatisticsEditTexts() {
        allUsedFuelQuantitySinceAllTimeEditText.setText(R.string.no_data);
        lengthOfWaySinceAllTimeEditText.setText(R.string.no_data);
        travelledRoadSinceLastFuellingEditText.setText(R.string.no_data);
        countOfFuelingEditText.setText(R.string.no_data);
        allFuelCostEditText.setText(R.string.no_data);
        sumOfAllCostEditText.setText(R.string.no_data);
        oneFuelingAvgTraveledRouteEditText.setText(R.string.no_data);
        avgConsumptionEditText.setText(R.string.no_data);
    }

    private void showErrorSnackBar(String message) {
        Snackbars applicationSnackbars = new Snackbars();
        applicationSnackbars.errorSnackbar(getView(), message);
    }

    private void showWarningSnackBar(String message) {
        Snackbars applicationSnackbars = new Snackbars();
        applicationSnackbars.warningSnackbar(getView(), message);
    }
}
