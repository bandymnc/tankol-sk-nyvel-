package gamf.tankolaskonyvelo.ui.vehicle;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.ui.home.HomeFragment;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;
import gamf.tankolaskonyvelo.ui.uihelper.keyboardhide.KeyboardHider;
import gamf.tankolaskonyvelo.ui.uihelper.menunavigation.MenuNavigator;

public class VehicleFragment extends Fragment {

    private MenuNavigator menuNavigator;
    private VehicleViewModel vehicleViewModel;
    private EditText licensePlateEditText;
    private EditText categoryEditText;
    private Spinner fuelTypeSpinner;
    private Button saveVehicleButton;
    private String licensePlateNumber = null;
    private String category = null;
    private String chosenFuelType;
    private int fuelId;
    private List<VehicleEntity> vehicleEntityList = new ArrayList<>();
    private boolean alreadyHaveDefaultVehicle=false;

    public VehicleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        vehicleViewModel = ViewModelProviders.of(this).get(VehicleViewModel.class);

        return inflater.inflate(R.layout.fragment_vehicle, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.nav_vehicle));
        menuNavigator = new MenuNavigator(getContext());

        licensePlateEditText = getView().findViewById(R.id.licensePlateEditText);
        categoryEditText = getView().findViewById(R.id.categoryEditText);
        fuelTypeSpinner = getView().findViewById(R.id.fuelTypeSpinner);
        saveVehicleButton = getView().findViewById(R.id.saveVehicleButton);

        getAllVehicle();
        addItemToSpinner();
        editTextListener();

        saveVehicleButton.setOnClickListener(view1 -> {
            KeyboardHider.hideKeyboard(this.getActivity());
            if (licensePlateNumber != null && category != null && !isSame(licensePlateNumber)) {
                VehicleEntity newVehicleEntity = new VehicleEntity(false, category, licensePlateNumber, fuelId);

                if(!alreadyHaveDefaultVehicle){
                    newVehicleEntity.setDefaultVehicle(true);
                }

                vehicleViewModel.insert(newVehicleEntity);
                informationSnackBar(getString(R.string.successful_save));

                menuNavigator.replaceFragment(new HomeFragment(), String.valueOf(R.layout.fragment_home));
            } else {
                errorSnackbar(getString(R.string.please_correct_the_errors));
            }
        });
    }

    private void editTextListener() {
        licensePlateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                licensePlateNumber = licensePlateEditText.getText().toString();

                if (isSame(licensePlateNumber)) {
                    licensePlateEditText.setError(getString(R.string.vehicle_already_added));
                }

                if (TextUtils.isEmpty(licensePlateEditText.getText().toString())) {
                    licensePlateNumber = null;
                    licensePlateEditText.setError(getString(R.string.fill_the_field));
                    return;
                }
            }
        });

        categoryEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                category = categoryEditText.getText().toString();
                if (TextUtils.isEmpty(categoryEditText.getText().toString())) {
                    category = null;
                    categoryEditText.setError(getString(R.string.fill_the_field));
                    return;
                }
            }
        });
    }

    private void addItemToSpinner() {
        vehicleViewModel.getAllFuelTypesOrderByName().observe(this, fuels -> {
            List<String> fuelTypeList = new ArrayList<String>();
            List<FuelTypeEntity> fuelTypeEntityList = new ArrayList<>();

            for (int i = 0; i < fuels.size(); i++) {
                FuelTypeEntity fuelTypeEntity = fuels.get(i);
                fuelTypeEntityList.add(fuelTypeEntity);
                fuelTypeList.add(fuelTypeEntity.getName());
            }

            fuelTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    chosenFuelType = fuelTypeSpinner.getSelectedItem().toString();
                    setIndexOfFuelType(fuelTypeEntityList, chosenFuelType);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, fuelTypeList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            fuelTypeSpinner.setAdapter(adapter);
        });
    }

    private void setIndexOfFuelType(List<FuelTypeEntity> fuelTypeEntityList, String fuelName) {

        for (int i = 0; i < fuelTypeEntityList.size(); i++) {
            if (fuelTypeEntityList.get(i).getName().equals(fuelName)) {
                fuelId = fuelTypeEntityList.get(i).getId();
            }
        }
    }

    private void errorSnackbar(String message) {
        Snackbars snackbars = new Snackbars();
        snackbars.errorSnackbar(getView(), message);
    }

    private void informationSnackBar(String message) {
        Snackbars snackbars = new Snackbars();
        snackbars.informationSnackbar(getView(), message);
    }

    private void getAllVehicle() {

        vehicleViewModel.getAllVehicle().observe(this, vehicles -> {

            for (int i = 0; i < vehicles.size(); i++) {
                vehicleEntityList.add(vehicles.get(i));
                alreadyHaveDefaultVehicle=true;
            }
        });
    }

    public boolean isSame(String newLicensePlateNumber) {
        for (int i = 0; i < vehicleEntityList.size(); i++) {
            if (vehicleEntityList.get(i).getLicensePlateNumber().toLowerCase().equals(newLicensePlateNumber.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}

