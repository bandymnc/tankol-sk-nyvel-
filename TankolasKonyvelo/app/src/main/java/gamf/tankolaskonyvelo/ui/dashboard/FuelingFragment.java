package gamf.tankolaskonyvelo.ui.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.ui.dialog.DatePickerFragment;
import gamf.tankolaskonyvelo.ui.dialog.DateTimeHelper;
import gamf.tankolaskonyvelo.ui.dialog.DialogArgs;
import gamf.tankolaskonyvelo.ui.dialog.DialogFragmentManager;
import gamf.tankolaskonyvelo.ui.dialog.TimePickerFragment;
import gamf.tankolaskonyvelo.ui.main.MainActivity;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;
import gamf.tankolaskonyvelo.ui.uihelper.keyboardhide.KeyboardHider;
import gamf.tankolaskonyvelo.ui.uihelper.menunavigation.MenuNavigator;

public class FuelingFragment extends Fragment implements FuelingCreationHelperListener, InsertionListener {

    private MenuNavigator menuNavigator;
    private FuelingViewModel fuelingViewModel;
    private Bundle argBundle;
    private boolean doCombinedUsage;
    private boolean isSaveSuccessful;
    private boolean isMileometerValid;
    private String mileometerState;

    private Spinner vehicleSpinner;
    private EditText mileometerStateEditText;
    private Spinner fuelTypeSpinner;
    private EditText fuelMeterStateEditText;
    private EditText gasStationEditText;
    private EditText fuelUnitPriceEditText;
    private EditText refuelingAmountEditText;
    private EditText dateEditText;
    private EditText timeEditText;

    public FuelingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            this.doCombinedUsage = getArguments().getBoolean("doCombinedUsage");
        }

        return inflater.inflate(R.layout.fragment_fueling, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuNavigator = new MenuNavigator(getContext());
        getActivity().setTitle(getString(R.string.nav_fueling));
        fuelingViewModel = ViewModelProviders.of(this).get(FuelingViewModel.class);

        argBundle = new Bundle();
        argBundle.putBoolean("doCombinedUsage", true);
        initViews();
        initPickers();
        initSaveListener();
        initDefaultVehicleSelection();
    }

    public void initDefaultVehicleSelection() {
        fuelingViewModel.getAllVehicle().observe(this, vehicles -> {
            List<String> vehicleLicensePlateNumbers = new ArrayList<>();
            int defaultVehicleIndex = 0;
            VehicleEntity selectedVehicle = null;

            for (int i = 0; i < vehicles.size(); i++) {
                VehicleEntity aVehicle = vehicles.get(i);
                vehicleLicensePlateNumbers.add(aVehicle.getLicensePlateNumber());

                if (aVehicle.getDefaultVehicle()) {
                    selectedVehicle = aVehicle;
                    defaultVehicleIndex = i;
                    fuelingViewModel.setSelectedVehicleId(aVehicle.getId());
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, vehicleLicensePlateNumbers);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            vehicleSpinner.setAdapter(adapter);
            vehicleSpinner.setSelection(defaultVehicleIndex);

            if (selectedVehicle != null) {
                String licensePlate = selectedVehicle.getLicensePlateNumber();

                initFuelTypeSpinner(selectedVehicle);
                initMileometerState(licensePlate);
                initFuelType(licensePlate);
                initVehicleSpinnerListener();
                mileometerStateTypeListener();
            }

        });
    }

    public void initVehicleSpinnerListener() {
        vehicleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String licensePlate = vehicleSpinner.getSelectedItem().toString();
                initMileometerState(licensePlate);
                initFuelType(licensePlate);
                mileometerStateTypeListener();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void initMileometerState(String licensePlate) {
        mileometerStateEditText.setError(null);
        mileometerState = null;
        if (licensePlate.isEmpty()) {
            licensePlate = "";
        }

        fuelingViewModel.getLastFueling(licensePlate).observe(this, lastFueling -> {
            if (lastFueling == null) {
                return;
            }
            mileometerState = String.valueOf(lastFueling.getKmMeterState());
            mileometerStateEditText.setText(mileometerState);

        });
        mileometerStateEditText.setText("");
    }

    public void initFuelType(String licensePlate) {
        fuelingViewModel.getFuelTypeByLicensePlate(licensePlate).observe(this, fuelType -> {
            Adapter adapter = fuelTypeSpinner.getAdapter();
            if (adapter == null) {
                return;
            }
            int n = adapter.getCount();
            for (int i = 0; i < n; i++) {
                String item = adapter.getItem(i).toString();
                if (item.equals(fuelType.getName())) {
                    fuelTypeSpinner.setSelection(i);
                    return;
                }
            }
        });
    }

    private void initFuelTypeSpinner(VehicleEntity selectedVehicle) {
        if (selectedVehicle == null) {
            return;
        }

        fuelingViewModel.getAllFuelType().observe(this, fuelTypes -> {
            List<String> fuelTypeNames = new ArrayList<>();
            int defaultFuelTypeIndex = 0;

            for (int i = 0; i < fuelTypes.size(); i++) {
                FuelTypeEntity aFuelType = fuelTypes.get(i);
                fuelTypeNames.add(aFuelType.getName());

                if (selectedVehicle.getDefaultFuel() == aFuelType.getId()) {
                    defaultFuelTypeIndex = i;
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(FuelingFragment.this.getContext(),
                    android.R.layout.simple_spinner_item, fuelTypeNames);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            fuelTypeSpinner.setAdapter(adapter);
            fuelTypeSpinner.setSelection(defaultFuelTypeIndex);
        });
    }

    private void initSaveListener() {
        Button button = getView().findViewById(R.id.saveFuelingButton);
        if (doCombinedUsage) {
            button.setText(R.string.next_button);
        } else {
            button.setText(R.string.save);
        }

        button.setOnClickListener(v -> {
            KeyboardHider.hideKeyboard(this.getActivity());

            fuelingViewModel.getVehicleAmount().observe(this, vehicleAmount -> {
                if (vehicleAmount > 0) {
                    isSaveSuccessful = saveFueling();
                    if (!isSaveSuccessful) {
                        return;
                    }
                } else {
                    new Snackbars().errorSnackbar(getView(), getString(R.string.add_vehicle_at_first));
                }
            });
        });
    }

    private void mileometerStateTypeListener() {
        mileometerStateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String mileometer = s.toString();
                isMileometerValid = isMileometerValid(mileometer);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }
        });
    }

    private boolean isMileometerValid(String mileometerStateText) {
        int modifiedMileometerState;
        try {
            modifiedMileometerState = Integer.valueOf(mileometerStateText);
            if (mileometerState == null) {
                return true;
            }
            Integer mileometerDouble = Integer.valueOf(mileometerState);
            if (modifiedMileometerState < mileometerDouble) {
                mileometerStateEditText.setError(getString(R.string.possible_lowest_value) + (mileometerDouble + 1));
                return false;
            } else if (modifiedMileometerState == mileometerDouble) {
                mileometerStateEditText.setError(getString(R.string.mileometer_increasing_required));
                return false;
            }
        } catch(Exception e) {
            return false;
        }
        return true;
    }

    private boolean saveFueling() {
        if (!areInputsValid()) {
            return false;
        }

        String selectedLicensePlate = vehicleSpinner.getSelectedItem().toString();
        String selectedFuelType = fuelTypeSpinner.getSelectedItem().toString();
        FuelingCreationHelper fch = new FuelingCreationHelper(this,
                fuelingViewModel, selectedLicensePlate, selectedFuelType);
        fch.addListener(this);

        return true;
    }

    private boolean areInputsValid() {
        int messageRedId = -1;

        if (!isMileometerValid) {
            messageRedId = R.string.invalid_mileometer_state;
        } else if (isEditTextEmpty(mileometerStateEditText)) {
            messageRedId = R.string.mileometer_required;
        } else if (isEditTextEmpty(fuelMeterStateEditText)) {
            messageRedId = R.string.fuelmeter_state_required;
        } else if (isEditTextEmpty(gasStationEditText)) {
            messageRedId = R.string.gas_station_name_required;
        } else if (isEditTextEmpty(dateEditText)) {
            messageRedId = R.string.date_required;
        } else if (isEditTextEmpty(timeEditText)) {
            messageRedId = R.string.time_required;
        } else if (isEditTextEmpty(refuelingAmountEditText)) {
            messageRedId = R.string.fuel_amount_required;
        }

        if (messageRedId != -1) {
            new Snackbars().errorSnackbar(getView(), getString(messageRedId));
            return false;
        }
        return true;
    }

    public static boolean isEditTextEmpty(EditText editText) {
        String editTextContent = editText.getText().toString();
        if (editTextContent.isEmpty()) {
            return true;
        }
        return false;
    }

    @Override
    public void onFuelHelperTerminated(int fuelTypeId, int vehicleId) {
        fuelingViewModel.setSelectedVehicleId(vehicleId);

        String fuelingDate = dateEditText.getText().toString() + " " + timeEditText.getText().toString();
        String nameOfGasStation = gasStationEditText.getText().toString();
        Double unitPriceOfFuel = Double.valueOf(fuelUnitPriceEditText.getText().toString());
        Double fuelQuantity = Double.valueOf(refuelingAmountEditText.getText().toString());
        Double fuelMeterState = Double.valueOf(fuelMeterStateEditText.getText().toString());
        int kmMeterState = Integer.valueOf(mileometerStateEditText.getText().toString());

        fuelingViewModel.insertFueling(new FuelingEntity(fuelingDate, nameOfGasStation, unitPriceOfFuel,
                fuelQuantity, fuelMeterState, kmMeterState, fuelTypeId, vehicleId), this);

        clearViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        dateEditText.setText(DateTimeHelper.getActualDate());
        timeEditText.setText(DateTimeHelper.getActualTime());
    }

    @Override
    public void onRecordInserted(long idOfInserted) {
        if (!isSaveSuccessful) {
            return;
        }

        new Snackbars().informationSnackbar(getView(), getString(R.string.successful_save));
        if (!doCombinedUsage) {
            ((MainActivity) getContext()).onBackPressed();
        } else {
            String tag = String.valueOf(R.layout.fragment_required_services);
            argBundle.putString("date", dateEditText.getText().toString());
            argBundle.putString("time", timeEditText.getText().toString());
            argBundle.putInt("selectedVehicleId", fuelingViewModel.getSelectedVehicleId());
            menuNavigator.replaceFragment(new RequiredServicesFragment(), tag, argBundle);
        }
    }

    private void initPickers() {
        DialogFragmentManager.definePicker(this,
                DatePickerFragment.getNewInstance(DialogArgs.FUELING_FRAGMENT_DATE, this),
                R.id.dateEditText);

        DialogFragmentManager.definePicker(this,
                TimePickerFragment.getNewInstance(DialogArgs.FUELING_FRAGMENT_TIME, this),
                R.id.timeEditText);
    }

    private void initViews() {
        vehicleSpinner = getView().findViewById(R.id.fuelingVehicleSpinner);
        mileometerStateEditText = getView().findViewById(R.id.mileometerStateEditText);
        fuelTypeSpinner = getView().findViewById(R.id.fuelTypeSpinner);
        fuelMeterStateEditText = getView().findViewById(R.id.fuelMeterEditText);
        gasStationEditText = getView().findViewById(R.id.gasStationNameEditText);
        fuelUnitPriceEditText = getView().findViewById(R.id.fuelUnitPriceEditText);
        refuelingAmountEditText = getView().findViewById(R.id.refuelingAmountEditText);
        dateEditText = getView().findViewById(R.id.dateEditText);
        timeEditText = getView().findViewById(R.id.timeEditText);
        dateEditText.setText(DateTimeHelper.getActualDate());
        timeEditText.setText(DateTimeHelper.getActualTime());
    }

    private void clearViews() {
        mileometerStateEditText.setText("");
        fuelMeterStateEditText.setText("");
        gasStationEditText.setText("");
        fuelUnitPriceEditText.setText("");
        refuelingAmountEditText.setText("");
        dateEditText.setText("");
        timeEditText.setText("");
    }
}