package gamf.tankolaskonyvelo.ui.dashboard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.ui.uihelper.menunavigation.MenuNavigator;

public class DashboardFragment extends Fragment {

    MenuNavigator menuNavigator;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, null);
    }

    public void defineCardViewListener(CardView cardView, final Fragment creatorFragment,
                                              final Fragment fragment, final String tag, boolean doCombinedUsage) {
        Bundle argBundle = new Bundle();

        if (doCombinedUsage) {
            argBundle.putBoolean("doCombinedUsage", true);
        } else {
            argBundle.putBoolean("doCombinedUsage", false);
        }

        cardView.setOnClickListener(v -> {
            menuNavigator.replaceFragment(fragment, tag, argBundle);
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        menuNavigator = new MenuNavigator(getContext());

        defineCardViewListener(getView().findViewById(R.id.addFuellingCardView),
                this, new FuelingFragment(),
                String.valueOf(R.layout.fragment_fueling), false);

        defineCardViewListener(getView().findViewById(R.id.addServiceCardView),
                this, new RequiredServicesFragment(),
                String.valueOf(R.layout.fragment_required_services), false);

        defineCardViewListener( getView().findViewById(R.id.addFuellingAndServiceCardView),
                this, new FuelingFragment(),
                String.valueOf(R.layout.fragment_fueling), true);
    }
}
