package gamf.tankolaskonyvelo.ui.dashboard;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.ui.dialog.DatePickerFragment;
import gamf.tankolaskonyvelo.ui.dialog.DateTimeHelper;
import gamf.tankolaskonyvelo.ui.dialog.DialogArgs;
import gamf.tankolaskonyvelo.ui.dialog.DialogFragmentManager;
import gamf.tankolaskonyvelo.ui.dialog.TimePickerFragment;
import gamf.tankolaskonyvelo.ui.recyclerviewcontroller.ListController;
import gamf.tankolaskonyvelo.ui.recyclerviewcontroller.RecyclerViewItem;
import gamf.tankolaskonyvelo.ui.recyclerviewcontroller.RecyclerViewController;
import gamf.tankolaskonyvelo.ui.uihelper.customsnackbar.Snackbars;
import gamf.tankolaskonyvelo.ui.uihelper.keyboardhide.KeyboardHider;

public class RequiredServicesFragment extends Fragment {
    private AutoCompleteTextView serviceNameAutoCompleteTextView;
    private EditText unitPriceEditText;
    private EditText amountEditText;
    private EditText servicesDateEditText;
    private EditText servicesTimeEditText;
    private CheckBox isImportantCheckBox;
    private Spinner servicesVehicleSpinner;

    private ListController listController;
    private List<PurchasedItemEntity> services;
    private int vehicleId;

    private RequiredServicesViewModel requiredServicesViewModel;
    private ArrayAdapter<String> autoCompleteAdapter;
    private Snackbars snackbars = new Snackbars();

    public RequiredServicesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_required_services, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.required_services);
        requiredServicesViewModel = ViewModelProviders.of(this).get(RequiredServicesViewModel.class);
        services = new ArrayList<>();

        initViews();
        initFieldsByArguments();
        initRecyclerViewController();
    }

    private void initViews() {
        serviceNameAutoCompleteTextView = getView().findViewById(R.id.serviceNameAutoCompleteTextView);
        unitPriceEditText = getView().findViewById(R.id.unitPriceEditText);
        amountEditText = getView().findViewById(R.id.amountEditText);
        isImportantCheckBox = getView().findViewById(R.id.isImportantCheckBox);
        servicesDateEditText = getView().findViewById(R.id.servicesDateEditText);
        servicesTimeEditText = getView().findViewById(R.id.servicesTimeEditText);
        servicesVehicleSpinner = getView().findViewById(R.id.servicesVehicleSpinner);

        DialogFragmentManager.definePicker(this,
                DatePickerFragment.getNewInstance(DialogArgs.REQUIRED_SERVICES_FRAGMENT_DATE, this),
                R.id.servicesDateEditText);

        DialogFragmentManager.definePicker(this,
                TimePickerFragment.getNewInstance(DialogArgs.REQUIRED_SERVICES_FRAGMENT_TIME, this),
                R.id.servicesTimeEditText);

        initVehicleSpinner();
        defineButtonListeners();
        initAutoComplete();
        requiredServicesViewModel.initVehicleSpinnerListener(this, servicesVehicleSpinner);
    }

    private void initVehicleSpinner() {
        requiredServicesViewModel.getAllVehicle().observe(this, vehicles -> {
            List<String> vehicleLicensePlateNumbers = new ArrayList<>();
            int vehicleIndex = 0;

            for (int i = 0; i < vehicles.size(); i++) {
                VehicleEntity aVehicle = vehicles.get(i);
                vehicleLicensePlateNumbers.add(aVehicle.getLicensePlateNumber());

                if (aVehicle.getDefaultVehicle()==true) {
                    vehicleIndex = i;
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, vehicleLicensePlateNumbers);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            servicesVehicleSpinner.setAdapter(adapter);
            servicesVehicleSpinner.setSelection(vehicleIndex);
        });
    }
    private void defineButtonListeners() {
        // adding service to RecyclerView
        Button addServiceButton = getView().findViewById(R.id.addServiceButton);
        addServiceButton.setOnClickListener(clickedView -> {
            KeyboardHider.hideKeyboard(this.getActivity());
            if (!areInputsValid()) {
                return;
            }

            int unitPrice = Integer.valueOf(unitPriceEditText.getText().toString());
            int amount = Integer.valueOf(amountEditText.getText().toString());
            int sumOfCosts = amount * unitPrice;

            String serviceName = serviceNameAutoCompleteTextView.getText().toString();
            String dateTime = servicesDateEditText.getText().toString() + " " + servicesTimeEditText.getText().toString();
            String unitPriceString = unitPrice + " " + getString(R.string.currency) + getString(R.string.per_piece);
            String amountString = amount + " " + getString(R.string.amount);
            String sumOfCostsString = sumOfCosts + " " + getString(R.string.currency);

            listController.add(new RecyclerViewItem(serviceName, unitPriceString, amountString, sumOfCostsString, dateTime));
            addItemToServiceList();
            clearViews();
        });

        // clearing every data from the layout
        Button clearServicesButton = getView().findViewById(R.id.clearRecyclerViewButton);
        clearServicesButton.setOnClickListener(clickedView -> {
            KeyboardHider.hideKeyboard(this.getActivity());
            clearRecyclerView();
            snackbars.informationSnackbar(getView(), getString(R.string.added_layout_data_deleted));
        });

        // inserting the content of the RecyclerView
        Button saveServicesButton = getView().findViewById(R.id.saveServicesButton);
        saveServicesButton.setOnClickListener(clickedView -> {
            KeyboardHider.hideKeyboard(this.getActivity());
            if (services.isEmpty()) {
                snackbars.informationSnackbar(getView(), getString(R.string.add_service_for_inserting));
                return;
            }
            requiredServicesViewModel.insertPurchasedItems(services);
            clearRecyclerView();
            snackbars.informationSnackbar(getView(), getString(R.string.successful_service_save));
        });
    }

    private void initRecyclerViewController() {
        RecyclerView recyclerView = getView().findViewById(R.id.servicesRecyclerView);
        listController = new RecyclerViewController(getContext(), recyclerView);
        listController.setRecyclerViewItems(new ArrayList<>());
        listController.show();
    }

    private void initFieldsByArguments() {
        requiredServicesViewModel.setArgBundle(getArguments());
        String date = requiredServicesViewModel.getDateByArgument();
        String time = requiredServicesViewModel.getTimeByArgument();

        if (date == null || date.isEmpty()) {
            servicesDateEditText.setText(DateTimeHelper.getActualDate());
        } else {
            servicesDateEditText.setText(date);
        }

        if (time == null || time.isEmpty()) {
            servicesTimeEditText.setText(DateTimeHelper.getActualTime());
        } else {
            servicesTimeEditText.setText(time);
        }

        vehicleId = requiredServicesViewModel.getDefaultVehicleIdByArgument();
        if (vehicleId == 0) {
            requiredServicesViewModel.getDefaultVehicle().observe(this, defaultVehicle -> {
                vehicleId = defaultVehicle.getId();
            });
        }
    }

    private void initAutoComplete() {
        requiredServicesViewModel.getAllItems().observe(this, items -> {
            List<String> itemNames = new ArrayList<>();
            for (ItemEntity item : items) {
                itemNames.add(item.getItemName());
            }
            autoCompleteAdapter = new ArrayAdapter<>(getActivity().getApplicationContext(),
                    R.layout.autocomplete_list_item, itemNames);
            serviceNameAutoCompleteTextView.setAdapter(autoCompleteAdapter);
        });
    }

    private void addItemToServiceList() {
        String date = String.valueOf(servicesDateEditText.getText().toString());
        String time = String.valueOf(servicesTimeEditText.getText().toString());
        String dateTime = date + " " + time;

        int amount = Integer.valueOf(amountEditText.getText().toString());
        int price = Integer.valueOf(unitPriceEditText.getText().toString());
        String serviceName = serviceNameAutoCompleteTextView.getText().toString();
        boolean isImportant = isImportantCheckBox.isChecked();

        PurchasedItemEntity invalidPurchasedItem = new PurchasedItemEntity(
                amount, dateTime, price, vehicleId, 0);

        requiredServicesViewModel.updatedPurchasedItems(this, serviceName, isImportant,
                services, invalidPurchasedItem, purchasedItems -> services = purchasedItems);
    }


    private boolean areInputsValid() {
        int messageRedId = -1;

        if (FuelingFragment.isEditTextEmpty(serviceNameAutoCompleteTextView)) {
            messageRedId = R.string.service_name_required;
        } else if (FuelingFragment.isEditTextEmpty(unitPriceEditText)) {
            messageRedId = R.string.unitprice_required;
        } else if (FuelingFragment.isEditTextEmpty(amountEditText)) {
            messageRedId = R.string.gas_station_name_required;
        } else if (FuelingFragment.isEditTextEmpty(servicesDateEditText)) {
            messageRedId = R.string.date_required;
        } else if (FuelingFragment.isEditTextEmpty(servicesTimeEditText)) {
            messageRedId = R.string.time_required;
        }

        if (messageRedId != -1) {
            snackbars.errorSnackbar(getView(), getString(messageRedId));
            return false;
        }
        return true;
    }

    private void clearViews() {
        serviceNameAutoCompleteTextView.setText("");
        unitPriceEditText.setText("");
        amountEditText.setText("");
        isImportantCheckBox.setChecked(false);

        servicesDateEditText.setText(DateTimeHelper.getActualDate());
        servicesTimeEditText.setText(DateTimeHelper.getActualTime());
    }

    private void clearRecyclerView() {
        listController.clear();
        services.clear();
        clearViews();
    }
}