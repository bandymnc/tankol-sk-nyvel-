package gamf.tankolaskonyvelo.ui.otherservices;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.dataobjects.PurchasedItemWithVehicle;


public class OtherServicesAdapter extends RecyclerView.Adapter<OtherServicesAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.other_services_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PurchasedItemWithVehicle purchasedItemWithVehicle = purchasedItemWithVehicleEntities.get(position);

        TextView textView1 = holder.listItemNameTextView;
        textView1.setText(purchasedItemWithVehicle.getItemName());

        TextView textView2 = holder.listItemDateTextView;
        textView2.setText(purchasedItemWithVehicle.getPurchaseDate());

        TextView textView3 = holder.listItemPieceTextView;
        textView3.setText(Integer.toString(purchasedItemWithVehicle.getPiece()) + " Db");

        TextView textView4 = holder.listItemPriceTextView;
        textView4.setText(Integer.toString(purchasedItemWithVehicle.getCurrentPrice()) + " Ft");

        TextView textView5=holder.listItemAllPriceTextView;
        textView5.setText(Integer.toString(purchasedItemWithVehicle.getCurrentPrice()*purchasedItemWithVehicle.getPiece())+ " Ft");
    }

    @Override
    public int getItemCount() {
        return purchasedItemWithVehicleEntities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView listItemNameTextView;
        public TextView listItemPieceTextView;
        public TextView listItemPriceTextView;
        public TextView listItemDateTextView;
        public TextView listItemAllPriceTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            listItemNameTextView = itemView.findViewById(R.id.listItemNameTextView);
            listItemDateTextView = itemView.findViewById(R.id.listItemDateTextView);
            listItemPieceTextView = itemView.findViewById(R.id.listItemPieceTextView);
            listItemPriceTextView = itemView.findViewById(R.id.listItemPriceTextView);
            listItemAllPriceTextView=itemView.findViewById(R.id.listItemAllPriceTextView);
        }
    }

    private List<PurchasedItemWithVehicle> purchasedItemWithVehicleEntities;

    public OtherServicesAdapter(List<PurchasedItemWithVehicle> purchasedItemWithVehicleEntities) {
        this.purchasedItemWithVehicleEntities = purchasedItemWithVehicleEntities;
    }

}