package gamf.tankolaskonyvelo.ui.otherservices;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.dataobjects.PurchasedItemWithVehicle;
import gamf.tankolaskonyvelo.entities.VehicleEntity;
import gamf.tankolaskonyvelo.repositories.VehicleRepository;

import gamf.tankolaskonyvelo.repositories.PurchasedItemRepository;

public class OtherServicesViewModel extends AndroidViewModel {

    private PurchasedItemRepository purcasedItemRepository;
    private VehicleRepository vehicleRepository;

    public OtherServicesViewModel(@NonNull Application application) {
        super(application);

        purcasedItemRepository = new PurchasedItemRepository(application, new AppExecutors());
        vehicleRepository = new VehicleRepository(application, new AppExecutors());
    }

    public LiveData<List<VehicleEntity>> getAllVehicle() {
        return vehicleRepository.getAllVehicle();
    }

    public LiveData<List<PurchasedItemWithVehicle>> getAllPurchasedItemByVehicle(String licensePlateNumber) {
        return purcasedItemRepository.getAllPurchasedItemByVehicle(licensePlateNumber);
    }

    public LiveData<List<PurchasedItemWithVehicle>> getAllImportantPurchasedItemByVehicle(String licensePlateNumber) {
        return purcasedItemRepository.getAllImportantPurchasedItemByVehicle(licensePlateNumber);
    }

}
