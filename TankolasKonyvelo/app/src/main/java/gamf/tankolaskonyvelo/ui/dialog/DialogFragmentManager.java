package gamf.tankolaskonyvelo.ui.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.View;

import java.util.Locale;

public class DialogFragmentManager {

    public static final String LAYOUT_TAG_KEY = "layoutTag";
    public static final String RES_ID_KEY = "resId";
    public static final String DIALOG = "dialog";

    // Removes the given DialogFragment from Fragment context
    public static View removeDialogFragment(DialogFragment dialogFragment) {
        Bundle argsBundle = dialogFragment.getArguments();

        Fragment fragment = dialogFragment.getActivity().getSupportFragmentManager()
                .findFragmentByTag(argsBundle.getString(LAYOUT_TAG_KEY));

        View view = fragment.getView().findViewById(argsBundle.getInt(RES_ID_KEY));
        view.setEnabled(true);

        dialogFragment.dismiss();
        return view;
    }

    public static void definePicker(final Fragment fragment, final DialogFragment dialogFragment, int resId) {
        if (dialogFragment != null) {
            final View view = fragment.getView().findViewById(resId);

            view.setOnClickListener(view1 -> {
                view1.setEnabled(false);
                dialogFragment.show(fragment.getActivity().getSupportFragmentManager(), DIALOG);
            });
        }
    }
}
