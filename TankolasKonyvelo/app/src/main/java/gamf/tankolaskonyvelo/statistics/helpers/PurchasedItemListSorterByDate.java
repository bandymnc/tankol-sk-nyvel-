package gamf.tankolaskonyvelo.statistics.helpers;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;

public class PurchasedItemListSorterByDate {
    public List<PurchasedItemEntity> sortPurchasedItemsByDate(List<PurchasedItemEntity> purchasedItems) {
        List<PurchasedItemEntity> sortedPurchasedItems = purchasedItems;

        Collections.sort(sortedPurchasedItems, (purchasedItemEntity1, purchasedItemEntity2) -> {
            Timestamp timestamp1 = convertStringToTimestamp(purchasedItemEntity1.getPurchaseDate());
            Timestamp timestamp2 = convertStringToTimestamp(purchasedItemEntity2.getPurchaseDate());

            return timestamp1.compareTo(timestamp2);
        });

        return sortedPurchasedItems;
    }

    private Timestamp convertStringToTimestamp(String date) {
        StringToTimestampConverter stringToTimestampConverter = new StringToTimestampConverter();

        return stringToTimestampConverter.convertStringToTimestamp(date);
    }
}
