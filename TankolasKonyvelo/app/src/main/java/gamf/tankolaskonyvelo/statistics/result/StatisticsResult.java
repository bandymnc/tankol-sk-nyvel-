package gamf.tankolaskonyvelo.statistics.result;

import java.util.List;

public interface StatisticsResult {
    List<String> getStatisticsNames();

    List<Double> getStatisticsValues();

    String getSingleStatisticsName();

    Double getSingleStatisticsValue();
}
