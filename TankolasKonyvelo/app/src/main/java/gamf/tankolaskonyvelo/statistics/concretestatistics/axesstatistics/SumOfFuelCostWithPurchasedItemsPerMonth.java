package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelingListSplitterByMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels.MonthFuelings;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class SumOfFuelCostWithPurchasedItemsPerMonth implements StatisticsCalculator {
    List<FuelingEntity> fuelings;
    List<PurchasedItemEntity> purchasedItems;
    AxesStatisticsResult statisticsResults;
    List<MonthFuelings> fuelingsGroupByMonth;

    public SumOfFuelCostWithPurchasedItemsPerMonth(List<FuelingEntity> fuelings, List<PurchasedItemEntity> purchasedItems) {
        this.fuelings = fuelings;
        this.purchasedItems = purchasedItems;

        FuelingListSplitterByMonth fuelingListSplitterByMonth = new FuelingListSplitterByMonth(fuelings);
        fuelingsGroupByMonth = fuelingListSplitterByMonth.getFuelingsGroupByMonth();
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> resultList = new ArrayList<>();
        List<Double> valueAggragation = new ArrayList<>();

        for (MonthFuelings aMonthlyFueling : fuelingsGroupByMonth) {
            Double monthlySum = aMonthlyFueling.getSumOfFuelingCostInMonth();
            monthlySum += getSumOfPurchasedItemsCostByMonth(aMonthlyFueling.getMonthIndex());

            SingleStatisticsResult aResult = new SingleStatisticsResult(aMonthlyFueling.getMonthlyDate(), monthlySum);
            valueAggragation.add(monthlySum);

            resultList.add(aResult);
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResults = new AxesStatisticsResult(resultList, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_month), StringResourceGetter.getResource(R.string.axes_name_sum_of_fuel_cost_with_purchased_items_per_month));

        return statisticsResults;
    }

    private Double getSumOfPurchasedItemsCostByMonth(int monthIndex) {
        Double sum = 0.0d;

        List<PurchasedItemEntity> purchasedItemsByMonth = getPurchasedItemsByMonth(monthIndex);
        for (PurchasedItemEntity aPurchasedItemEntity : purchasedItemsByMonth) {
            sum += aPurchasedItemEntity.getCurrentPrice() * aPurchasedItemEntity.getPiece();
        }

        return sum;
    }

    private List<PurchasedItemEntity> getPurchasedItemsByMonth(int monthIndex) {
        List<PurchasedItemEntity> result = new ArrayList<>();

        for (PurchasedItemEntity aPurchasedItemEntity : purchasedItems) {
            String purchasedDate = aPurchasedItemEntity.getPurchaseDate();
            int purchasedDateIndex = getMonthFromStringDate(purchasedDate);

            if (monthIndex == purchasedDateIndex) {
                result.add(aPurchasedItemEntity);
            }
        }

        return result;
    }

    private int getMonthFromStringDate(String date) {
        String[] dateParts = date.split("-");

        return Integer.parseInt(dateParts[1]);
    }
}
