package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class AllUsedFuelQuantitySinceAllTime implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private SingleStatisticsResult statisticsResult;

    public AllUsedFuelQuantitySinceAllTime(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        Double fuelQuantity = 0.0d;
        for (int i = 0; i < fuelings.size() - 1; i++) {
            fuelQuantity += (fuelings.get(i).getFuelMeterState() + fuelings.get(i).getFuelQuantity()) - fuelings.get(i + 1).getFuelMeterState();
        }

        statisticsResult = new SingleStatisticsResult("all used fuel since all time", fuelQuantity);

        return statisticsResult;
    }
}
