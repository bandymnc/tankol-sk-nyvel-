package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class AverageConsumptionSinceAllTime implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private SingleStatisticsResult statisticsResult;

    public AverageConsumptionSinceAllTime(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        double wornOutFuel = 0;
        double travelledRoad = 0;

        if (fuelings.size() > 1) {
            for (int i = 0; i < fuelings.size() - 1; i++) {
                for (int j = 1; j < fuelings.size(); j++) {
                    wornOutFuel += fuelings.get(i).getFuelMeterState() + fuelings.get(i).getFuelQuantity() - fuelings.get(j).getFuelMeterState();
                    travelledRoad += (fuelings.get(j).getKmMeterState() - fuelings.get(i).getKmMeterState());
                }
            }
        }

        Double averageConsumption = wornOutFuel / (travelledRoad / 100);
        statisticsResult = new SingleStatisticsResult("average consumption since all time", (double) Math.round(averageConsumption));

        return statisticsResult;
    }
}
