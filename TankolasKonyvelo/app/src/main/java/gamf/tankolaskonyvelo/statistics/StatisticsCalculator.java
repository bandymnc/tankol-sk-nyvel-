package gamf.tankolaskonyvelo.statistics;

import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public interface StatisticsCalculator {
    StatisticsResult calculateStatisics();
}
