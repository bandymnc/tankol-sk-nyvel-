package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels.MonthFuelings;
import gamf.tankolaskonyvelo.statistics.helpers.FuelingListSorterByDate;

public class FuelingListSplitterByMonth {
    private List<FuelingEntity> fuelings;

    public FuelingListSplitterByMonth(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    public List<MonthFuelings> getFuelingsGroupByMonth() {
        List<MonthFuelings> statisticsGroupByMonth = new ArrayList<>();

        List<FuelingEntity> sortedFuelings = sortFuelingByDate(fuelings);
        int currentMonthIndex = 0;
        MonthFuelings dataListOfCurrentMonth = new MonthFuelings(currentMonthIndex);
        for (int i = 0; i < sortedFuelings.size(); i++) {
            FuelingEntity aSortedFueling = sortedFuelings.get(i);
            int aMonth = getMonthOfSingleFueling(aSortedFueling);

            boolean isFirstElement = i == 0;
            boolean isLastElement = i == sortedFuelings.size() - 1;
            boolean isNewMonth = currentMonthIndex != aMonth;
            if (isFirstElement) {
                currentMonthIndex = aMonth;
                dataListOfCurrentMonth.setMonthIndex(currentMonthIndex);
            } else if (isNewMonth || isLastElement) {
                statisticsGroupByMonth.add(dataListOfCurrentMonth);

                currentMonthIndex = aMonth;
                dataListOfCurrentMonth = new MonthFuelings(currentMonthIndex);
            }

            dataListOfCurrentMonth.addFuelingToMonth(aSortedFueling);
        }

        return statisticsGroupByMonth;
    }

    private List<FuelingEntity> sortFuelingByDate(List<FuelingEntity> fuelings) {
        FuelingListSorterByDate fuelingListSorterByDate = new FuelingListSorterByDate();
        return fuelingListSorterByDate.sortFuelingsByDate(fuelings);
    }

    private int getMonthOfSingleFueling(FuelingEntity fuelingEntity) {
        Calendar aCalendar = new StringDateToCalendarConverter().getCalendarFromStringDate(fuelingEntity.getFuelingDate());
        return aCalendar.get(Calendar.MONTH);
    }
}
