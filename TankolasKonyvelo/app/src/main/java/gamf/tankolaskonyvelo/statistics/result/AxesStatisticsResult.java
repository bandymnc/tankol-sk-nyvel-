package gamf.tankolaskonyvelo.statistics.result;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class AxesStatisticsResult implements StatisticsResult, List<SingleStatisticsResult> {
    private List<SingleStatisticsResult> statisticsResults;
    private Double upperExtremeValue;
    private Double lowerExtremeValue;
    private String xAxesName;
    private String yAxesName;

    public AxesStatisticsResult(List<SingleStatisticsResult> statisticsResults, Double upperExtremeValue, Double lowerExtremeValue, String xAxesName, String yAxesName) {
        this.statisticsResults = statisticsResults;
        this.upperExtremeValue = upperExtremeValue;
        this.lowerExtremeValue = lowerExtremeValue;
        this.xAxesName = xAxesName;
        this.yAxesName = yAxesName;
    }

    @Override
    public List<String> getStatisticsNames() {
        List<String> statisticsNames = new ArrayList<>();

        for (SingleStatisticsResult aStatisticsResult : statisticsResults) {
            statisticsNames.add(aStatisticsResult.getSingleStatisticsName());
        }

        return statisticsNames;
    }

    @Override
    public List<Double> getStatisticsValues() {
        List<Double> statisticsValues = new ArrayList<>();

        for (SingleStatisticsResult aStatisticsResult : statisticsResults) {
            statisticsValues.add(aStatisticsResult.getSingleStatisticsValue());
        }

        return statisticsValues;
    }

    @Override
    public String getSingleStatisticsName() {
        return statisticsResults.get(0).getSingleStatisticsName();
    }

    @Override
    public Double getSingleStatisticsValue() {
        return statisticsResults.get(0).getSingleStatisticsValue();
    }

    @Override
    public int size() {
        return statisticsResults.size();
    }

    @Override
    public boolean isEmpty() {
        return statisticsResults.isEmpty();
    }

    @Override
    public boolean contains(Object singleStatisticsResult) {
        return statisticsResults.contains(singleStatisticsResult);
    }

    @Override
    public Iterator<SingleStatisticsResult> iterator() {
        return statisticsResults.iterator();
    }

    @Override
    public Object[] toArray() {
        return statisticsResults.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return statisticsResults.toArray(ts);
    }

    @Override
    public boolean add(SingleStatisticsResult singleStatisticsResult) {
        return statisticsResults.add(singleStatisticsResult);
    }

    @Override
    public boolean remove(Object o) {
        return statisticsResults.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return statisticsResults.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends SingleStatisticsResult> collection) {
        return statisticsResults.addAll(collection);
    }

    @Override
    public boolean addAll(int i, @NonNull Collection<? extends SingleStatisticsResult> collection) {
        return statisticsResults.addAll(i, collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return statisticsResults.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return retainAll(collection);
    }

    @Override
    public void clear() {
        statisticsResults.clear();
    }

    @Override
    public SingleStatisticsResult get(int i) {
        return statisticsResults.get(i);
    }

    @Override
    public SingleStatisticsResult set(int i, SingleStatisticsResult singleStatisticsResult) {
        return statisticsResults.set(i, singleStatisticsResult);
    }

    @Override
    public void add(int i, SingleStatisticsResult singleStatisticsResult) {
        statisticsResults.add(i, singleStatisticsResult);
    }

    @Override
    public SingleStatisticsResult remove(int i) {
        return statisticsResults.remove(i);
    }

    @Override
    public int indexOf(Object o) {
        return statisticsResults.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return statisticsResults.lastIndexOf(o);
    }

    @NonNull
    @Override
    public ListIterator<SingleStatisticsResult> listIterator() {
        return null;
    }

    @NonNull
    @Override
    public ListIterator<SingleStatisticsResult> listIterator(int i) {
        return statisticsResults.listIterator(i);
    }

    @NonNull
    @Override
    public List<SingleStatisticsResult> subList(int i, int i1) {
        return statisticsResults.subList(i, i1);
    }

    public Double getUpperExtremeValue() {
        return upperExtremeValue;
    }

    public void setUpperExtremeValue(Double upperExtremeValue) {
        this.upperExtremeValue = upperExtremeValue;
    }

    public Double getLowerExtremeValue() {
        return lowerExtremeValue;
    }

    public void setLowerExtremeValue(Double lowerExtremeValue) {
        this.lowerExtremeValue = lowerExtremeValue;
    }
    public String[] getStatisticsNameAsTmb(){
        List<String> getStatisticsNames=getStatisticsNames();
        return getStatisticsNames.toArray(new String[0]);
    }
    public Float getUpperExtremeFloatValue() {
        Float value = (float) ((int) (getUpperExtremeValue().floatValue() * 10f)) / 10f;

        return value;
    }

    public Float getLowerExtremeFloatValue() {
        Float value = (float) ((int) (getLowerExtremeValue().floatValue() * 10f)) / 10f;

        return value;
    }

    public String getxAxesName() {
        return xAxesName;
    }

    public void setxAxesName(String xAxesName) {
        this.xAxesName = xAxesName;
    }

    public String getyAxesName() {
        return yAxesName;
    }

    public void setyAxesName(String yAxesName) {
        this.yAxesName = yAxesName;
    }

    public String getAxesFullName() {
        return xAxesName + "/" + yAxesName;
    }
}
