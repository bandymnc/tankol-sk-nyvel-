package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;

public class MonthFuelings {
    private List<FuelingEntity> fuelingsInMonth;
    private int monthIndex;

    public MonthFuelings(int monthIndex) {
        fuelingsInMonth = new ArrayList<>();
        this.monthIndex = monthIndex;
    }

    public void addFuelingToMonth(FuelingEntity fuelingInMonth) {
        fuelingsInMonth.add(fuelingInMonth);
    }

    public int size() {
        return fuelingsInMonth.size();
    }

    public FuelingEntity get(int index) {
        return fuelingsInMonth.get(index);
    }

    public String getNameByIndex(int index) {
        return fuelingsInMonth.get(index).getFuelingDate();
    }

    public String getMonthlyDate() {
        return fuelingsInMonth.get(0).getFuelingDate();
    }

    public String getMonthlyName() {
        String[] dateParts = fuelingsInMonth.get(0).getFuelingDate().split("-");

        return dateParts[0] + "-" + dateParts[1];
    }

    public List<FuelingEntity> getFuelingsInMonth() {
        return fuelingsInMonth;
    }

    public void setFuelingsInMonth(List<FuelingEntity> fuelingsInMonth) {
        this.fuelingsInMonth = fuelingsInMonth;
    }

    public int getMonthIndex() {
        return monthIndex;
    }

    public void setMonthIndex(int monthIndex) {
        this.monthIndex = monthIndex;
    }

    public Double getSumOfFuelingCostInMonth() {
        Double sum = 0.0d;

        for (FuelingEntity aFueling : fuelingsInMonth) {
            sum += aFueling.getUnitPriceOfFuel() * aFueling.getFuelQuantity();
        }

        return sum;
    }

    public Double getSumOfPassedKmInMonth() {
        Double sum = 0.0d;

        for (int i = 0, j = 1; j < fuelingsInMonth.size(); i++, j++) {
            FuelingEntity fueling1 = fuelingsInMonth.get(i);
            FuelingEntity fueling2 = fuelingsInMonth.get(j);

            sum += fueling2.getKmMeterState() - fueling1.getKmMeterState();
        }

        return sum;
    }

    public Double getSumOfConsumedFuelInMonth() {
        Double sum = 0.0d;

        for (FuelingEntity aFueling : fuelingsInMonth) {
            sum += aFueling.getFuelQuantity();
        }

        return sum;
    }
}
