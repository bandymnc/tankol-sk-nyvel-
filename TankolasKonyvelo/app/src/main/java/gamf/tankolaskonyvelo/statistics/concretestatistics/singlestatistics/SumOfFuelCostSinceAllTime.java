package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelTypeConstants;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class SumOfFuelCostSinceAllTime implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private SingleStatisticsResult statisticsResult;
    private FuelTypeConstants fuelTypeConstants;

    public SumOfFuelCostSinceAllTime(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
        this.fuelTypeConstants = FuelTypeConstants.getInstance();
    }

    @Override
    public StatisticsResult calculateStatisics() {
        Double sumOfFuelCost = 0.0d;
        for (FuelingEntity aFueling : fuelings) {
            FuelTypeEntity fuelType = fuelTypeConstants.getFuelTypeEntityByFuelTypeId(aFueling.getFuelTypeId());

            if (fuelType.getUnit().equalsIgnoreCase("kw")) {
                sumOfFuelCost += aFueling.getUnitPriceOfFuel();
            } else {
                sumOfFuelCost += aFueling.getFuelQuantity() * aFueling.getUnitPriceOfFuel();
            }
        }

        statisticsResult = new SingleStatisticsResult("összes üzemanyag költsége", sumOfFuelCost);

        return statisticsResult;
    }
}
