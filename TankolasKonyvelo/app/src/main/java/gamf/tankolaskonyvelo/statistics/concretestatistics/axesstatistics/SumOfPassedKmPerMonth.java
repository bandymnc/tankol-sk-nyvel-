package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelingListSplitterByMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels.MonthFuelings;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class SumOfPassedKmPerMonth implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private final List<MonthFuelings> fuelingsGroupByMonth;
    private AxesStatisticsResult statisticsResults;

    public SumOfPassedKmPerMonth(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;

        FuelingListSplitterByMonth fuelingListSplitterByMonth = new FuelingListSplitterByMonth(fuelings);
        fuelingsGroupByMonth = fuelingListSplitterByMonth.getFuelingsGroupByMonth();
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> resultList = new ArrayList<>();
        List<Double> valueAggragation = new ArrayList<>();

        for (MonthFuelings aFuelingInMonth : fuelingsGroupByMonth) {
            Double passedKmInMonth = aFuelingInMonth.getSumOfPassedKmInMonth();

            resultList.add(new SingleStatisticsResult(aFuelingInMonth.getMonthlyName(), passedKmInMonth));
            valueAggragation.add(passedKmInMonth);
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResults = new AxesStatisticsResult(resultList, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_month), StringResourceGetter.getResource(R.string.axes_name_sum_of_passed_km_per_month));

        return statisticsResults;
    }
}
