package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class OneFuelingAvgTraveledRouteSinceAllTime implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private SingleStatisticsResult statisticsResult;

    public OneFuelingAvgTraveledRouteSinceAllTime(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        int fuelingCount = 0;
        Double travelledRoadSinceAllTime = 0.0d;
        for (int i = 0, j = 1; j < fuelings.size(); i++, j++, fuelingCount++) {
            int penultimateFuelingKmState = fuelings.get(i).getKmMeterState();
            int lastFuelingKmState = fuelings.get(j).getKmMeterState();

            Double trvaledderRoadBetweenCurrentLastTwoFuelings = new Double(lastFuelingKmState - penultimateFuelingKmState);
            travelledRoadSinceAllTime += trvaledderRoadBetweenCurrentLastTwoFuelings;
        }

        Double averageOfTravelledRoadBetweenTwoFueling = travelledRoadSinceAllTime / fuelingCount;
        statisticsResult = new SingleStatisticsResult("one fueling average traveled route since all time", averageOfTravelledRoadBetweenTwoFueling);

        return statisticsResult;
    }
}
