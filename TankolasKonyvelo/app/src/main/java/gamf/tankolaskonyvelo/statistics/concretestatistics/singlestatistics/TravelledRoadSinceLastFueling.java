package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.helpers.FuelingListSorterByDate;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class TravelledRoadSinceLastFueling implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private SingleStatisticsResult statisticsResult;


    public TravelledRoadSinceLastFueling(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<FuelingEntity> sortedfuelings = sortFuelingsByDate(fuelings);

        int indexOfLastFueling = sortedfuelings.size() - 1;
        int indexOfPenultimateFueling = sortedfuelings.size() - 2;
        int lastKmMeterState = sortedfuelings.get(indexOfLastFueling).getKmMeterState();
        int penultimateKmMeterState = sortedfuelings.get(indexOfPenultimateFueling).getKmMeterState();

        Double travelledRoadSinceLastFueling = new Double(lastKmMeterState - penultimateKmMeterState);
        statisticsResult = new SingleStatisticsResult("travelled road since last fueling", travelledRoadSinceLastFueling);

        return statisticsResult;
    }

    private List<FuelingEntity> sortFuelingsByDate(List<FuelingEntity> fuelings) {
        FuelingListSorterByDate fuelingListSorterByDate = new FuelingListSorterByDate();
        List<FuelingEntity> sortedFuelings = fuelingListSorterByDate.sortFuelingsByDate(fuelings);

        return sortedFuelings;
    }
}
