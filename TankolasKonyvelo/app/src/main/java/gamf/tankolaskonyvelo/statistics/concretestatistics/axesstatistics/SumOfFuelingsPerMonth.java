package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelingListSplitterByMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels.MonthFuelings;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class SumOfFuelingsPerMonth implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private AxesStatisticsResult statisticsResult;

    public SumOfFuelingsPerMonth(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> result = new ArrayList<>();
        FuelingListSplitterByMonth fuelingListSplitterByMonth = new FuelingListSplitterByMonth(fuelings);
        List<MonthFuelings> fuelingsGroupByMonth = fuelingListSplitterByMonth.getFuelingsGroupByMonth();
        List<Double> valueAggragation = new ArrayList<>();

        for (MonthFuelings fuelingsInAMonth : fuelingsGroupByMonth) {
            Double countOfFuelingsInMonth = new Double(fuelingsInAMonth.size());
            String[] data = fuelingsInAMonth.getMonthlyDate().split("-");
            result.add(new SingleStatisticsResult(data[0] + "-" + data[1], countOfFuelingsInMonth));
            valueAggragation.add(countOfFuelingsInMonth);
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResult = new AxesStatisticsResult(result, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_month), StringResourceGetter.getResource(R.string.axes_name_sum_of_fuelings_per_month));

        return statisticsResult;
    }
}
