package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class CountOfFuelingsSinceAllTime implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private SingleStatisticsResult statisticsResult;

    public CountOfFuelingsSinceAllTime(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        Double countOfFuelings = new Double(fuelings.size());
        statisticsResult = new SingleStatisticsResult("tankolások száma", countOfFuelings);

        return statisticsResult;
    }
}
