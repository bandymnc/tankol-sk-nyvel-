package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers;

import android.content.Context;
import android.content.res.Resources;

public class StringResourceGetter {
    private static Resources resources;

    public static void init(Context context) {
        resources = context.getResources();
    }

    public static String getResource(int resourceId) {
        return resources.getString(resourceId);
    }
}
