package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;

import java.util.HashMap;
import java.util.Map;

import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.repositories.FuelTypeRepository;

public class FuelTypeConstants {

    private static FuelTypeConstants instance = null;
    private Map<Integer, FuelTypeEntity> fuelTypesById = null;
    private Map<String, FuelTypeEntity> fuelTypesByName = null;

    private FuelTypeRepository fuelTypeRepository;

    private FuelTypeConstants() {
        fuelTypesById = new HashMap<>();
        fuelTypesByName = new HashMap<>();
    }

    public static FuelTypeConstants getInstance() {
        if (instance == null) {
            instance = new FuelTypeConstants();
        }

        return instance;
    }

    public void Initialize(LifecycleOwner lifecycleOwner, Application application, AppExecutors appExecutors) {
        fuelTypeRepository = new FuelTypeRepository(application, appExecutors);

        fuelTypeRepository.getAllFuelTypesOrderByName().observe(lifecycleOwner, fuelTypeEntities -> {
            for (FuelTypeEntity fuelTypeEntity : fuelTypeEntities) {
                fuelTypesById.put(fuelTypeEntity.getId(), fuelTypeEntity);
                fuelTypesByName.put(fuelTypeEntity.getName(), fuelTypeEntity);
            }
        });
    }

    public String getFuelTypeNameById(int id) {
        return fuelTypesById.get(id).getName();
    }

    public int getFuelTypeIdByName(String name) {
        return fuelTypesByName.get(name).getId();
    }

    public FuelTypeEntity getFuelTypeEntityByFuelTypeId(int id) {
        return fuelTypesById.get(id);
    }
}
