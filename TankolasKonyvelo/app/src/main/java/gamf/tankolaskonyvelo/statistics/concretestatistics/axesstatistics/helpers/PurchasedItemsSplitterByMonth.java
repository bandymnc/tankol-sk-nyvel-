package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels.MonthPurchasedItems;
import gamf.tankolaskonyvelo.statistics.helpers.PurchasedItemListSorterByDate;

public class PurchasedItemsSplitterByMonth {
    private List<PurchasedItemEntity> purchasedItems;

    public PurchasedItemsSplitterByMonth(List<PurchasedItemEntity> purchasedItems) {
        this.purchasedItems = purchasedItems;
    }

    public List<MonthPurchasedItems> getFuelingsGroupByMonth() {
        List<MonthPurchasedItems> statisticsGroupByMonth = new ArrayList<>();

        List<PurchasedItemEntity> sortedPurchasedItems = sortFuelingByDate(purchasedItems);
        int currentMonthIndex = 0;
        MonthPurchasedItems dataListOfCurrentMonth = new MonthPurchasedItems(currentMonthIndex);
        for (int i = 0; i < sortedPurchasedItems.size(); i++) {
            PurchasedItemEntity aSortedFueling = sortedPurchasedItems.get(i);
            int aMonth = getMonthOfSingleFueling(aSortedFueling);

            boolean isFirstElement = i == 0;
            boolean isLastElement = i == sortedPurchasedItems.size() - 1;
            boolean isNewMonth = currentMonthIndex != aMonth;
            if (isFirstElement) {
                currentMonthIndex = aMonth;
                dataListOfCurrentMonth.setMonthIndex(currentMonthIndex);
            } else if (isNewMonth || isLastElement) {
                statisticsGroupByMonth.add(dataListOfCurrentMonth);

                currentMonthIndex = aMonth;
                dataListOfCurrentMonth = new MonthPurchasedItems(currentMonthIndex);
            }

            dataListOfCurrentMonth.addPurchasedItemToMonth(aSortedFueling);
        }

        return statisticsGroupByMonth;
    }

    private List<PurchasedItemEntity> sortFuelingByDate(List<PurchasedItemEntity> fuelings) {
        PurchasedItemListSorterByDate fuelingListSorterByDate = new PurchasedItemListSorterByDate();
        return fuelingListSorterByDate.sortPurchasedItemsByDate(fuelings);
    }

    private int getMonthOfSingleFueling(PurchasedItemEntity purchasedItemEntity) {
        Calendar aCalendar = new StringDateToCalendarConverter().getCalendarFromStringDate(purchasedItemEntity.getPurchaseDate());
        return aCalendar.get(Calendar.MONTH);
    }
}
