package gamf.tankolaskonyvelo.statistics.helpers;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class StringToTimestampConverter {
    public Timestamp convertStringToTimestamp(String date) {
        Timestamp timestamp = null;

        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            timestamp = new Timestamp((dateFormat.parse(date)).getTime());
        } catch (ParseException e) {
        }

        return timestamp;
    }
}
