package gamf.tankolaskonyvelo.statistics.helpers;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;

public class FuelingListSorterByDate {
    public List<FuelingEntity> sortFuelingsByDate(List<FuelingEntity> fuelings) {
        List<FuelingEntity> sortedFuelings = fuelings;

        Collections.sort(sortedFuelings, (fuelingEntity1, fuelingEntity2) -> {
            Timestamp timestamp1 = convertStringToTimestamp(fuelingEntity1.getFuelingDate());
            Timestamp timestamp2 = convertStringToTimestamp(fuelingEntity2.getFuelingDate());

            return timestamp1.compareTo(timestamp2);
        });

        return sortedFuelings;
    }

    private Timestamp convertStringToTimestamp(String date) {
        StringToTimestampConverter stringToTimestampConverter = new StringToTimestampConverter();

        return stringToTimestampConverter.convertStringToTimestamp(date);
    }
}
