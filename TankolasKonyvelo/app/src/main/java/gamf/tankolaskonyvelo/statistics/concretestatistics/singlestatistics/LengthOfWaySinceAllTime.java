package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.helpers.FuelingListSorterByDate;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class LengthOfWaySinceAllTime implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private SingleStatisticsResult statisticsResult;

    public LengthOfWaySinceAllTime(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<FuelingEntity> sortedfuelings = sortFuelingsByDate(fuelings);

        int lastFuelingIndex = sortedfuelings.size() - 1;
        int firstFuelingIndex = 0;
        int lastKmMeterState = sortedfuelings.get(lastFuelingIndex).getKmMeterState();
        int firstKmMeterState = sortedfuelings.get(firstFuelingIndex).getKmMeterState();
        Double lengthOfWaySinceAllTime = new Double(lastKmMeterState - firstKmMeterState);

        statisticsResult = new SingleStatisticsResult("Length of way since all time", lengthOfWaySinceAllTime);

        return statisticsResult;
    }

    private List<FuelingEntity> sortFuelingsByDate(List<FuelingEntity> fuelings) {
        FuelingListSorterByDate fuelingListSorterByDate = new FuelingListSorterByDate();

        List<FuelingEntity> sortedFuelings = fuelingListSorterByDate.sortFuelingsByDate(fuelings);

        return sortedFuelings;
    }
}
