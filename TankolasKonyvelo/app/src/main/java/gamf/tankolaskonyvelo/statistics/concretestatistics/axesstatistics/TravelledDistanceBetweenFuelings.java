package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class TravelledDistanceBetweenFuelings implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private AxesStatisticsResult statisticsResult;

    public TravelledDistanceBetweenFuelings(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> result = new ArrayList<>();
        List<Double> valueAggragation = new ArrayList<>();
        Double upperExtremeValue;
        Double lowerExtremeValue;

        for (int i = 0, j = 1; j < fuelings.size(); i++, j++) {
            String statisticsName = fuelings.get(i).getFuelingDate() + " - " + fuelings.get(j).getFuelingDate();
            Double statisticsValue = (double) (fuelings.get(j).getKmMeterState() - fuelings.get(i).getKmMeterState());

            valueAggragation.add(statisticsValue);
            SingleStatisticsResult newSingleStatistics = new SingleStatisticsResult(statisticsName, statisticsValue);
            result.add(newSingleStatistics);
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResult = new AxesStatisticsResult(result, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_time), StringResourceGetter.getResource(R.string.axes_name_travelled_distance_between_fuelings));

        return statisticsResult;
    }
}
