package gamf.tankolaskonyvelo.statistics.helpers;

import java.util.List;

import gamf.tankolaskonyvelo.entities.ItemEntity;

public class ItemListFilter {

    private int countOfShownItems;
    private List<ItemEntity> items;
    private List<Double> itemOccurrences;

    public ItemListFilter(int countOfShownItems, List<ItemEntity> items, List<Double> itemOccurrences) {
        this.countOfShownItems = countOfShownItems;
        this.items = items;
        this.itemOccurrences = itemOccurrences;
    }

    public void filterForCountOfShownItems() {
        for (int i = 0; i < items.size(); ) {
            boolean hasToShow = (i + 1) < countOfShownItems;
            if (hasToShow) {
                i++;
                continue;
            }
            items.remove(i);
            itemOccurrences.remove(i);
        }
    }

    public List<ItemEntity> getFilteredItems() {
        return items;
    }

    public List<Double> getFilteredItemOccurrences() {
        return itemOccurrences;
    }
}
