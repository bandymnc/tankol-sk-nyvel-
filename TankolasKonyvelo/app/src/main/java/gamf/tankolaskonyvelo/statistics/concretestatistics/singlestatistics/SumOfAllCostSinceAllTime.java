package gamf.tankolaskonyvelo.statistics.concretestatistics.singlestatistics;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelTypeConstants;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class SumOfAllCostSinceAllTime implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private List<PurchasedItemEntity> purchasedItems;
    private SingleStatisticsResult statisticsResult;
    private FuelTypeConstants fuelTypeConstants;

    public SumOfAllCostSinceAllTime(List<FuelingEntity> fuelings, List<PurchasedItemEntity> purchasedItems) {
        this.fuelings = fuelings;
        this.purchasedItems = purchasedItems;
        this.fuelTypeConstants = FuelTypeConstants.getInstance();
    }

    @Override
    public StatisticsResult calculateStatisics() {
        Double allFuelingCost = 0.0d;
        Double allPurchasedItemCost = 0.0d;

        for (FuelingEntity aFueling : fuelings) {
            FuelTypeEntity fuelType = fuelTypeConstants.getFuelTypeEntityByFuelTypeId(aFueling.getFuelTypeId());
            if (fuelType.getUnit().equalsIgnoreCase("kw")) {
                allFuelingCost += aFueling.getUnitPriceOfFuel();
            } else {
                allFuelingCost += aFueling.getFuelQuantity() * aFueling.getUnitPriceOfFuel();
            }
        }

        for (PurchasedItemEntity aPurchasedItem : purchasedItems) {
            allPurchasedItemCost += aPurchasedItem.getCurrentPrice() * aPurchasedItem.getPiece();
        }

        statisticsResult = new SingleStatisticsResult("sum of all cost since all time", allFuelingCost + allPurchasedItemCost);

        return statisticsResult;
    }
}
