package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.dataobjects.FuelingWithFuelType;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class FuelTypesRatioToAllVehicle implements StatisticsCalculator {
    List<FuelingWithFuelType> fuelingsWithFuelTypes;
    AxesStatisticsResult statisticsResults;

    public FuelTypesRatioToAllVehicle(List<FuelingWithFuelType> fuelingsWithFuelTypes) {
        this.fuelingsWithFuelTypes = fuelingsWithFuelTypes;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> resultList = new ArrayList<>();
        List<Double> valueAggragation = new ArrayList<>();

        // grouping by fuel type
        List<List<FuelingWithFuelType>> fuelingsWithFuelTypesGroupByFuelTypes = new ArrayList<>();
        List<String> fuelTypes = new ArrayList<>();
        for (FuelingWithFuelType aFuelingWithFuelType : fuelingsWithFuelTypes) {
            if (!fuelTypes.contains(aFuelingWithFuelType.getFuelName())) {
                fuelTypes.add(aFuelingWithFuelType.getFuelName());
                fuelingsWithFuelTypesGroupByFuelTypes.add(new ArrayList<>());
            }

            fuelingsWithFuelTypesGroupByFuelTypes.get(fuelTypes.indexOf(aFuelingWithFuelType.getFuelName())).add(aFuelingWithFuelType);
        }

        // calculate ration
        for (int i = 0; i < fuelingsWithFuelTypesGroupByFuelTypes.size(); i++) {
            List<FuelingWithFuelType> aFuelingsWithFuelTypesGroupByFuelTypes = fuelingsWithFuelTypesGroupByFuelTypes.get(i);

            Double sumOfConsumedFuelByCertainFuelType = 0.0d;
            for(FuelingWithFuelType aGroupMember : aFuelingsWithFuelTypesGroupByFuelTypes) {
                sumOfConsumedFuelByCertainFuelType += aGroupMember.getFuelQuantity();
            }

            resultList.add(new SingleStatisticsResult(fuelTypes.get(i), sumOfConsumedFuelByCertainFuelType));
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResults = new AxesStatisticsResult(resultList, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_time), StringResourceGetter.getResource(R.string.axes_name_fuel_types_ratio_to_all_vehicle));

        return statisticsResults;
    }
}
