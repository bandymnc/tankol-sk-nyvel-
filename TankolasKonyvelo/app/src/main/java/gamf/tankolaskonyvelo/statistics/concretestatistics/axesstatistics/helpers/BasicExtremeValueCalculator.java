package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BasicExtremeValueCalculator {
    private final Double DIFFERENCE_COEFFICIENT = 0.1;
    private List<Double> values;
    private Double q1;
    private Double q3;

    public BasicExtremeValueCalculator(List<Double> values) {
        this.values = values;
    }

    public Double getUpperExtremeValue() {
        calculate();
        return q3;
    }

    public Double getLowerExtremeValue() {
        return q1;
    }

    private void calculate() {
        if (values.size() < 7) {
            q1 = 0.0d;
        } else {
            Collections.sort(values);

            if ((values.size() % 2) == 0) {
                even(values);
            } else {
                odd(values);
            }
        }
    }

    private void odd(List<Double> data) {

        List<Double> bottomHalf = new ArrayList<>();
        List<Double> topHalf = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            if (i < (data.size() / 2))
                bottomHalf.add(data.get(i));
            if (i > (data.size() / 2))
                topHalf.add(data.get(i));
        }
        if ((bottomHalf.size() / 2) == 0) {

            q1 = (bottomHalf.get((bottomHalf.size() / 2) - 1) + bottomHalf.get(bottomHalf.size() / 2)) / 2;
            q3 = (topHalf.get((topHalf.size() / 2) - 1) + topHalf.get(topHalf.size() / 2)) / 2;

        } else {

            q1 = bottomHalf.get((bottomHalf.size() / 2));
            q3 = topHalf.get((topHalf.size() / 2));
        }
    }

    public void even(List<Double> data) {
        List<Double> bottomHalf = new ArrayList<>();
        List<Double> topHalf = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            if (i < data.size() / 2 - 1)
                bottomHalf.add(data.get(i));
            if (i > data.size() / 2)
                topHalf.add(data.get(i));
        }

        if ((bottomHalf.size() / 2) == 0) {

            q1 = (bottomHalf.get(bottomHalf.size() / 2 - 1) + bottomHalf.get(bottomHalf.size() / 2)) / 2;
            q3 = (topHalf.get(topHalf.size() / 2 - 1) + topHalf.get(topHalf.size() / 2)) / 2;

        } else {

            q1 = bottomHalf.get((bottomHalf.size() / 2));
            q3 = topHalf.get((topHalf.size() / 2));
        }
    }
}
