package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelTypeConstants;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.FuelingListSplitterByMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels.MonthFuelings;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class SumOfFuelCostPerMonth implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private AxesStatisticsResult statisticsResult;
    private FuelTypeConstants fuelTypeConstants;

    public SumOfFuelCostPerMonth(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
        this.fuelTypeConstants = FuelTypeConstants.getInstance();
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> monthlyStatistics = new ArrayList<>();
        FuelingListSplitterByMonth fuelingListSplitterByMonth = new FuelingListSplitterByMonth(fuelings);
        List<MonthFuelings> statisticsGroupByMonth = fuelingListSplitterByMonth.getFuelingsGroupByMonth();
        List<Double> valueAggragation = new ArrayList<>();

        for (MonthFuelings aMonthlyStatistics : statisticsGroupByMonth) {
            SingleStatisticsResult aMonthlyStatisticsResult = calculateSumOfMonthlyCost(aMonthlyStatistics);
            monthlyStatistics.add(aMonthlyStatisticsResult);
            valueAggragation.add(aMonthlyStatisticsResult.getSingleStatisticsValue());
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResult = new AxesStatisticsResult(monthlyStatistics, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_month), StringResourceGetter.getResource(R.string.axes_name_sum_of_fuel_cost_per_month));

        return statisticsResult;
    }

    private SingleStatisticsResult calculateSumOfMonthlyCost(MonthFuelings dataListOfCurrentMonth) {
        Double sum = 0.0d;

        for (FuelingEntity aData : dataListOfCurrentMonth.getFuelingsInMonth()) {
            FuelTypeEntity fuelType = fuelTypeConstants.getFuelTypeEntityByFuelTypeId(aData.getFuelTypeId());
            if (fuelType.getUnit().equalsIgnoreCase("kw")) {
                sum += aData.getUnitPriceOfFuel();
            } else {
                sum += aData.getUnitPriceOfFuel() * aData.getFuelQuantity();
            }
        }

        String dataName = "";
        if (dataListOfCurrentMonth.size() != 0) {
            dataName = getYearAndMonthFromStringDate(dataListOfCurrentMonth.getMonthlyDate());
        }

        return new SingleStatisticsResult(dataName, sum);
    }

    private String getYearAndMonthFromStringDate(String date) {
        String[] dateParts = date.split("-");

        return dateParts[0] + "-" + dateParts[1];
    }
}
