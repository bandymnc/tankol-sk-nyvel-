package gamf.tankolaskonyvelo.statistics;

public class StatisticsOptions {
    private TimeDivision timeDivision;
    private int numberOfDisplayedDataOnAPage;
    // TODO: private Range range; (ha pl gorget a felhasznalo es nem az utolso 5 heti elemet akarja listazni, hanem mondjok az 2 hettel ezelottol szamitott 5 heti esemeny)

    public StatisticsOptions() {
        this.timeDivision = TimeDivision.ALL_TIME;
        this.numberOfDisplayedDataOnAPage = 5;
    }

    public TimeDivision getTimeDivision() {
        return timeDivision;
    }

    public void setTimeDivision(TimeDivision timeDivision) {
        this.timeDivision = timeDivision;
    }

    public int getNumberOfDisplayedDataOnAPage() {
        return numberOfDisplayedDataOnAPage;
    }

    public void setNumberOfDisplayedDataOnAPage(int numberOfDisplayedDataOnAPage) {
        this.numberOfDisplayedDataOnAPage = numberOfDisplayedDataOnAPage;
    }
}
