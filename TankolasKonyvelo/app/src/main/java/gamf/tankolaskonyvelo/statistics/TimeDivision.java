package gamf.tankolaskonyvelo.statistics;

public enum TimeDivision {DIVIDED_INTO_WEEKS, DIVIDED_INTO_MONTHS, DIVIDED_INTO_YEARS, ALL_TIME}
