package gamf.tankolaskonyvelo.statistics.helpers;

import java.util.List;

import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;

public class ItemListSorterByOccurrence {

    private List<PurchasedItemEntity> purchasedItems;
    private List<ItemEntity> items;
    private List<Double> itemOccurrences;

    public ItemListSorterByOccurrence(List<PurchasedItemEntity> purchasedItems,
                                      List<ItemEntity> items, List<Double> itemOccurrences) {
        this.purchasedItems = purchasedItems;
        this.items = items;
        this.itemOccurrences = itemOccurrences;
    }

    public void sortItemsByOccurrences() {
        for (int i = 0; i < items.size(); i++) {
            for (int j = i; j < items.size(); j++) {
                if (itemOccurrences.get(i) < itemOccurrences.get(j)) {
                    flipItems(i, j);
                    flipItemOccurrences(i, j);
                }
            }
        }
    }

    private void flipItems(int i, int j) {
        ItemEntity helperItem;
        helperItem = items.get(i);
        items.set(i, items.get(j));
        items.set(j, helperItem);
    }

    private void flipItemOccurrences(int i, int j) {
        Double helperOccurrence;
        helperOccurrence = itemOccurrences.get(i);
        itemOccurrences.set(i, itemOccurrences.get(j));
        itemOccurrences.set(j, helperOccurrence);
    }

    public List<ItemEntity> getOrderedItems() {
        return items;
    }

    public List<Double> getOrderedItemOccurrences() {
        return itemOccurrences;
    }
}
