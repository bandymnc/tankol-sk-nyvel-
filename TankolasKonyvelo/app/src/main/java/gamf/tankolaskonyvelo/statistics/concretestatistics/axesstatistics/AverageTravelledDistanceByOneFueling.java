package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class AverageTravelledDistanceByOneFueling implements StatisticsCalculator {
    private List<FuelingEntity> fuelings;
    private AxesStatisticsResult statisticsResult;

    public AverageTravelledDistanceByOneFueling(List<FuelingEntity> fuelings) {
        this.fuelings = fuelings;
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> statisticsResultList = new ArrayList<>();
        List<Double> valueAggragation = new ArrayList<>();
        Double sumOfTravelledDistance = 0.0d;
        Integer countOfFuelings = 0;

        for (int i = 0, j = 1; j < fuelings.size(); i++, j++) {
            sumOfTravelledDistance += fuelings.get(j).getKmMeterState() - fuelings.get(i).getKmMeterState();
            countOfFuelings++;

            String newStatisticsName = fuelings.get(i).getFuelingDate() + " - " + fuelings.get(j).getFuelingDate();
            Double newStatisticsValue = sumOfTravelledDistance / countOfFuelings;

            valueAggragation.add(newStatisticsValue);
            SingleStatisticsResult aResult = new SingleStatisticsResult(newStatisticsName, newStatisticsValue);
            statisticsResultList.add(aResult);
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResult = new AxesStatisticsResult(statisticsResultList, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_time), StringResourceGetter.getResource(R.string.axes_name_average_travelled_distance_by_one_fueling));
        return statisticsResult;
    }
}
