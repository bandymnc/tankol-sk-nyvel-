package gamf.tankolaskonyvelo.statistics.result;

import java.util.ArrayList;
import java.util.List;

public class SingleStatisticsResult implements StatisticsResult {
    private String statisticsName;
    private Double statisticsValue;

    public SingleStatisticsResult(String statisticsName, Double statisticsValue) {
        this.statisticsName = statisticsName;
        this.statisticsValue = statisticsValue;
    }

    @Override
    public List<String> getStatisticsNames() {
        List<String> statisticsNameResult = new ArrayList<>();
        statisticsNameResult.add(statisticsName);

        return statisticsNameResult;
    }

    @Override
    public List<Double> getStatisticsValues() {
        List<Double> statisticsValueResult = new ArrayList<>();
        statisticsValueResult.add(statisticsValue);

        return statisticsValueResult;
    }

    @Override
    public String getSingleStatisticsName() {
        return statisticsName;
    }

    @Override
    public Double getSingleStatisticsValue() {
        return statisticsValue;
    }

    public Float getSingleStatisticsFloatValue() {
        Float value = (float) ((int) (getSingleStatisticsValue().floatValue() * 10f)) / 10f;

        return value;
    }
}
