package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.PurchasedItemsSplitterByMonth;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels.MonthPurchasedItems;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class SumOfPurchasedItemsCostPerMonth implements StatisticsCalculator {
    List<PurchasedItemEntity> purchasedItems;
    List<MonthPurchasedItems> purchasedItemsGroupByMonth;
    AxesStatisticsResult statisticsResults;

    public SumOfPurchasedItemsCostPerMonth(List<PurchasedItemEntity> purchasedItems) {
        this.purchasedItems = purchasedItems;

        PurchasedItemsSplitterByMonth fuelingListSplitterByMonth = new PurchasedItemsSplitterByMonth(purchasedItems);
        purchasedItemsGroupByMonth = fuelingListSplitterByMonth.getFuelingsGroupByMonth();
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> resultList = new ArrayList<>();
        List<Double> valueAggragation = new ArrayList<>();

        for (MonthPurchasedItems aMonthPurchasedItems : purchasedItemsGroupByMonth) {
            Double costOfMonth = aMonthPurchasedItems.getCostOfMonthlyPurchasedItems();

            resultList.add(new SingleStatisticsResult(aMonthPurchasedItems.getMonthlyDate(), costOfMonth));
            valueAggragation.add(costOfMonth);
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator = new BasicExtremeValueCalculator(valueAggragation);
        statisticsResults = new AxesStatisticsResult(resultList, basicExtremeValueCalculator.getUpperExtremeValue(), basicExtremeValueCalculator.getLowerExtremeValue(), StringResourceGetter.getResource(R.string.axes_name_month), StringResourceGetter.getResource(R.string.axes_name_sum_of_purchased_items_cost_per_month));

        return statisticsResults;
    }
}
