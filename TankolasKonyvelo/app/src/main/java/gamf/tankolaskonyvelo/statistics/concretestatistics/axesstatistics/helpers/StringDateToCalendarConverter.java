package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StringDateToCalendarConverter {
    public Calendar getCalendarFromStringDate(String stringDate) {
        long milliseconds = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        try {
            Date d = dateFormat.parse(stringDate);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendarInstance = Calendar.getInstance();
        calendarInstance.setTime(new Date(milliseconds));

        return calendarInstance;
    }
}
