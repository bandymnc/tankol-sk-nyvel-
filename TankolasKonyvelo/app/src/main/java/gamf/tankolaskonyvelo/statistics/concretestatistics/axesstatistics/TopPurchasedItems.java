package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.statistics.StatisticsCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.BasicExtremeValueCalculator;
import gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.StringResourceGetter;
import gamf.tankolaskonyvelo.statistics.helpers.ItemListFilter;
import gamf.tankolaskonyvelo.statistics.helpers.ItemListSorterByOccurrence;
import gamf.tankolaskonyvelo.statistics.result.AxesStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.SingleStatisticsResult;
import gamf.tankolaskonyvelo.statistics.result.StatisticsResult;

public class TopPurchasedItems implements StatisticsCalculator {
    private List<PurchasedItemEntity> purchasedItems;
    private List<ItemEntity> items;
    private List<Double> itemOccurrences;

    private int countOfShownItems;
    private AxesStatisticsResult statisticsResult;

    /**
     * @param purchasedItems    az osszes PurchasedItemEntity
     * @param items             az osszes ItemEntity
     * @param countOfShownItems a szamitott mennyiseg (pl. top 5 tetel)
     */
    public TopPurchasedItems(List<PurchasedItemEntity> purchasedItems, List<ItemEntity> items, int countOfShownItems) {
        this.items = items;
        this.purchasedItems = purchasedItems;
        initDefaultItemsOccurrences();
        initPurchasedItemsOccurrences();

        initCountOfShownItems(countOfShownItems);
    }

    private void initDefaultItemsOccurrences() {
        itemOccurrences = new ArrayList<>();

        for (int i = 0; i < items.size(); i++) {
            itemOccurrences.add(1.0);
        }
    }

    @Override
    public StatisticsResult calculateStatisics() {
        List<SingleStatisticsResult> topItemsStatistics = new ArrayList<>();
        ItemListFilter filter = new ItemListFilter(countOfShownItems, items, itemOccurrences);
        filter.filterForCountOfShownItems();

        List<ItemEntity> sortedItems = filter.getFilteredItems();
        List<Double> sortedOccurrences = filter.getFilteredItemOccurrences();

        ItemListSorterByOccurrence sorter = new ItemListSorterByOccurrence(purchasedItems, sortedItems, sortedOccurrences);
        sorter.sortItemsByOccurrences();

        sortedItems = sorter.getOrderedItems();
        sortedOccurrences = sorter.getOrderedItemOccurrences();

        for (int i = 0; i < items.size(); i++) {
            SingleStatisticsResult newSingleStatistics =
                    new SingleStatisticsResult(sortedItems.get(i).getItemName(),
                            Double.valueOf(sortedOccurrences.get(i)));
            topItemsStatistics.add(newSingleStatistics);
        }

        BasicExtremeValueCalculator basicExtremeValueCalculator =
                new BasicExtremeValueCalculator(sortedOccurrences);

        statisticsResult = new AxesStatisticsResult(topItemsStatistics,
                basicExtremeValueCalculator.getUpperExtremeValue(),
                basicExtremeValueCalculator.getLowerExtremeValue(),
                StringResourceGetter.getResource(R.string.axes_name_product),
                StringResourceGetter.getResource(R.string.axes_name_top_purchased_items));

        return statisticsResult;
    }

    private void initCountOfShownItems(int countOfShownItems) {
        if (purchasedItems == null) {
            this.countOfShownItems = 0;
            return;
        }

        if (isCountTooHigh(countOfShownItems)) {
            this.countOfShownItems = purchasedItems.size();
        } else if (isCountTooLow(countOfShownItems)) {
            this.countOfShownItems = 0;
        } else {
            this.countOfShownItems = countOfShownItems;
        }
    }

    private boolean isCountTooHigh(int countOfShownItems) {
        boolean isCountTooHigh = purchasedItems.size() < countOfShownItems;
        if (!isCountTooHigh) {
            return true;
        }
        return false;
    }

    private boolean isCountTooLow(int countOfShownItems) {
        boolean isCountTooLow = countOfShownItems < 0;
        if (!isCountTooLow) {
            return true;
        }
        return false;
    }

    private void initPurchasedItemsOccurrences() {
        for (PurchasedItemEntity purchasedItem : purchasedItems) {
            increaseItemOccurrenceAt(purchasedItem);
        }

        return;
    }

    private void increaseItemOccurrenceAt(PurchasedItemEntity purchasedItem) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId() == purchasedItem.getItemId()) {
                Double actualOccurrence = itemOccurrences.get(i);
                itemOccurrences.set(i, actualOccurrence + 1);
            }
        }
    }

}
