package gamf.tankolaskonyvelo.statistics.concretestatistics.axesstatistics.helpers.helpermodels;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;

public class MonthPurchasedItems {
    private List<PurchasedItemEntity> purchasedItemsInMonth;
    private int monthIndex;

    public MonthPurchasedItems(int monthIndex) {
        this.purchasedItemsInMonth = new ArrayList<>();
        this.monthIndex = monthIndex;
    }

    public void addPurchasedItemToMonth(PurchasedItemEntity purchasedItemInMonth) {
        purchasedItemsInMonth.add(purchasedItemInMonth);
    }

    public Double getCostOfMonthlyPurchasedItems() {
        Double result = 0.0d;

        for (PurchasedItemEntity aPurchasedItemEntity : purchasedItemsInMonth) {
            result += aPurchasedItemEntity.getCurrentPrice() * aPurchasedItemEntity.getPiece();
        }

        return result;
    }

    public String getMonthlyDate() {
        return parseLongDateToMonthlyDate();
    }

    private String parseLongDateToMonthlyDate() {
        String[] dateParts = purchasedItemsInMonth.get(0).getPurchaseDate().split("-");

        return dateParts[0] + "-" + dateParts[1];
    }

    public List<PurchasedItemEntity> getPurchasedItemsInMonth() {
        return purchasedItemsInMonth;
    }

    public void setPurchasedItemsInMonth(List<PurchasedItemEntity> purchasedItemsInMonth) {
        this.purchasedItemsInMonth = purchasedItemsInMonth;
    }

    public int getMonthIndex() {
        return monthIndex;
    }

    public void setMonthIndex(int monthIndex) {
        this.monthIndex = monthIndex;
    }
}
