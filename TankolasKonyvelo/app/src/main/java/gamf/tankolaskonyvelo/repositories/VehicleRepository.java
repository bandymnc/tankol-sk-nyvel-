package gamf.tankolaskonyvelo.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.daos.VehicleDao;
import gamf.tankolaskonyvelo.entities.VehicleEntity;

public class VehicleRepository {
    private AppExecutors appExecutors;
    private VehicleDao vehicleDao;

    public VehicleRepository(Application application, AppExecutors appExecutors) {
        AppDatabase database = AppDatabase.getInstance(application, appExecutors);
        this.appExecutors = appExecutors;

        vehicleDao = database.vehicleDao();
    }

    public void insert(VehicleEntity vehicleEntity) {
        appExecutors.databaseIO().execute(() -> {
            vehicleDao.insert(vehicleEntity);
        });
    }
    public LiveData<String> getDefaultFuelUnit(String plateNumber){
        return vehicleDao.getDefaultFuelUnit(plateNumber);
    }

    public void update(VehicleEntity vehicleEntity) {
        appExecutors.databaseIO().execute(() -> {
            vehicleDao.update(vehicleEntity);
        });
    }

    public void delete(VehicleEntity vehicleEntity) {
        appExecutors.databaseIO().execute(() -> {
            vehicleDao.delete(vehicleEntity);
        });
    }

    public LiveData<List<VehicleEntity>> getAllVehicle() {
        return vehicleDao.getAllVechicle();
    }

    public LiveData<VehicleEntity> getDefaultVehicle() {
        return vehicleDao.getDefaultVehicle();
    }

    public LiveData<Integer> getVehicleAmount() {
        return vehicleDao.getVehicleAmount();
    }

}
