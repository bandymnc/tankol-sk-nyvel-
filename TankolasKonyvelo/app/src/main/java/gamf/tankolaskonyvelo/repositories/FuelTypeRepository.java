package gamf.tankolaskonyvelo.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.daos.FuelTypeDao;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;

public class FuelTypeRepository {
    private AppExecutors appExecutors;
    FuelTypeDao fuelTypeDao;

    public FuelTypeRepository(Application application, AppExecutors appExecutors) {
        AppDatabase database = AppDatabase.getInstance(application, appExecutors);
        this.appExecutors = appExecutors;

        fuelTypeDao = database.fuelTypeDao();
    }

    public void insert(FuelTypeEntity fuelTypeEntity) {
        appExecutors.databaseIO().execute(() -> {
            fuelTypeDao.insert(fuelTypeEntity);
        });
    }

    public void update(FuelTypeEntity fuelTypeEntity) {
        appExecutors.databaseIO().execute(() -> {
            fuelTypeDao.update(fuelTypeEntity);
        });
    }

    public void delete(FuelTypeEntity fuelTypeEntity) {
        appExecutors.databaseIO().execute(() -> {
            fuelTypeDao.delete(fuelTypeEntity);
        });
    }

    public LiveData<List<FuelTypeEntity>> getAllFuelTypesOrderByName() {
        return fuelTypeDao.getAllFuelTypeOrderByName();
    }

    public LiveData<FuelTypeEntity> getFuelTypeByLicensePlate(String licensePlate) {
        return fuelTypeDao.getFuelTypeByLicensePlate(licensePlate);
    }
}
