package gamf.tankolaskonyvelo.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.daos.PurchasedItemDao;
import gamf.tankolaskonyvelo.entities.dataobjects.PurchasedItemWithVehicle;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;

public class PurchasedItemRepository {
    AppExecutors appExecutors;
    PurchasedItemDao purchasedItemDao;

    public PurchasedItemRepository(Application application, AppExecutors appExecutors) {
        AppDatabase database = AppDatabase.getInstance(application, appExecutors);
        this.appExecutors = appExecutors;

        purchasedItemDao = database.purchasedItemDao();
    }

    public void insert(PurchasedItemEntity purchasedItemEntity) {
        appExecutors.databaseIO().execute(() -> {
            purchasedItemDao.insert(purchasedItemEntity);
        });
    }

    public void update(PurchasedItemEntity purchasedItemEntity) {
        appExecutors.databaseIO().execute(() -> {
            purchasedItemDao.update(purchasedItemEntity);
        });
    }

    public void delete(PurchasedItemEntity purchasedItemEntity) {
        appExecutors.databaseIO().execute(() -> {
            purchasedItemDao.delete(purchasedItemEntity);
        });
    }

    public LiveData<List<PurchasedItemEntity>> getAllPurchasedItemsOrderByPurschasedDateDesc() {
        return purchasedItemDao.getAllPurchasedItemsOrderByPurschasedDateDesc();
    }

    public LiveData<List<PurchasedItemWithVehicle>> getAllPurchasedItemByVehicle(String licensePlateNumber) {
        return purchasedItemDao.getAllPurchasedItemByVehicle(licensePlateNumber);
    }

    public LiveData<List<PurchasedItemWithVehicle>> getAllImportantPurchasedItemByVehicle(String licensePlateNumber) {
        return purchasedItemDao.getAllImportantPurchasedItemByVehicle(licensePlateNumber);
    }

    public LiveData<List<PurchasedItemEntity>> getAllPurchasedItemsByLicensePlateNumber(String licensePlateNumber) {
        return purchasedItemDao.getAllPurchasedItemsByLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<PurchasedItemEntity>> getAllPurchasedItems() {
        return purchasedItemDao.getAllPurchasedItems();
    }
}
