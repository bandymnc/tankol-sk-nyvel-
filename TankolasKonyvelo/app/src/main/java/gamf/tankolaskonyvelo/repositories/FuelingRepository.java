package gamf.tankolaskonyvelo.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.daos.FuelingDao;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.dataobjects.FuelingWithFuelType;
import gamf.tankolaskonyvelo.ui.dashboard.InsertionListener;

public class FuelingRepository {
    private AppExecutors appExecutors;
    private FuelingDao fuelingDao;

    public FuelingRepository(Application application, AppExecutors appExecutors) {
        AppDatabase database = AppDatabase.getInstance(application, appExecutors);
        this.appExecutors = appExecutors;

        fuelingDao = database.fuelingDao();
    }

    public void insert(FuelingEntity fuelingEntity) {
        appExecutors.databaseIO().execute(() -> {
            fuelingDao.insert(fuelingEntity);
        });
    }

    public void insert(FuelingEntity fuelingEntity, InsertionListener listener) {
        appExecutors.databaseIO().execute(() -> {
            long idOfInserted = fuelingDao.insert(fuelingEntity);
            listener.onRecordInserted(idOfInserted);
        });
    }

    public void update(FuelingEntity fuelingEntity) {
        appExecutors.databaseIO().execute(() -> {
            fuelingDao.update(fuelingEntity);
        });
    }

    public void delete(FuelingEntity fuelingEntity) {
        appExecutors.databaseIO().execute(() -> {
            fuelingDao.delete(fuelingEntity);
        });
    }

    public LiveData<FuelingEntity> getLastFueling(String licensePlate) {
        return fuelingDao.getLastFueling(licensePlate);
    }

    public LiveData<FuelingWithFuelType> getLastFuelingByVehicleLicensePlateNumber(String licensePlateNumber) {
        return fuelingDao.getLastFuelingByVehicleLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<FuelingEntity>> getAllFuelingsByLicensePlateNumber(String licensePlateNumber) {
        return fuelingDao.getAllFuelingsByLicensePlateNumber(licensePlateNumber);
    }

    public LiveData<List<FuelingWithFuelType>> getAllFuelingsWithFuelTypesOrderByFuelingDate() {
        return  fuelingDao.getAllFuelingsWithFuelTypesOrderByFuelingDate();
    }
}
