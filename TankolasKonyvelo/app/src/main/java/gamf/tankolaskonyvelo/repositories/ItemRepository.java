package gamf.tankolaskonyvelo.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;

import java.util.List;

import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.daos.ItemDao;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.ui.dashboard.InsertionListener;

public class ItemRepository {
    private AppExecutors appExecutors;
    private ItemDao itemDao;

    public ItemRepository(Application application, AppExecutors appExecutors) {
        AppDatabase database = AppDatabase.getInstance(application, appExecutors);
        this.appExecutors = appExecutors;

        itemDao = database.itemDao();
    }

    public void insert(ItemEntity itemEntity, InsertionListener listener) {
        appExecutors.databaseIO().execute(() -> {
            long idOfInserted = itemDao.insert(itemEntity);
            listener.onRecordInserted(idOfInserted);
        });
    }

    public void update(ItemEntity itemEntity) {
        appExecutors.databaseIO().execute(() -> {
            itemDao.update(itemEntity);
        });
    }

    public void delete(ItemEntity itemEntity) {
        appExecutors.databaseIO().execute(() -> {
            itemDao.delete(itemEntity);
        });
    }

    public LiveData<ItemEntity> getItemByName(String itemName) {
        return itemDao.getItemByName(itemName);
    }

    public LiveData<List<ItemEntity>> getAllFuelingsOrderByFuelingDate() {
        return itemDao.getAllItemOrderByName();
    }


    public LiveData<List<ItemEntity>> getAllItemOrderByName() {
        return itemDao.getAllItemOrderByName();
    }
}
