package gamf.tankolaskonyvelo.localization;

public interface Localization {
    void setLocale(String language);
    void apply();
    void loadDefaultLocale();
    void saveDefaultLocale(String language);
}
