package gamf.tankolaskonyvelo.localization;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.Locale;
import static android.content.Context.MODE_PRIVATE;

public class Localizer implements Localization {

    private final String DEFAULT_LANGUAGE = "hu";
    public static final String LANGUAGE = "Language";
    private final String SETTINGS = "Settings";

    private String language;
    private Activity context;
    private Class cls;

    public Localizer(Activity context, Class cls) {
        this.context = context;
        this.cls = cls;
    }

    @Override
    public void setLocale(String language) {
        if (language.equals(DEFAULT_LANGUAGE)) {
            this.language = "";
        } else {
            this.language = language;
        }

        Locale locale = new Locale(this.language);
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.setLocale(locale);
        context.getBaseContext().getResources().updateConfiguration(
                config, context.getBaseContext().getResources().getDisplayMetrics());

        SharedPreferences.Editor editor = context.getSharedPreferences(SETTINGS, MODE_PRIVATE).edit();
        editor.putString(LANGUAGE, this.language);
        editor.apply();
    }

    @Override
    public void apply() {
        Intent intent = new Intent(context, cls);
        Bundle bundle = new Bundle();
        bundle.putString(LANGUAGE, language);
        intent.putExtras(bundle);

        context.startActivity(intent);
        context.finish();
    }

    @Override
    public void loadDefaultLocale() {
        SharedPreferences prefs = context.getSharedPreferences(LANGUAGE, MODE_PRIVATE);
        String language = prefs.getString(LANGUAGE, DEFAULT_LANGUAGE);
        setLocale(language);
    }

    @Override
    public void saveDefaultLocale(String language) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(LANGUAGE, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LANGUAGE, language);
        editor.commit();
    }
}
