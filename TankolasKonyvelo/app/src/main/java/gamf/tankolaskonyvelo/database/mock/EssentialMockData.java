package gamf.tankolaskonyvelo.database.mock;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.ItemEntity;

public class EssentialMockData {
    private List<FuelTypeEntity> newFuelTypes;
    private List<ItemEntity> newItems;

    public EssentialMockData(List<FuelTypeEntity> newFuelTypes, List<ItemEntity> newItems) {
        this.newFuelTypes = newFuelTypes;
        this.newItems = newItems;
    }

    public List<FuelTypeEntity> getNewFuelTypes() {
        return newFuelTypes;
    }

    public void setNewFuelTypes(List<FuelTypeEntity> newFuelTypes) {
        this.newFuelTypes = newFuelTypes;
    }

    public List<ItemEntity> getNewItems() {
        return newItems;
    }

    public void setNewItems(List<ItemEntity> newItems) {
        this.newItems = newItems;
    }
}
