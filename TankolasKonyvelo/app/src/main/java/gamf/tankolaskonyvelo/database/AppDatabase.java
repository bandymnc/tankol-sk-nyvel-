package gamf.tankolaskonyvelo.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import gamf.tankolaskonyvelo.daos.FuelTypeDao;
import gamf.tankolaskonyvelo.daos.FuelingDao;
import gamf.tankolaskonyvelo.daos.ItemDao;
import gamf.tankolaskonyvelo.daos.PurchasedItemDao;
import gamf.tankolaskonyvelo.daos.VehicleDao;
import gamf.tankolaskonyvelo.database.mock.MockDataGenerator;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;

@Database(entities = {FuelingEntity.class, FuelTypeEntity.class, VehicleEntity.class, ItemEntity.class, PurchasedItemEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(final Context context, final AppExecutors appExecutors) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "app_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);

                            MockDataGenerator mockDataGenerator = new MockDataGenerator();
                            mockDataGenerator.fillDatabaseWithEssentialMockDataOnly(instance, appExecutors);
                        }

                        @Override
                        public void onOpen(@NonNull SupportSQLiteDatabase db) {
                            super.onOpen(db);

                            //MockDataGenerator mockDataGenerator = new MockDataGenerator();
                            //mockDataGenerator.fillDatabaseWithMockData(instance, appExecutors);
                        }
                    })
                    .build();
        }

        return instance;
    }

    public abstract FuelingDao fuelingDao();

    public abstract FuelTypeDao fuelTypeDao();

    public abstract ItemDao itemDao();

    public abstract VehicleDao vehicleDao();

    public abstract PurchasedItemDao purchasedItemDao();
}
