package gamf.tankolaskonyvelo.database.mock;

import java.util.List;

import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;

public class MockData {
    private List<VehicleEntity> newVehicles;
    private List<FuelingEntity> newFuelings;
    private List<PurchasedItemEntity> newPurchasedItems;

    public MockData(List<VehicleEntity> newVehicles, List<FuelingEntity> newFuelings, List<PurchasedItemEntity> newPurchasedItems) {
        this.newVehicles = newVehicles;
        this.newFuelings = newFuelings;
        this.newPurchasedItems = newPurchasedItems;
    }

    public List<VehicleEntity> getNewVehicles() {
        return newVehicles;
    }

    public void setNewVehicles(List<VehicleEntity> newVehicles) {
        this.newVehicles = newVehicles;
    }

    public List<FuelingEntity> getNewFuelings() {
        return newFuelings;
    }

    public void setNewFuelings(List<FuelingEntity> newFuelings) {
        this.newFuelings = newFuelings;
    }

    public List<PurchasedItemEntity> getNewPurchasedItems() {
        return newPurchasedItems;
    }

    public void setNewPurchasedItems(List<PurchasedItemEntity> newPurchasedItems) {
        this.newPurchasedItems = newPurchasedItems;
    }
}
