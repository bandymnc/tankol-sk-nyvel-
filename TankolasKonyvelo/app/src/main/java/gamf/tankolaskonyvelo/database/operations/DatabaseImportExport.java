package gamf.tankolaskonyvelo.database.operations;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import gamf.tankolaskonyvelo.R;
import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.ui.main.MainActivity;

public class DatabaseImportExport {

    private Context context;
    private AppDatabase database;
    private Activity activity;

    public DatabaseImportExport(Context context, AppDatabase database, Activity activity) {
        this.context = context;
        this.database = database;
        this.activity = activity;
    }

    public String databaseImport(Uri source) {
        database.close();

        FileInputStream input = null;
        FileOutputStream output = null;

        try {
            input = (FileInputStream) activity.getContentResolver().openInputStream(source);
            output = new FileOutputStream(context.getDatabasePath("app_database"));

            IOUtils.copy(input, output);

        } catch (FileNotFoundException exc) {
            return Resources.getSystem().getString(R.string.no_database_sourche_file);
        } catch (IOException exc) {
            return Resources.getSystem().getString(R.string.some_error);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
                if (output != null) {
                    output.close();
                }
            } catch (IOException exc) {
                return Resources.getSystem().getString(R.string.some_error);
            }
            restartApp();
            return Resources.getSystem().getString(R.string.succesfull_import);
        }
    }

    public String databaseExport() {
        database.close();
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        String[] data = path.split("/");
        File sourceDb = context.getDatabasePath("app_database");
        File destinationDb = new File(path + "/Adatbázis/app_database");

        try {
            FileUtils.copyFile(sourceDb, destinationDb);
        } catch (Exception e) {
            return Resources.getSystem().getString(R.string.some_error);
        }
        restartApp();
        return context.getResources().getString(R.string.succesfull_saving_to) + " " + data[data.length - 1];
    }

    private void restartApp() {
        Intent intent = new Intent(context, MainActivity.class);
        int mPendingIntentId = 10;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }
}
