package gamf.tankolaskonyvelo.database.mock;

import java.util.ArrayList;
import java.util.List;

import gamf.tankolaskonyvelo.daos.FuelTypeDao;
import gamf.tankolaskonyvelo.daos.FuelingDao;
import gamf.tankolaskonyvelo.daos.ItemDao;
import gamf.tankolaskonyvelo.daos.PurchasedItemDao;
import gamf.tankolaskonyvelo.daos.VehicleDao;
import gamf.tankolaskonyvelo.database.AppDatabase;
import gamf.tankolaskonyvelo.database.AppExecutors;
import gamf.tankolaskonyvelo.entities.FuelTypeEntity;
import gamf.tankolaskonyvelo.entities.FuelingEntity;
import gamf.tankolaskonyvelo.entities.ItemEntity;
import gamf.tankolaskonyvelo.entities.PurchasedItemEntity;
import gamf.tankolaskonyvelo.entities.VehicleEntity;

public class MockDataGenerator {
    public void fillDatabaseWithEssentialMockDataOnly(AppDatabase database, AppExecutors appExecutors) {
        clearDatabase(database, appExecutors);

        MockDataGenerator mockDataGenerator = new MockDataGenerator();
        EssentialMockData essentialMockData = mockDataGenerator.getEssentialMockData();

        fillDatabase(database, appExecutors, essentialMockData);
    }

    public void fillDatabaseWithMockData(AppDatabase database, AppExecutors appExecutors) {
        clearDatabase(database, appExecutors);

        MockDataGenerator mockDataGenerator = new MockDataGenerator();
        EssentialMockData essentialMockData = mockDataGenerator.getEssentialMockData();
        MockData mockData = mockDataGenerator.getMockData();

        fillDatabase(database, appExecutors, essentialMockData, mockData);
    }

    private EssentialMockData getEssentialMockData() {
        List<FuelTypeEntity> newFuelTypes = new ArrayList<>();
        List<ItemEntity> newItems = new ArrayList<>();

        newFuelTypes.add(new FuelTypeEntity("Benzin", "l"));
        newFuelTypes.add(new FuelTypeEntity("Gázolaj", "l"));
        newFuelTypes.add(new FuelTypeEntity("Keverék", "l"));
        newFuelTypes.add(new FuelTypeEntity("LPG autógáz", "l"));
        newFuelTypes.add(new FuelTypeEntity("Áram", "kw"));

        newItems.add(new ItemEntity("Ablakmosó folyadék", false));
        newItems.add(new ItemEntity("Ablaktörlők", false));
        newItems.add(new ItemEntity("Izzók", false));
        newItems.add(new ItemEntity("Gumik", false));
        newItems.add(new ItemEntity("Kisszervíz", true));
        newItems.add(new ItemEntity("Nagyszervíz", true));
        newItems.add(new ItemEntity("Fék", true));
        newItems.add(new ItemEntity("Olajszűrő", true));
        newItems.add(new ItemEntity("Olajcsere", true));
        newItems.add(new ItemEntity("Vezérműszíj", true));

        return new EssentialMockData(newFuelTypes, newItems);
    }

    private void clearDatabase(AppDatabase database, AppExecutors appExecutors) {
        FuelTypeDao fuelTypeDao = database.fuelTypeDao();
        VehicleDao vehicleDao = database.vehicleDao();
        FuelingDao fuelingDao = database.fuelingDao();
        ItemDao itemDao = database.itemDao();
        PurchasedItemDao purchasedItemDao = database.purchasedItemDao();

        appExecutors.databaseIO().execute(() -> {
            fuelTypeDao.deleteAll();
            vehicleDao.deleteAll();
            fuelingDao.deleteAll();
            itemDao.deleteAll();
            purchasedItemDao.deleteAll();
        });
    }

    private void fillDatabase(AppDatabase database, AppExecutors appExecutors, EssentialMockData essentialMockData, MockData mockData) {
        FuelTypeDao fuelTypeDao = database.fuelTypeDao();
        VehicleDao vehicleDao = database.vehicleDao();
        FuelingDao fuelingDao = database.fuelingDao();
        ItemDao itemDao = database.itemDao();
        PurchasedItemDao purchasedItemDao = database.purchasedItemDao();

        appExecutors.databaseIO().execute(() -> {
            fuelTypeDao.insert(essentialMockData.getNewFuelTypes());
            vehicleDao.insert(mockData.getNewVehicles());
            fuelingDao.insert(mockData.getNewFuelings());
            itemDao.insert(essentialMockData.getNewItems());
            purchasedItemDao.insert(mockData.getNewPurchasedItems());
        });
    }

    private void fillDatabase(AppDatabase database, AppExecutors appExecutors, EssentialMockData essentialMockData) {
        FuelTypeDao fuelTypeDao = database.fuelTypeDao();
        VehicleDao vehicleDao = database.vehicleDao();
        FuelingDao fuelingDao = database.fuelingDao();
        ItemDao itemDao = database.itemDao();
        PurchasedItemDao purchasedItemDao = database.purchasedItemDao();

        appExecutors.databaseIO().execute(() -> {
            fuelTypeDao.insert(essentialMockData.getNewFuelTypes());
            itemDao.insert(essentialMockData.getNewItems());
        });
    }

    private MockData getMockData() {
        List<VehicleEntity> newVehicles = new ArrayList<>();
        List<FuelingEntity> newFuelings = new ArrayList<>();
        List<PurchasedItemEntity> newPurchasedItems = new ArrayList<>();

        newVehicles.add(new VehicleEntity(true, "személygépkocsi", "ABC-123", 1));
        newVehicles.add(new VehicleEntity(false, "személygépkocsi", "DEF-456", 3));
        newVehicles.add(new VehicleEntity(false, "tehergépkocsi", "GHI-789", 2));

        newFuelings.add(new FuelingEntity("2019-01-20 11:04", "MOL", 365.0, 10.0, 10.0, 147256, 1, 1));
        newFuelings.add(new FuelingEntity("2019-01-21 8:11", "MOL", 367.0, 30.0, 5.0, 147460, 1, 1));
        newFuelings.add(new FuelingEntity("2019-01-22 15:22", "MOL", 365.0, 20.0, 11.0, 147888, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-20 9:21", "MOL", 363.0, 5.0, 21.0, 147999, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-21 7:10", "MOL", 363.0, 10.0, 10.0, 148050, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-22 9:15", "MOL", 365.0, 30.0, 5.0, 148100, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-23 11:24", "MOL", 367.0, 20.0, 11.0, 148200, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-25 13:20", "MOL", 363.0, 5.0, 21.0, 148350, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-26 15:25", "MOL", 365.0, 5.0, 10.0, 148450, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-28 17:30", "MOL", 367.0, 5.0, 5.0, 148600, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-01 19:35", "MOL", 363.0, 10.0, 11.0, 148700, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-02 7:40", "MOL", 365.0, 30.0, 21.0, 148900, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-04 8:44", "MOL", 367.0, 20.0, 10.0, 148970, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-05 9:50", "MOL", 363.0, 5.0, 5.0, 149050, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-06 10:51", "MOL", 365.0, 5.0, 11.0, 149150, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-09 12:52", "MOL", 367.0, 5.0, 21.0, 149250, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-11 13:10", "MOL", 363.0, 5.0, 10.0, 149350, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-12 15:5", "MOL", 365.0, 10.0, 5.0, 149450, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-13 16:20", "MOL", 367.0, 30.0, 11.0, 149550, 1, 1));
        newFuelings.add(new FuelingEntity("2019-03-14 7:21", "MOL", 363.0, 5.0, 21.0, 149600, 1, 1));
        newFuelings.add(new FuelingEntity("2019-02-21 10:08", "Auchan", 401.0, 14.0, 7.0, 5000, 2, 3));
        newFuelings.add(new FuelingEntity("2019-02-22 12:33", "Auchan", 399.0, 25.0, 5.0, 5400, 2, 3));
        newFuelings.add(new FuelingEntity("2019-03-20 18:54", "Auchan", 405.0, 32.0, 10.0, 5759, 2, 3));
        newFuelings.add(new FuelingEntity("2019-03-21 19:59", "Auchan", 401.0, 14.7, 5.0, 6501, 2, 3));
        newFuelings.add(new FuelingEntity("2019-03-19 21:01", "Shell", 220.0, 20.0, 10.0, 90000, 4, 2));
        newFuelings.add(new FuelingEntity("2019-03-20 8:11", "Shell", 226.0, 50.0, 0.0, 90310, 4, 2));
        newFuelings.add(new FuelingEntity("2019-03-21 7:14", "Shell", 230.0, 10.0, 38.0, 90408, 4, 2));
        newFuelings.add(new FuelingEntity("2019-03-22 11:11", "Shell", 228.0, 20.0, 10.0, 90660, 4, 2));

        newPurchasedItems.add(new PurchasedItemEntity(1, "2019-01-20 11:04", 405, 1, 1));
        newPurchasedItems.add(new PurchasedItemEntity(4, "2019-01-20 11:15", 105, 1, 3));
        newPurchasedItems.add(new PurchasedItemEntity(2, "2019-02-11 8:11", 980, 2, 2));
        newPurchasedItems.add(new PurchasedItemEntity(1, "2019-01-10 9:21", 14000, 1, 7));
        newPurchasedItems.add(new PurchasedItemEntity(1, "2019-03-19 17:05", 7000, 2, 6));
        newPurchasedItems.add(new PurchasedItemEntity(1, "2019-03-19 17:05", 17500, 1, 7));

        return new MockData(newVehicles, newFuelings, newPurchasedItems);
    }
}
