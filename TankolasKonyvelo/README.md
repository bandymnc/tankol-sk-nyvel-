Projekt erőforrás könyvelés:
https://docs.google.com/spreadsheets/d/1JHr4yY7ByP0yr8z2LncUyi8QQJKhmhGhoe4HDEZX6oA/edit?usp=sharing

Technológiai dokumentáció PDF formátumban:
https://drive.google.com/open?id=1MOZnRzNIamyd5BIkvXSjjot6_NYEgT-f

Technológiai dokumentáció ODT formátumban (csak a Google Docs előnézete jeleníti meg helytelen formázással):
https://drive.google.com/open?id=18J4jsFu7IE05NLcqpVeOpb2iC4AqC37T

Technológiai dokumentáció DOCX formátumban:
https://drive.google.com/open?id=1yxvMefNza5KkMqDqzUsR4R-9zkctjI38

Felhasználói dokumentáció PDF formátumban
https://drive.google.com/open?id=1T4WcK69UuuKwgErNtsyE20wTDB0wcIqM

Felhasználói dokumentáció ODT formátumban 
https://drive.google.com/open?id=1t_lL0-HRskJbOr364xTkimy5ONiX9-V1
